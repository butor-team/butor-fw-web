/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.web.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.butor.json.JsonHelper;
import org.butor.json.JsonRequest;
import org.butor.json.service.BinResponseHandler;
import org.butor.json.service.ResponseHandler;
import org.butor.utils.ApplicationException;
import org.butor.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.google.api.client.util.Strings;
import com.google.common.base.Preconditions;

public class BinAjaxComponent extends BaseAjaxComponent {
	protected Logger logger = LoggerFactory.getLogger(getClass());
	private JsonHelper jsh = new JsonHelper();

	public BinAjaxComponent(Object targetCmp) {
		super(targetCmp);
	}

	@Override
	public void process(final HttpServletRequest request, final HttpServletResponse response)
			throws ServletException, IOException {

		BinResponseHandler streamer = null;
		String serviceName = null;
		final JsonRequest fjr;

		try {
			Object[] params = null;
			// Check that we have a file upload request
			boolean isMultipart = request.getContentType() != null
					&& request.getContentType().toLowerCase().indexOf("multipart") > -1;
			if (isMultipart) {
				// upload service handler is a service with a single argument (Context)
				params = new Object[1];
				fjr = findMultipPartJsonRequest(request);

			} else {
				fjr = findJsonRequest(request);
				params  = jsh.deserializeServiceArgs(fjr.getServiceArgsJson());

			}
			serviceName = fjr.getService();

			if (StringUtil.isEmpty(serviceName)) {
				ApplicationException.exception("Missing serviceName");
			}

			final Method m = findService(serviceName, params.length);
			if (m == null)
				throw new UnsupportedOperationException(String.format(
						"Service=%s with %d args doesn't exists", serviceName, params.length));

			streamer = new DefaultBinResponseHandler(request, response) {
				@Override
				public void setContentType(String contentType, Map<String, String> headers) {
					super.setContentType(contentType, headers);
					if (fjr != null) {
						response.addHeader("Set-Cookie", fjr.getReqId()
								+ "=started; Max-Age=10; path=/");
					}
				}
			};

			final ResponseHandler<byte[]> fStreamer = streamer;
			final AjaxContext ctx = buildAjaxContext(request, response, fjr, fStreamer);
			final Object[] args = jsh.parseServiceArgs(params, m.getParameterTypes());
			args[0] = ctx;

			boolean xa = m.isAnnotationPresent(Transactional.class);
			if (xa && null == transactionManager) {
				logger.warn("Transcationnal service {} but there is no transaction manager!");
				xa = false;
			}

			if (xa) {
				Transactional trx = m.getAnnotation(Transactional.class);
				Preconditions.checkNotNull(transactionManager,
						"The method is transactionnal, but no transaction manager was detected!");
				TransactionTemplate trxTpl = new TransactionTemplate(transactionManager);
				trxTpl.setIsolationLevel(trx.isolation().value());
				trxTpl.setReadOnly(trx.readOnly());
				trxTpl.setPropagationBehavior(trx.propagation().value());
				trxTpl.setTimeout(trx.timeout());

				final String sn = serviceName;
				trxTpl.execute(new TransactionCallbackWithoutResult() {
					@Override
					protected void doInTransactionWithoutResult(TransactionStatus status) {
						try {
							m.invoke(getTargetCmp(), args);
						} catch (Throwable e) {
							status.setRollbackOnly();
							if (e instanceof InvocationTargetException) {
								handleException(ctx.getResponseHandler(), sn,
										((InvocationTargetException) e).getTargetException());
							} else {
								handleException(ctx.getResponseHandler(), sn, e);
							}
						}
					}
				});

			} else {
				m.invoke(getTargetCmp(), args);
			}

		} catch (InvocationTargetException e) {
			if (streamer != null)
				handleException(streamer, serviceName, e.getTargetException());
		} catch (Throwable e) {
			if (streamer != null)
				handleException(streamer, serviceName, e);
			else
				ApplicationException.exception(e);
		}
	}

	

	protected JsonRequest findMultipPartJsonRequest(final HttpServletRequest req)
			throws IOException {
		boolean isMultipart = req.getContentType().toLowerCase().indexOf("multipart") > -1;
		if (!isMultipart) {
			return null;
		}

		// Assume 4 first parts are (in no particular order) session id, service name, language and request id.
		// Do not read other parts. the upload handler (the service) will do read all the parts
		StringBuffer line = new StringBuffer();
		Map<String, String> argsMap = new HashMap<String, String>();

		boolean valueNext = false;
		String name = null;
		InputStream is = req.getInputStream();
		String boundary = null;
		while (true) {
			int b = is.read();
			if (b == -1) {
				break;
			} else if (b != 10 & b != 13) {
				line.append((char) b);
			}

			if (b == 10) {
				String str = line.toString();
				if (boundary == null) {
					boundary = str;
				} else {
					//lineCount += 1;
					if (str.equals(boundary)) {
						valueNext = false;
					} else {
						if (str.toLowerCase().startsWith("content-disposition")) {
							int pos = str.indexOf("name=");
							if (pos > -1) {
								String[] toks = str.split(";");
								name = toks[1].split("=")[1].replaceAll("\"", "");
							} else {
								throw new IllegalArgumentException(
										"second line should contain the part name!");
							}

						} else if (Strings.isNullOrEmpty(str)) {
							valueNext = true;

						} else if (valueNext) {
							argsMap.put(name, str);
						}
					}
				}
				line.setLength(0);
			}

			if (argsMap.size() == 4) {
				break;
			}
		}

		JsonRequest jr = new JsonRequest();
		String userId = null;
		if (req.getUserPrincipal() != null) {
			userId = req.getUserPrincipal().getName();
		}
		jr.setUserId(userId);
		jr.setSessionId(argsMap.get("sessionId"));
		jr.setDomain(req.getServerName());
		jr.setReqId(argsMap.get("reqId"));
		jr.setService(argsMap.get("service"));
		jr.setLang(argsMap.get("lang"));

		return jr;
	}
	
	protected AjaxContext buildAjaxContext(final HttpServletRequest request, final HttpServletResponse response,
			final JsonRequest fjr, final ResponseHandler<byte[]> fStreamer) {
		return new AjaxContext() {
			@Override
			public ResponseHandler<byte[]> getResponseHandler() {
				return fStreamer;
			}
			@Override
			public HttpServletResponse getHttpServletResponse() {
				return response;
			}
			@Override
			public HttpServletRequest getHttpServletRequest() {
				return request;
			}
			@Override
			public JsonRequest getRequest() {
				return fjr;
			}
		};
	}
}
