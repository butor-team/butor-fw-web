/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.web.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.butor.json.JsonHelper;
import org.butor.json.JsonRequest;
import org.butor.json.service.ResponseHandler;
import org.butor.utils.Message;
import org.butor.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.google.common.base.Preconditions;

public class DefaultAjaxComponent extends BaseAjaxComponent {
	protected Logger logger = LoggerFactory.getLogger(getClass());
	private JsonHelper jsh = new JsonHelper();
	//private static final int MAX_ROWS_PER_NOTIF = 40;
	public DefaultAjaxComponent(Object targetCmp) {
		super(targetCmp);
	}
	@Override
	public void process(final HttpServletRequest request, final HttpServletResponse response)
			throws ServletException, IOException {

		OutputStream os = null;
		ResponseHandler<Object> streamer = null;
		String serviceName = null;
		final List<Message> fmsgs = new ArrayList<Message>();
		final List<Object> frows = new ArrayList<Object>();
		ScriptStreamer notif = null;
		JsonRequest jr = null;
		try {
			jr = findJsonRequest(request);
			Object[] params  = jsh.deserializeServiceArgs(jr.getServiceArgsJson());
			final JsonRequest fjr = jr;

			serviceName = jr.getService();
			if (StringUtil.isEmpty(serviceName))
				throw new IllegalArgumentException("Missing serviceName");

			final Method m = findService(serviceName, params.length);
			if (m == null) {
				String msg = String.format("Service=%s with %d args does not exist!", 
					serviceName, params.length);
				logger.warn(msg);
				response.sendError(HttpServletResponse.SC_BAD_REQUEST,msg);
				throw new UnsupportedOperationException(
						msg);
			}
			if (!fjr.isStreaming()) {
				response.setContentType("application/json");
			} else {
				response.setContentType("text/html");
			}

			os = response.getOutputStream();
			final OutputStream fos = os;

			notif = fjr.isStreaming() ? new ScriptStreamer() : null;
			final ScriptStreamer fnotif = notif;

			final String reqId = jr.getReqId();

			streamer = new ResponseHandler<Object>() {
				int rowsPerNotif = 1;
				long rowCount = 0;
				boolean startedResp = false;
				@Override
				public void end() {
					if (!startedResp)
						try {
							//startResp(os, fjr.isStreaming(), reqId, notif.funcDef());
							startedResp = true;
							if (fjr.isStreaming()) {
								fos.write(fnotif.funcDef());
								fos.flush();
							} else {
								fos.write(String.format("{\"reqId\":\"%s\",\"data\":[", reqId).getBytes());
								fos.flush();
							}
						} catch (IOException e1) {
							logger.warn("Failed to write beginnig of response", e1);
						}
				}
				@Override
				public boolean addRow(Object row_) {
					if (row_ == null)
						return false;

					if (!startedResp)
						try {
							//startResp(os, fjr.isStreaming(), reqId, notif.funcDef());
							startedResp = true;
							if (fjr.isStreaming()) {
								fos.write(fnotif.funcDef());
								fos.flush();
							} else {
								fos.write(String.format("{\"reqId\":\"%s\",\"data\":[", reqId).getBytes());
								fos.flush();
							}
						} catch (IOException e1) {
							logger.warn("Failed to addRow", e1);
							return false;
						}

					rowCount++;
					try {
						if (fjr.isStreaming()) {
							// send small size notif first, then grows slowly
							// to reach a max. This way, results will hit the browser
							// sooner.
							frows.add(row_);
							if (frows.size() >= rowsPerNotif) { 
								if (rowsPerNotif < fjr.getRowsPerChunk())
									rowsPerNotif += 2;
								
								String json = jsh.serialize(frows);
								frows.clear();
								fos.write(fnotif.buildNotif(fjr.getReqId(), "row", json, false));
							}
						} else {
							if (rowCount>1)
								fos.write(',');
							String json = jsh.serialize(row_);
							fos.write(json.getBytes());
						}
						fos.flush();
						return true;
					} catch (IOException e) {
						logger.warn("Failed to addRow", e);
					}
					return false;
				}

				@Override
				public boolean addMessage(Message message_) {
					if (message_ == null)
						return false;

					if (!startedResp)
						try {
							//startResp(os, fjr.isStreaming(), reqId, notif.funcDef());
							startedResp = true;
							if (fjr.isStreaming()) {
								fos.write(fnotif.funcDef());
								fos.flush();
							} else {
								fos.write(String.format("{\"reqId\":\"%s\",\"data\":[", reqId).getBytes());
								fos.flush();
							}
						} catch (IOException e1) {
							logger.warn("Failed to addRow", e1);
							return false;
						}

					fmsgs.add(message_);
					try {
						if (fjr.isStreaming()) {
							if (frows.size() > 0) {
								String json = jsh.serialize(frows);
								frows.clear();
								fos.write(fnotif.buildNotif(fjr.getReqId(), "row", json, false));
							}

							String json = jsh.serialize(fmsgs);
							fmsgs.clear();
							fos.write(fnotif.buildNotif(fjr.getReqId(), "msg", json, false));
							fos.flush();
						}
						return true;
					} catch (IOException e) {
						logger.warn("Failed to write row or message", e);
					}
					return false;
				}
				@Override
				public Type getResponseType() {
					return Object.class;
				}
			};

			final ResponseHandler<Object> fStreamer = streamer;
			final AjaxContext ctx = buildAjaxContext(request, response, fjr, fStreamer);
			final Object[] args = jsh.parseServiceArgs(params, m.getParameterTypes());
			args[0] = ctx;
			
			boolean xa = m.isAnnotationPresent(Transactional.class);
			if (xa && null == transactionManager) {
				logger.warn("Transcationnal service {} but there is no transaction manager!");
				xa = false;
			}

			if (xa) {
				Transactional trx = m.getAnnotation(Transactional.class);
				Preconditions.checkNotNull(transactionManager,"The method is transactionnal, but no transaction manager was detected!");
				TransactionTemplate trxTpl = new TransactionTemplate(transactionManager);
				trxTpl.setIsolationLevel(trx.isolation().value());
				trxTpl.setReadOnly(trx.readOnly());
				trxTpl.setPropagationBehavior(trx.propagation().value());
				trxTpl.setTimeout(trx.timeout());
				
				final String sn = serviceName;
				trxTpl.execute(new TransactionCallbackWithoutResult() {
					@Override
					protected void doInTransactionWithoutResult(TransactionStatus status) {
						try {
							m.invoke(getTargetCmp(), args);
						} catch (Throwable e) {
							status.setRollbackOnly();
							if (e instanceof InvocationTargetException) {
								handleException(ctx.getResponseHandler(), sn, ((InvocationTargetException)e).getTargetException());
							} else {
								handleException(ctx.getResponseHandler(), sn, e);
							}
						}
					}});
				
			} else {
				m.invoke(getTargetCmp(), args);
			}

		} catch (InvocationTargetException e) {
			handleException(streamer, serviceName, e.getTargetException());
		} catch (Throwable e) {
			handleException(streamer, serviceName, e);
		} finally {
			if (streamer != null) {
				try {
					streamer.end();
	
					if (jr.isStreaming()) {
						if (frows.size() > 0) {
							String json = jsh.serialize(frows);
							frows.clear();
							os.write(notif.buildNotif(jr.getReqId(), "row", json, false));
						}
						os.write(notif.buildNotif(jr.getReqId(), "row", null, true));
					} else {
						os.write("],\"messages\":".getBytes());
						os.write(jsh.serialize(fmsgs).getBytes());
						os.write('}');
					}
					os.flush();
				} catch (Exception e) {
					logger.warn("Oh shit! Failed in finally.", e);
					//never mind
				}
			}
		}
	}
	protected AjaxContext buildAjaxContext(final HttpServletRequest request, final HttpServletResponse response,
			final JsonRequest fjr, final ResponseHandler<Object> fStreamer) {
		
		return new AjaxContext() {
			@Override
			public ResponseHandler<Object> getResponseHandler() {
				return fStreamer;
			}
			@Override
			public HttpServletResponse getHttpServletResponse() {
				return response;
			}
			@Override
			public HttpServletRequest getHttpServletRequest() {
				return request;
			}
			@Override
			public JsonRequest getRequest() {
				return fjr;
			}
		};
	}
}