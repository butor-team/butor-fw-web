/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.web.servlet;

import static com.google.common.base.Strings.isNullOrEmpty;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.butor.json.service.BinResponseHandler;
import org.butor.utils.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultBinResponseHandler implements BinResponseHandler {
	protected Logger logger = LoggerFactory.getLogger(getClass());

	protected final HttpServletRequest request;
	protected final HttpServletResponse response;
	protected final OutputStream os;
	protected final InputStream is;

	public DefaultBinResponseHandler(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		this.request = request;
		this.response = response;
		this.os = response.getOutputStream();
		this.is = request.getInputStream();
	}

	@Override
	public void end() {
	}

	@Override
	public boolean addRow(byte[] row) {
		try {
			os.write(row);
			os.flush();
			return true;
		} catch (IOException e) {
			logger.warn("Failed to addRow", e);
		}
		return false;
	}

	@Override
	public boolean addMessage(Message message) {
		return true;
	}

	@Override
	public void setContentType(String contentType, Map<String, String> headers) {
		if (!isNullOrEmpty(contentType)) {
			response.setContentType(contentType);
		}
		if (headers != null) {
			for (String h : headers.keySet()) {
				response.addHeader(h, headers.get(h));
			}
		}
	}

	@Override
	public OutputStream getOutputStream() {
		return os;
	}

	@Override
	public Class<byte[]> getResponseType() {
		return byte[].class;
	}

	@Override
	public InputStream getInputStream() {
		return is;
	}
}
