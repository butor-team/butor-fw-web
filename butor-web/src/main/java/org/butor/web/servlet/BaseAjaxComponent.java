/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.web.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.net.ConnectException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.servlet.http.HttpServletRequest;

import org.butor.json.JsonRequest;
import org.butor.json.service.Context;
import org.butor.json.service.ResponseHandler;
import org.butor.utils.ApplicationException;
import org.butor.utils.CommonMessageID;
import org.butor.utils.Message;
import org.butor.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.transaction.PlatformTransactionManager;

import com.google.api.client.http.HttpResponseException;
import com.google.common.base.Strings;
import com.google.common.base.Throwables;

public abstract class BaseAjaxComponent implements AjaxComponent, 
		InitializingBean, BeanFactoryAware {
	
	protected Logger logger = LoggerFactory.getLogger(getClass());
	protected ConcurrentMap<String, Method> servicesMap = 
			new ConcurrentHashMap<String, Method>();
	protected Object targetCmp = null;
	

	protected PlatformTransactionManager transactionManager;
	protected BeanFactory beanFactory;

	protected boolean allowHtmlTagsInServicesArgs = true;

	protected String requestBaseArgsPattern = "^[a-zA-Z0-9_\\\\.\\\\-]+$";
	protected String[] requestBaseArgs = new String[] {"sessionId", "reqId", "service", "lang", "streaming"};

	public BaseAjaxComponent(Object targetCmp) {
		this.targetCmp = targetCmp;
		Method[] methods = this.targetCmp.getClass().getMethods();
		if (methods.length == 0) {
			return;
		}

		for (Method m : methods) {
			Class<?> rt = m.getReturnType();
			Class<?>[] pts = m.getParameterTypes();

			if (pts.length > 0 && pts[0].isAssignableFrom(Context.class) && rt.equals(void.class)) { 
				String key = buildKey(m.getName(), pts.length);
				if (servicesMap.containsKey(key)) {
					logger.warn(String.format("method %s with %d args has been mapped already. Ignoring similar one!", m.getName(), pts.length));
					continue;
				}
				logger.info("Adding service {} with {} args",m.getName(),pts.length);
				servicesMap.put(key, m);
			}	
		}
	}
	protected String buildKey(String methodName, int nbArgs) {
		return String.format("%s.%d", methodName, nbArgs);
	}
	protected void handleException(final ResponseHandler<?> handler, String serviceName, Throwable e) {
		logger.error(String.format("Failed! service=%s, cmp=%s", 
				serviceName, targetCmp.getClass().getName()), e);
		if (handler == null) {
			return;
		}
		if (e instanceof ApplicationException) {
			ApplicationException appEx = (ApplicationException)e;
			for (Message message : appEx.getMessages()) {
				handler.addMessage(message);
			}
			
		} else {
			Throwable th = Throwables.getRootCause(e);
			if (th instanceof ConnectException) {
				handler.addMessage(CommonMessageID.SERVICE_NOT_AVAILABLE.getMessage());
			} else if (th instanceof HttpResponseException) {
				HttpResponseException hre = (HttpResponseException)th;
				logger.error(String.format("Failed to connect service=%s, cmp=%s, HttpStausCode=%s", 
						serviceName, targetCmp.getClass().getName(), hre.getStatusCode()), e);
				if (hre.getStatusCode() == 504) {//gateway timeout
					handler.addMessage(CommonMessageID.SERVICE_TIMEOUT.getMessage());
				} else {
					handler.addMessage(CommonMessageID.SERVICE_FAILURE.getMessage("HttpResponseException HttpStatusCode=" +hre.getStatusCode()));
				}
			} else {
				logger.error(String.format("Failed to invoke service=%s, cmp=%s", 
						serviceName, targetCmp.getClass().getName()), e);
				handler.addMessage(CommonMessageID.SERVICE_FAILURE.getMessage());
			}
		}
	}

	protected JsonRequest findJsonRequest(final HttpServletRequest req) throws IOException {
		JsonRequest jr = null;
		InputStream is = null;
		String qs;
		if (req.getMethod().equalsIgnoreCase("post")) {
			is = req.getInputStream();
			StringWriter sw = new StringWriter();
			while (true) {
				int bb = is.read();
				if (bb == -1)
					break;
				sw.write(bb);
			}
			qs = sw.toString();
		} else {
			qs = req.getQueryString();
		}

		Map<String,String> argsMap = new HashMap<String, String>();
		String[] toks = qs.split("&");
		for (String tok : toks) {
			tok = URLDecoder.decode(tok, "utf-8");
			String[] pair = tok.split("=");
			argsMap.put(pair[0], pair.length > 1 ? pair[1] : null);
		}

		if (requestBaseArgsPattern != null && requestBaseArgs != null) {
			for (String arg : requestBaseArgs) {
				if (!Strings.isNullOrEmpty(argsMap.get(arg)) && !argsMap.get(arg).matches(requestBaseArgsPattern)) {
					ApplicationException.exception(String.format("bad pattern of arg %s=%s", arg, argsMap.get(arg)));
				}
			}
		}

		jr = new JsonRequest();
		String userId = null;
		if (req.getUserPrincipal() != null)
			userId = req.getUserPrincipal().getName();
		jr.setUserId(userId);
		String sessionId = argsMap.get("sessionId");
		
		if (Strings.isNullOrEmpty(sessionId)) {
			sessionId = req.getSession().getId();
		}
		jr.setSessionId(sessionId);
		jr.setDomain(req.getServerName());
		jr.setReqId(argsMap.get("reqId"));
		jr.setService(argsMap.get("service"));
		jr.setLang(argsMap.get("lang"));
		String val = argsMap.get("streaming");
		jr.setStreaming(StringUtil.isEmpty(val) ? false : val.equalsIgnoreCase("yes") || val.equalsIgnoreCase("true"));

		val = argsMap.get("rowsPerChunk");
		if (!StringUtil.isEmpty(val)) {
			try {
				jr.setRowsPerChunk(Integer.valueOf(val));
			} catch (NumberFormatException e) {
				logger.warn("Failed to parse request rowsPerNotif {}", val);
			}
		}
		
		String args = URLDecoder.decode(Strings.nullToEmpty(argsMap.get("args")), "UTF-8");
		if (!allowHtmlTagsInServicesArgs) {
			args = args.replaceAll("\\\\u003c|<", "&lt;").replaceAll("\\\\u003e|>", "&gt;");
		}
		jr.setServiceArgsJson(args);

		return jr;
	}
	protected Method findService(String serviceName, int nbArgs) {
		String key = buildKey(serviceName, nbArgs);
		Method m = servicesMap.get(key);
		return m;
	}
	protected Object getTargetCmp() {
		return targetCmp;
	}
	@Override
	public void afterPropertiesSet() throws Exception {
		try {
			transactionManager = beanFactory.getBean(PlatformTransactionManager.class);
		} catch (NoSuchBeanDefinitionException e) {
			logger.warn("No (or more than one) TransactionManager defined in your Spring context. Will not set transaction manager.",e);
		}
	}

	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory = beanFactory;
		
	}
	public void setAllowHtmlTagsInServicesArgs(boolean allowHtmlTagsInServicesArgs) {
		this.allowHtmlTagsInServicesArgs = allowHtmlTagsInServicesArgs;
	}
	public void setRequestBaseArgsPattern(String requestBaseArgsPattern) {
		this.requestBaseArgsPattern = requestBaseArgsPattern;
	}
}
