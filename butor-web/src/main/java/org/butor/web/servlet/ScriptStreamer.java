/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.web.servlet;

import java.io.StringWriter;

public class ScriptStreamer {
	private final String DIV_START = "<div id=\"div%d\">";
	private final String SCRIPT_START = "<script type=\"text/javascript\" charset=\"UTF-8\">";
	private final String SCRIPT_NOTIF_START = "_hn(";
	private final String SCRIPT_NOTIF_END = ");";
	private final String SCRIPT_END = "</script>";
	private final String DIV_END = "</div>\n";
	private final String CLEAN_DIV = "_cd(\"div%s\");";

	private long _pushCount = 0;

	public byte[] funcDef() {
		StringWriter notif = new StringWriter();
		notif.write(SCRIPT_START);
		notif.write("function _hn(n_){try{window.parent.AJAX.onNotification(n_);} catch(er){window.parent.LOGGER.error(er);}}\n");
		notif.write("function _cd(d_){try{var div=document.getElementById(d_);if (div) div.parentNode.removeChild(div);} catch(er){window.parent.LOGGER.error(er);}}\n");
		notif.write(SCRIPT_END);

		return notif.toString().getBytes();
	}
	public byte[] buildNotif(String reqId_, String type_, String json_, boolean complete_) {
		StringWriter notif = new StringWriter();
		notif.write(String.format(DIV_START, _pushCount));
		notif.write(SCRIPT_START);
		notif.write(SCRIPT_NOTIF_START);

		notif.write("{\"reqId\":\"");
		notif.write(reqId_);
		notif.write("\"");
		if (type_ != null) {
			notif.write(",\"type\":\"");
			notif.write(type_);
			notif.write("\"");
		}
		if (complete_) {
			notif.write(",\"complete\":true");
		}
		if (json_ != null) {
			notif.write(",\"data\":");
			notif.write(json_);
		}
		notif.write("}");

		notif.write(SCRIPT_NOTIF_END);
		notif.write(String.format(CLEAN_DIV, _pushCount++));
		notif.write(SCRIPT_END);
		notif.write(DIV_END);

		return notif.toString().getBytes();
	}
}
