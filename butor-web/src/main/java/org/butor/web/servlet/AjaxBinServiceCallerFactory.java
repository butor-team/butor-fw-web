/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.web.servlet;

import java.util.Set;

import org.butor.json.service.BinServiceCallerFactory;
import org.butor.json.service.ServiceCallerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Factory class that return a proxy of the service interface
 * that calls a remote service
 * 
 * This eliminate the need of creating a new service class
 * that use a service caller to invoke remote services with identical 
 * name and signature (pass through services). 
 * 
 * @author asawan
 *
 */
public class AjaxBinServiceCallerFactory extends AjaxServiceCallerFactory<byte[]> {
	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public ServiceCallerFactory<byte[]> createServiceCallerFactory(String namespace, 
			String url, int maxPayloadLengthToLog, Set<String> servicesToNotLogArgs) {
		return new BinServiceCallerFactory<byte[]>(namespace, url, maxPayloadLengthToLog, servicesToNotLogArgs);
	}
}
