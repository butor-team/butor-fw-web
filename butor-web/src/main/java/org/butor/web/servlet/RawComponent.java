/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.web.servlet;

import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.butor.utils.ApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.google.common.base.Preconditions;

/**
 * @author asawan
 *
 */
public class RawComponent extends BaseAjaxComponent {
	protected Logger _logger = LoggerFactory.getLogger(getClass());
	public RawComponent(Object targetCmp) {
		super(targetCmp);
		
		// map (request, response) methods
		Method[] methods = targetCmp.getClass().getMethods();
		for (Method m : methods) {
			Class<?> rt = m.getReturnType();
			Class<?>[] pts = m.getParameterTypes();

			if (pts.length == 2 && pts[0].isAssignableFrom(HttpServletRequest.class) &&
					pts[1].isAssignableFrom(HttpServletResponse.class) && rt.equals(void.class)) { 
				String key = buildKey(m.getName(), pts.length);
				if (servicesMap.containsKey(key)) {
					logger.warn(String.format("method %s with %d args has been mapped already. Ignoring similar one!"));
					continue;
				}
				logger.info("Adding service {} with {} args",m.getName(),pts.length);
				servicesMap.put(key, m);
			}	
		}

	}
	@Override
	public void process(final HttpServletRequest request, final HttpServletResponse response)
			throws ServletException, IOException {

		try {
			final String sn = request.getQueryString();
			logger.info("Invoquing {} ...", sn);

			final Method m = servicesMap.get(buildKey(sn, 2));
			if (m == null)
				throw new UnsupportedOperationException(
					String.format("Service=%s with %d args doesn't exists", 
						sn, 2));

			final Object[] args = new Object[]{request, response};
			boolean xa = m.isAnnotationPresent(Transactional.class);
			if (xa && null == transactionManager) {
				logger.warn("Transcationnal service {} but there is no transaction manager!");
				xa = false;
			}

			if (xa) {
				Transactional trx = m.getAnnotation(Transactional.class);
				Preconditions.checkNotNull(transactionManager,"The method is transactionnal, but no transaction manager was detected!");
				TransactionTemplate trxTpl = new TransactionTemplate(transactionManager);
				trxTpl.setIsolationLevel(trx.isolation().value());
				trxTpl.setReadOnly(trx.readOnly());
				trxTpl.setPropagationBehavior(trx.propagation().value());
				trxTpl.setTimeout(trx.timeout());
				
				trxTpl.execute(new TransactionCallbackWithoutResult() {
					@Override
					protected void doInTransactionWithoutResult(TransactionStatus status) {
						try {
							m.invoke(getTargetCmp(), args);
						} catch (Throwable e) {
							status.setRollbackOnly();
							if (e instanceof InvocationTargetException) {
								handleException(response, sn, ((InvocationTargetException)e).getTargetException());
							} else {
								handleException(response, sn, e);
							}
						}
					}});
				
			} else {
				m.invoke(getTargetCmp(), args);
			}			

		} catch (InvocationTargetException e) {
			ApplicationException.exception(e);
		} catch (Throwable e) {
			ApplicationException.exception(e);
		}
	}

	protected void handleException(final HttpServletResponse response, String serviceName, Throwable e) {
		String msg = String.format("Failed! service=%s, cmp=%s", 
				serviceName, targetCmp.getClass().getName());
		logger.error(msg, e);
		try {
			response.getOutputStream().write(msg.getBytes());
			response.getOutputStream().write('\n');
			e.printStackTrace(new PrintStream(response.getOutputStream()));
		} catch (IOException ex) {
			logger.error("Failed while printing exception to HttpServletResponse OutputStream", ex);
		}
	}
}
