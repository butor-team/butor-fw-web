/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.web.servlet;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.google.api.client.http.HttpStatusCodes;
import com.google.api.client.repackaged.com.google.common.base.Strings;

/**
 * @author asawan
 * 
 */
public class AjaxControllerServlet extends HttpServlet {

	private static final long serialVersionUID = 5144202269971046027L;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private ServletContext sctx = null;
	private String componentPrefix;

	@Override
	protected void service(HttpServletRequest req_, HttpServletResponse resp_)
			throws ServletException, IOException {
		String ctxt = req_.getContextPath();
		String uri = req_.getRequestURI();
		String path = uri.substring(ctxt.length() +1 +componentPrefix.length());
		logger.info("Servicing component {} ...", path);
		
		WebApplicationContext wac = WebApplicationContextUtils.
				getRequiredWebApplicationContext(sctx);

		AjaxComponent ajaxComponent = null;
		try {
			ajaxComponent = wac.getBean(path, AjaxComponent.class);
		} catch (NoSuchBeanDefinitionException ex) {
			//OK will be considered as null
		}
		if (ajaxComponent == null) {
			logger.info("No component found {}!", path);
			resp_.setStatus(HttpStatusCodes.STATUS_CODE_NOT_FOUND);
			return;
		}
		logger.info("Processing by component {} ...", path);
		ajaxComponent.process(req_, resp_);
	}

	@Override
	public void init(ServletConfig config) {
		logger.info("Initializing ...");
		sctx = config.getServletContext();
		componentPrefix = Strings.nullToEmpty(config.getInitParameter("componentPrefix"));
		logger.info("Initialized");
	}
}
