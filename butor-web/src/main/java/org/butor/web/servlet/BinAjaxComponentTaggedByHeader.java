/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.web.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.butor.json.JsonRequest;
import org.butor.json.service.ResponseHandler;

public class BinAjaxComponentTaggedByHeader extends BinAjaxComponent {
	private String requestIdSeparator = "---";
	private String headerKey = "";
	public BinAjaxComponentTaggedByHeader(Object targetCmp) {
		super(targetCmp);
	}
	@Override
	protected AjaxContext buildAjaxContext(final HttpServletRequest request, final HttpServletResponse response,
			final JsonRequest fjr, final ResponseHandler<byte[]> fStreamer) {
		return new AjaxContext() {
			@Override
			public ResponseHandler<byte[]> getResponseHandler() {
				return fStreamer;
			}
			@Override
			public HttpServletResponse getHttpServletResponse() {
				return response;
			}
			@Override
			public HttpServletRequest getHttpServletRequest() {
				return request;
			}
			@Override
			public JsonRequest getRequest() {
				JsonRequest fjrcopy = fjr.clone();
				String reqId = fjr.getReqId();
				String custoMreqId = String.format("%s%s%s", reqId,requestIdSeparator,getCustomReqId(request));
				fjrcopy.setReqId(custoMreqId);
				return fjrcopy;
			}

		};
	}
	public String getRequestIdSeparator() {
		return requestIdSeparator;
	}
	public void setRequestIdSeparator(String requestIdSeparator) {
		this.requestIdSeparator = requestIdSeparator;
	}
	private String getCustomReqId(HttpServletRequest request) {
		// exmple X-Amzn-Trace-Id ...
		return request.getHeader(headerKey);
	}
	public String getHeaderKey() {
		return headerKey;
	}
	public void setHeaderKey(String headerKey) {
		this.headerKey = headerKey;
	}
}