#deprecated, we use plugin
exit 1;

OUTF=../resources/butor/js
java -jar /opt/closure-compiler/compiler.jar \
--js ../resources/butor/js/butor.js \
--js ../resources/butor/js/butor-dlg.js \
--js ../resources/butor/js/butor-app.js \
--js ../resources/butor/js/butor-table.js \
--js ../resources/butor/js/bundle.js \
--js ../resources/butor/js/butor-panels.js \
--js ../resources/butor/js/butor-upload.js \
--js ../resources/butor/js/butor-live-search.js \
--js ../resources/butor/js/butor-row-editor.js \
--js_output_file $OUTF/butor.min.js \
--language_in ECMASCRIPT5 \
--compilation_level SIMPLE_OPTIMIZATIONS 
