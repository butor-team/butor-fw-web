/*
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @namespace butor.dlg
 */
butor.dlg = {};

/**
 * @class Dialog
 * @abstract
 * @memberof butor.dlg
 * @required {http://fstoke.me/jquery/window/ jQuery Window Plugin}
 * @description Creates a dialog window.
 *
 * This abstract class provides a set of methods to make a dialog box
 * for the platform. The method `_init` needs to be implemented:
 * it initialilizes the dialog box and **must call the base Class
 * by calling the method `this.base` in order to call the MsgPanel interface**.
 *
 * The dialog box can be used and configurated by both methods:
 * {@link butor.dlg.Dialog#setUrl setUrl} and {@link butor.dlg.Dialog#setHtml setHtml}.
 * These methods takes arguments in order to set up the content of the dialog and you **must**
 * use one of them in order to build the dialog. These methods are automaticaly
 * created as properties of the Dialog class.
 *
 * @param {object} dlgOpts - Options of the dialog, it is used to configure
 * the dialog.
 * @param {string} dlgOpts.title - Name of the dialog.
 * @param {number} dlgOpts.width - Width of the dialog.
 * @param {number} dlgOpts.height - Heigth of the dialog.
 * @param {boolean} [dlgOpts.resizable=false] - Flag to make the dialog resizable.
 * @param {boolean} [dlgOpts.maximizable=false] - Flag to make the dialog maximizable.
 * @param {boolean} [dlgOpts.minimizable=false] - Flag to make the dialog minimizable.
 * @param {boolean} [dlgOpts.draggable=false] - Flag to make the dialog draggable.
 * @param {boolean} [dlgOpts.modal=false] - Flag to make the dialog modal.
 * @param {string} dlgOpts.bundleId - ID of the bundle linked to the dialog.
 *
 * @example
 * <div id='exampleDlg'>
 * 	<div class="dlg-tool-bar row">
 * 		<div class="col-sm-12">
 * 			<ul id="tabs" class="nav nav-pills" style="margin-bottom: 2px;">
 * 				<li id="tabFirst" class="active"><a><i class="icon-lock"></i>First</a></li>
 * 				<li id="tabSecond"><a><i class="icon-filter"></i>Second</a></li>
 * 			</ul>
 * 		</div>
 * 	</div>
 * 	<div class="dlg-content">
 * 		<div id="firstPanel">
 * 			lorem_ipsum
 * 		</div>
 * 		<div id="secondPanel" style="display: none">
 * 			Hi, this is a second tab !
 * 			<br/>
 * 			<button class="btn btn-primary" onClick='onDlgMsg(this)'>Send an info</button>
 * 			<button class="btn btn-warning" onClick='onDlgMsg(this)'>Send a warning</button>
 * 			<button class="btn btn-danger" onClick='onDlgMsg(this)'>Send an error</button>
 * 		</div>
 * 	</div>
 * 	<div class="dlg-button-bar align-right">
 * 		<button class="btn btn-default">Cancel</button>
 * 		<button type="button" class="btn btn-default">Ok</button>
 * 	</div>
 * </div>
 *
 * @example
 * //Custom implementation of the abstract class butor.dlg.Dialog
 * butor.ExampleDlg = butor.ExampleDlg || butor.dlg.Dialog.define({
 *   //Constructor
 *   construct: function(html) {
 *     //Define the options of the dialog box
 *     dlgOpts = {
 *       title : "ExampleDlg",
 *       width : 500,
 *       height : 430,
 *       resizable: true,
 *       maximizable: true,
 *       bundleId: 'exampleBundle'
 *     };
 *
 *     //Call the base classe (butor.dlg.Dialog#construct)
 *     this.base(dlgOpts);
 *
 *     //Set the URL (or the HTML) of the Dialog
 *     //setUrl('/example/ExampleDlg.html');
 *     this.setHtml(html);
 *   },
 *   //Implementation of the initializer
 *   _init: function() {
 *     this.base();
 *     //Some stuff...
 *   },
 *   //Custom methods
 * });
 *
 * var exampleDlg = new butor.ExampleDlg($('#exampleDlg'));
 * exampleDlg.show();
 *
 * var onDlgMsgPanelRequest = function(e) {
 * 	if (!example_dlg || !e.className) {
 * 		return;
 * 	}
 *
 * 	switch (e.className.split(' ')[1]) {
 * 		case 'btn-danger':
 * 			example_dlg.msgError('This is an error!');
 * 			break;
 * 		case 'btn-warning':
 * 			example_dlg.msgWarn('This is a warning!');
 * 			break;
 * 		default:
 * 			example_dlg.msgInfo('This is an info.');
 * 	}
 * }
 */

butor.dlg.Dialog = butor.Class.define({
	statics: {
		/**
		 * @var {Object} _registry
		 * @access private
		 * @memberof butor.dlg.Dialog
		 * @description Registry for the dialog.
		 */
		_registry: {},

		/**
		 * @method getDlg
		 * @memberof butor.dlg.Dialog
		 * @description Gets the dialog.
		 * @param {Number} id - The ID of the expected dialog.
		 */
		getDlg: function(id) {
			return butor.dlg.Dialog_registry[id];
		}
	},
	_args: null,
	_win: null,
	_anchor: null,
	_dom: null,
	_jqe: null,
	_msgPanel: null,
	properties: {
		/**
		 * @method getUrl
		 * @memberof butor.dlg.Dialog#
		 * @description Gets the url of the dialog.
		 * @returns {string} The url of the dialog.
		 */
		 /**
			* @method setUrl
			* @memberof butor.dlg.Dialog#
			* @description Sets the url of the dialog.
			* @param {string} url - The url of the dialog.
			*/
		url: {
			type: 'String'
		},
		/**
		 * @method getHtml
		 * @memberof butor.dlg.Dialog#
		 * @description Gets the html of the dialog.
		 * @returns {string} The html of the dialog.
		 */
		 /**
			* @method setHtml
			* @memberof butor.dlg.Dialog#
			* @description Sets the html of the dialog.
			* @param {string} The html of the dialog.
			*/
		html: {
			type: 'String'
		}
	},
	construct: function(dlgOpts) {
		$.Window.prepare({
			handleScrollbar: true
		});
		this._id = null;
		this._dlgOpts = dlgOpts;
		/*
		 * if (!this._dlgOpts) { throw new Error("Missing options."); } if
		 * (!this._dlgOpts.url && !this._dlgOpts.html) { throw new Error(
		 * "Missing url and html options. Should provide one them."); }
		 */
	},

	/**
	 * @method _init
	 * @access private
	 * @memberof butor.dlg.Dialog#
	 * @description Initializes the dialog.
	 */
	_init: function() {
		// TODO to be overloaded by dialogs impl (subclasses).
		this._createMsgPanel();
	},

	/**
	 * @method _createMsgPanel
	 * @access private
	 * @memberof butor.dlg.Dialog#
	 * @description Creates a default message panel.
	 */
	_createMsgPanel: function() {
		this._msgPanel = new butor.panel.MsgPanel(this._jqe.find(".butor-dlg"),
			'80%', '24px');
		this._msgPanel.hide();
	},

	/**
	 * @method getMsgPanel
	 * @memberof butor.dlg.Dialog#
	 * @description Gets the current MessagePanel of the Dialog.
	 * @returns {butor.panel.MsgPanel} The current message panel.
	 * @see butor.panel.MsgPanel
	 */
	getMsgPanel: function() {
		return this._msgPanel;
	},

	/**
	 * @method msgError
	 * @memberof butor.dlg.Dialog#
	 * @description Displays an error message in the dialog.
	 * @param {String} message - Error message to display.
	 */
	msgError: function(msg) {
		this._msgPanel.error(msg);
	},

	/**
	 * @method msgInfo
	 * @memberof butor.dlg.Dialog#
	 * @description Displays an info message in the dialog.
	 * @param {String} message - Info message to display.
	 */
	msgInfo: function(msg) {
		this._msgPanel.info(msg);
	},

	/**
	 * @method msgWarn
	 * @memberof butor.dlg.Dialog#
	 * @description Displays an warning message in the dialog.
	 * @param {String} message - Warning message to display.
	 */
	msgWarn: function(msg) {
		this._msgPanel.warning(msg);
	},

	/**
	 * @method showMsg
	 * @memberof butor.dlg.Dialog#
	 * @description Shows a message.
	 * @param {String} message - Message to show.
	 */
	showMsg: function(msg) {
		this._msgPanel.showMsg(msg);
	},

	/**
	 * @method hideMsg
	 * @memberof butor.dlg.Dialog#
	 * @description Hides the current MessagePanel of the Dialog.
	 */
	hideMsg: function() {
		this._msgPanel.hide();
	},

	/**
	 * @method getId
	 * @memberof butor.dlg.Dialog#
	 * @description Gets the dialog ID.
	 * @return {Number} The ID of the dialog.
	 */
	getId: function() {
		return this._id;
	},

	/**
	 * @method getWin
	 * @memberof butor.dlg.Dialog#
	 * @description Gets the window of the dialog.
	 * @return {Object} The window of the dialog.
	 */
	getWin: function() {
		return this._win;
	},

	/**
	 * @method resize
	 * @memberof butor.dlg.Dialog#
	 * @description Resizes the dialog.
	 * @param {Number} width - Width to resize.
	 * @param {Number} heigth - Heigth to resize.
	 */
	resize: function(w, h) {
		this._win && this._win.resize(w, h);
		this._jqe.find('.window_frame').width(w).height(h);
	},

	/**
	 * @method setTitle
	 * @memberof butor.dlg.Dialog#
	 * @description Sets the title of the dialog.
	 * @param {String} title - Title of the dialog.
	 */
	setTitle: function(title) {
		this._dlgOpts.title = title;
		this._win && this._win.setTitle(title);
	},

	/**
	 * @method getTitle
	 * @memberof butor.dlg.Dialog#
	 * @description Get the title of the dialog.
	 */
	getTitle: function() {
		return this._win == null ? null : this._win.getTitle();
	},

	/**
	 * @method maximize
	 * @memberof butor.dlg.Dialog#
	 * @description Maximizes the dialog.
	 */
	maximize: function() {
		this._win.maximize();
	},

	/**
	 * @method minimize
	 * @memberof butor.dlg.Dialog#
	 * @description Minimizes the dialog.
	 */
	minimize: function() {
		this._win.minimize();
	},

	/**
	 * @method restore
	 * @memberof butor.dlg.Dialog#
	 * @description Restores the dialog.
	 */
	restore: function() {
		this._win.restore();
	},

	/**
	 * @method hide
	 * @memberof butor.dlg.Dialog#
	 * @description Hides the dialog.
	 */
	hide: function() {
		this._win.hide();
	},

	/**
	 * @method width
	 * @memberof butor.dlg.Dialog#
	 * @description Gets the width of the dialog.
	 * @return {Number} The width of the dialog.
	 */
	width: function() {
		return this._win.getContainer().width();
	},

	/**
	 * @method height
	 * @memberof butor.dlg.Dialog#
	 * @description Gets the height of the dialog.
	 * @return {Number} The height of the dialog.
	 */
	height: function() {
		return this._win.getContainer().height();
	},

	/**
	 * @method setResizable
	 * @memberof butor.dlg.Dialog#
	 * @description Decides if the dialog is resizable or not.
	 * @param {Boolean} resizable - Resizable switch.
	 */
	setResizable: function(resizable) {
		this._dlgOpts.resizable = resizable;
		this._win && this._win.setResizable(resizable);
	},

	/**
	 * @method setDraggable
	 * @memberof butor.dlg.Dialog#
	 * @description Decides if the dialog is draggable or not.
	 * @param {Boolean} draggable - Draggable switch.
	 */
	setDraggable: function(draggable) {
		this._dlgOpts.draggable = draggable;
		this._win && this._win.setDraggable(draggable);
	},

	/**
	 * @method setMaximizable
	 * @memberof butor.dlg.Dialog#
	 * @description Decides if the dialog is maximizable or not.
	 * @param {Boolean} maximizable - Maximizable switch.
	 */
	setMaximizable: function(maximizable) {
		this._dlgOpts.maximizable = maximizable;
		this._win && this._win.setMaximizable(maximizable);
	},

	/**
	 * @method setMinimizable
	 * @memberof butor.dlg.Dialog#
	 * @description Decides if the dialog is minimizable or not.
	 * @param {Boolean} minimizable - Minimizable switch.
	 */
	setMinimizable: function(minimizable) {
		this._dlgOpts.minimizable = minimizable;
		this._win && this._win.setMinimizable(minimizable);
	},

	/**
	 * @method setModal
	 * @memberof butor.dlg.Dialog#
	 * @description Decides if the dialog is modal or not.
	 * @param {Boolean} modal - Modal switch.
	 */
	setModal: function(modal) {
		this._dlgOpts.modal = modal;
		this._win && this._win.setModal(modal);
	},

	/**
	 * @method getDom
	 * @memberof butor.dlg.Dialog#
	 * @description Gets the DOM linked to the dialog.
	 * @return {JQueryElement} The JQueryElement of the dialog.
	 */
	getDom: function() {
		return this._jqe;
	},

	/**
	 * @method close
	 * @memberof butor.dlg.Dialog#
	 * @description Closes the dialog.
	 */
	close: function() {
		if (!this._jqe || this._jqe.attr('_isClosing_') === '1') {
			return;
		}
		this._jqe.attr('_isInternalClose_', '1');
		$(window).unbind('resize', this._wrp);
		this._win.close();
	},

	/**
	 * @method mask
	 * @memberof butor.dlg.Dialog#
	 * @description Masks a message in the dialog.
	 * @param {String} msg - Message to mask.
	 */
	mask: function(msg) {
		this._win.getContainer().mask(msg);
	},

	/**
	 * @method unmask
	 * @memberof butor.dlg.Dialog#
	 * @description Unmasks the dialog.
	 */
	unmask: function() {
		this._win.getContainer().unmask();
	},

	/**
	 * @method _showDlg
	 * @access private
	 * @memberof butor.dlg.Dialog#
	 * @description Shows the dialog.
	 * @param {HTMLObject} html - HTML of the dialog.
	 * @fires butor.dlg.Dialog#resized
	 * @fires butor.dlg.Dialog#maximized
	 * @fires butor.dlg.Dialog#minimized
	 * @fires butor.dlg.Dialog#cascaded
	 */
	_showDlg: function(html) {
		var html = '<div class="butor-dlg">' + html + '</div>';
		var dlg = this;
		this._win = $.window({
			showRoundCorner: true,
			title: this._dlgOpts.title,
			content: html,
			// footerContent : "HELLO",
			// showFooter: false,
			maxWidth: -1,
			maxHeight: -1,
			width: this._dlgOpts.width,
			height: this._dlgOpts.height,
			// headerClass: '',
			// frameClass: '',
			scrollable: this._dlgOpts.scrollable || false,
			draggable: this._dlgOpts.draggable == null ? true : this._dlgOpts.draggable,
			resizable: this._dlgOpts.resizable == null ? false : this._dlgOpts.resizable,
			maximizable: this._dlgOpts.maximizable == null ? false : this._dlgOpts.maximizable,
			minimizable: this._dlgOpts.minimizable == null ? false : this._dlgOpts.minimizable,
			showModal: typeof this._dlgOpts.modal !== 'undefined' ? this._dlgOpts.modal : true,
			closable: this._dlgOpts.closable == null ? true : this._dlgOpts.closable,
			afterResize: function() {
				dlg.fire('resized');
			},
			afterMaximize: function() {
				dlg.fire('resized');
				dlg.fire('maximized');
				dlg._win.resize();
			},
			afterMinimize: function() {
				dlg.fire('resized');
				dlg.fire('minimized');
			},
			afterCascade: function() {
				dlg.fire('resized');
				dlg.fire('cascaded');
			},
			onClose: function(wnd) {
				// TODO delete App.dlg.registry[_id];
				if (dlg._anchor) {
					dlg._anchor.remove();
				}
				this.html = null;
				dlg._anchor = null;
				if (dlg._jqe.attr('_isClosing_') === '1') {
					return;
				}
				dlg._jqe.attr('_isClosing_', '1');
				dlg.fire('closed');
			}
		});
		this._id = this._win.getWindowId();
		// TODO App.dlg.registry[_id] = _win;

		// the ui-widget-content class cut the bg color! remove it
		// temporarly
		$('#' + this._id + ' > div:nth-child(2)').removeClass(
			"ui-widget-content");
		this._jqe = this._win.getContainer();
		this._dom = this._jqe; //backward compatibility. Still reference to _dom
		App.translateElem(this._jqe, this._dlgOpts.bundleId);

		if (this._dlgOpts.maximized || this.width() > $(window).width() || this.height() >
			$(window).height()) {
			this.maximize();
		}
		this._wrp = $.proxy(this._windowResized, this);
		$(window).bind('resize', this._wrp);
		this._init();
	},

	/**
	 * @method _windowResized
	 * @access private
	 * @memberof butor.dlg.Dialog#
	 * @description Fires a "resized" event for the dialog.
	 */
	 //TODO window plugin do not fire resize event if is it maximized
	_windowResized: function() {
		//TODO window plugin do not fire resize event if is it maximized
		// if browser window is resized
		if (this._win.isMaximized()) {
			this.fire('resized');
		}
	},

	/**
	 * @method _normalizeArgs
	 * @access private
	 * @memberof butor.dlg.Dialog#
	 * @description Normilizes the dialog arguments.
	 */
	_normalizeArgs: function() {
		if (this._args) {
			if (this._args.callBack) {
				this._args.callback = this._args.callBack;
			}
		}
	},

	/**
	 * @method show
	 * @memberof butor.dlg.Dialog#
	 * @description Shows the dialog.
	 * @param {Map} args - Arguments which configure the dialog box when it is displayed.
	 *
	 * JavaScript map used to configure the configure the dialog when it will be shown. this structure is used in a lot of place of the framework and it can be extended on your own way.
	 *
	 * @param {Function} args.callBack - Function which returns the configurated callback object.
	 * @param {jQueryElement} args.target - jQuery element targeted by the object.
	 * @param {String} args.mode - Current mode of the linked object.
	 * @param {String} args.id - ID of the linked object.
	 */
	show: function(args) {
		if (this._win) {
			this._win.show();
			return;
		}
		if (this.getUrl()) {
			this._loadUrl(this.getUrl(), args);
		} else if (this.getHtml()) {
			this._loadHtml(this.getHtml(), args);
		} else {
			this._loadHtml(
				'<span class="alert alert-error">No URL or Html was set for this dialog!</span>',
				args);
		}
	},

	/**
	 * @method _loadHtml
	 * @access private
	 * @memberof butor.dlg.Dialog#
	 * @description Loads the HTML contained in the dialog.
	 */
	_loadHtml: function(html, args) {
		this._args = args;
		this._normalizeArgs();
		if (this._win == null) {
			this._showDlg(html);
		}
	},

	/**
	 * @method _loadUrl
	 * @access private
	 * @memberof butor.dlg.Dialog#
	 * @description Loads the URL of the dialog.
	 */
	_loadUrl: function(url, args) {
		this._args = args;
		this._normalizeArgs();
		if (url.indexOf('?') === -1) {
			url += '?';
		} else {
			url += '&';
		}
		url += '?t=' + new Date().getTime();
		var anchor = $('<div>');
		var dlg = this;
		anchor.load(url, function() {
			if (dlg._win == null) {
				dlg._showDlg(anchor.html());
			}
		});
	},

	/**
	 * @method tr
	 * @memberof butor.dlg.Dialog#
	 * @description Translates a text.
	 * @param {String} key - The key of the text to translate.
	 * @see butor.App#tr
	 */
	tr: function(key) {
		return App.tr(key, this._dlgOpts.bundleId);
	}
});

/**
 * Notifies dialog resizing.
 * @event butor.dlg.Dialog#resized
 */

/**
 * Notifies dialog maximizing.
 * @event butor.dlg.Dialog#maximized
 */

/**
 * Notifies dialog minifying.
 * @event butor.dlg.Dialog#minimized
 */

/**
 * Notifies the dialog is cascaded.
 * @event butor.dlg.Dialog#cascaded
 */
