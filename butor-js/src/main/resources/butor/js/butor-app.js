/*
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 /**
	* @class App
	* @abstract
	* @description Base class of all applications made on the platform.
	*
	* The App instanciates the basic componants to make a *butor* application. It provides a
	* {@link butor.panel.MsgPanel MsgPanel}, {@link butor.panel.BottomPanel BottomPanel},
	* some translator functions, a {@link butor.Bundle} and some functions to manage the history and the theme of the application.
	* @memberof butor
	*/
butor.App = butor.Class.define({
	_themeName : null,
	_msgPanel : null,
	_bottomPanel : null,
	_sessionId : null,
	_bundle : null,
	_env : null,
	_options : {},

	construct : function() {
		this._bundle = new butor.Bundle();
	},

	/**
	* @method setOptions
	* @memberof butor.App#
	* @description Extends the options of the App.
	*
	* These options are working options of the application and these can be
	* used anytime in the application environment.
	* @param {Map} options - Options of the application.
	*/
	setOptions : function(options) {
		$.extend(this._options, options);
	},

	/**
	* @method start
	* @memberof butor.App#
	* @description Starts the App.
	* @fires butor.App#historyChanged
	* @fires butor.App#state-changed
	*/
	start : function() {
		this._sessionId = "s-" + butor.Utils.getUUID();
		this._msgPanel = this.createMsgPanel();
		this._bottomPanel = this.createBottomPanel();
		var link = this.createBackToTopLink();
		if (link) {
			$(window).scroll(function() {
				var st = $(this).scrollTop();
				var visible = link.is(":visible");
				if (st > 20) {
					if (!visible) {
						link.fadeIn();
					}
				} else if (visible) {
					link.fadeOut();
				}
			});

			link.click(function() {
				$.scrollTo(0, 1000); //, { easing:'elasout' }
			});
		}

		$(window).bind( 'hashchange', $.proxy(function(e) {
			var state = $.bbq.getState();
			this.fire('historyChanged', state);
			this.fire('state-changed', state);
		},this));
	},

	/**
	* @method addBundle
	* @memberof butor.App#
	* @description Sets a specific bundle for the App.
	* @param {string} id - Bundle ID.
	* @param {butor.Bundle} bundle - Bundle to add.
	*/
	addBundle : function(id, bundle) {
		this._bundle.add(id, bundle);
	},

	/**
	* @method getLang
	* @memberof butor.App#
	* @description Gets the **current** language of the application.
	* @returns {string} The **current** language for the application.
	*/
	getLang : function() {
		return this._bundle.getLang();
	},

	/**
	* @method pushHistory
	* @memberof butor.App#
	* @description Adds a browser history element about the working page.
	* @param {Map} data - Working page information.
	* @param {string} title - Title of the working page.
	* @param {string} url - URL of the working page.
	*/
	pushHistory : function(data, title, url) {
		$.bbq.pushState(data, title, url);
	},

	/**
	* @method changeState
	* @memberof butor.App#
	* @description Changes the information of the current working page.
	* @see butor.App#pushHistory
	* @param {Map} data - Working page information.
	* @param {string} title - Title of the working page.
	* @param {string} url - URL of the working page.
	*/
	//TODO Check where it is used
	changeState: function(data, title, url) {
		for (var f in data) {
			if (butor.Utils.isEmpty(data[f])) {
				data[f] = '';
			}
		}
		$.bbq.pushState(data, title, url);
	},

	/**
	* @method getState
	* @memberof butor.App#
	* @description Gets the information of the current working page.
	* @see butor.App#pushHistory
	* @return {Map} The working page information.
	*
	*/
	getState : function() {
		return $.bbq.getState();
	},

	/**
	* @method sessionTimedOut
	* @memberof butor.App#
	* @description Notifies a HTTP session time-out.
	*/
	sessionTimedOut : function () {
		alert(this.tr("SESSION_TIMEDOUT"));
	},

	/**
	* @method getEnv
	* @memberof butor.App#
	* @description Gets the current environment.
	*
	* The built applications can be launched in different kind of
	* environments characterized by their name as
	* *TEST*, *DEV* or *PROD* for examples.
	* @return {string} Environment name.
	*/
	getEnv : function() {
		return this._env;
	},

	/**
	* @method themeLoaded
	* @memberof butor.App#
	* @description Notifies that the App theme is loaded.
	* @fires butor.App#theme-loaded
	*/
	themeLoaded : function(themeName) {
		this._themeName = themeName;
		this.fire('theme-loaded', themeName);
	},

	/**
	* @method getThemeName
	* @memberof butor.App#
	* @description Gets the current theme name.
	* @return {string} Theme name.
	*/
	getThemeName : function() {
		return this._themeName;
	},

	/**
	* @method createMsgPanel
	* @memberof butor.App#
	* @description Creates the message panel.
	*
	* The message panel is designed to display important message
	* to the user as the error message by example.
	* The class {@link butor.panel.MsgPanel MsgPanel} provides
	* this default behaviour but it may be
	* overridden by another behaviour defined for some specific usages
	* in a MsgPanel extending.
	* @return {butor.panel.MsgPanel} A new message panel.
	* @see butor.panel.MsgPanel
	*/
	createMsgPanel: function() {
		var panel = new butor.panel.MsgPanel($('body'), '50%', '41px', '25%');
		panel.setPosition('fixed');
		panel.hideMsg();
		return panel;
	},

	/**
	* @method createBottomPanel
	* @memberof butor.App#
	* @description Creates the bottom panel.
	*
	* The bottom panel is designed to display a progress bar
	* in the bottom of the page.
	* The class {@link butor.panel.Bottom BottomPanel} provides
	* this default behaviour but it may be
	* overridden by another behaviour defined for some specific usages
	* in a BottomPanel extending.
	* @return {butor.panel.BottomPanel} A new bottom panel.
	* @see butor.panel.BottomPanel
	*/
	createBottomPanel: function() {
		var panel = new butor.panel.BottomPanel('200px', '200px');
		panel.hide();
		return panel;
	},

	/**
	* @method createBackToTopLink
	* @memberof butor.App#
	* @description Creates the back to top link.
	*
	* The **back-to-top** link appears when
	* the user scrolls the current working page in order to make the
	* navigation easier when a large set of data is displayed
	* on one page for instance.
	* @return {JQuerySelector} A back to top link.
	*/
	createBackToTopLink: function() {
		var link = $('<div class="backToTopLink label label-info"><i class="icon-chevron-up"></i><span class="bundle">top</span></div>');
		link.hide().appendTo($('body'));
		return link;
	},

	/**
	* @method mask
	* @memberof butor.App#
	* @description Masks the App with a message.
	* @param {string} msg - Message.
	*/
	mask: function(msg) {
		$('body').mask(msg);
	},

	/**
	* @method unmask
	* @memberof butor.App#
	* @description Unmasks the App.
	*/
	unmask: function() {
		$('body').unmask();
	},

	/**
	* @method getId
	* @memberof butor.App#
	* @return {string} The application ID.
	* @description Gets the application ID.
	*
	* Each application has an unique ID, this method needs to be overridden
	* by developer for his specific application.
	*/
	getId: function() {
		// TODO override
		return null;
	},

	/**
	* @method getSessionId
	* @abstract
	* @memberof butor.App#
	* @description Gets the session ID.
	* @return {string} Session ID.
	*/
	getSessionId: function() {
		return this._sessionId;
	},

	/**
	* @method setSessionId
	* @memberof butor.App#
	* @description Sets the session ID.
	* @param {string} sessionId - Session ID.
	*/
	setSessionId: function(sessionId) {
		this._sessionId = sessionId;
	},

	/**
	* @method scrollTo
	* @memberof butor.App#
	* @description Scrolls to the specified ID.
	*
	* Scrolls the current working page to an HTML element
	* which is equal to the specified ID.
	* @param {string} id - ID.
	*/
	scrollTo: function(id) {
		$.scrollTo($("#" + id), 1500);
	},

	/**
	* @method showStatus
	* @memberof butor.App#
	* @description Shows a message in the bottom panel.
	* @param {string} msg - Status.
	*/
	showStatus: function(msg) {
		if (this._bottomPanel) {
			this._bottomPanel.showMsg(msg);
		}
	},

	/**
	 * @method progress
	 * @memberof butor.App#
	 * @description Updates the progress bar beside the bottom panel.
	 *
	 * This method calculates automatically the good pourcentage of
	 * the progression when it is called
	 * with the current value and maximum value.
	 * The message will be displayed on the progression
	 * in order to indicate the current state of the loading task.
	 * @param {Number} count - Current value.
	 * @param {Number} total - Total value of the progress.
	 * @param {string} msg - Message to display during the progress.
	 * @see butor.panel.BottomPanel#progress
	 * @see butor.panel.ProgressPanel#progress
	 */
	progress: function(count, total, msg) {
		if (this._bottomPanel) {
			this._bottomPanel.progress(count, total, msg);
		}
	},

	/**
	* @method removeStatus
	* @memberof butor.App#
	* @description Removes the current status of the application by removing it
	* of the message panel then by hidden the message panel.
	*/
	removeStatus: function() {
		if (this._bottomPanel) {
			this._bottomPanel.showMsg('');
			this._bottomPanel.hide();
		}
	},

	/**
	* @method getMsgPanel
	* @memberof butor.App#
	* @description Gets the message panel.
	* @return {butor.panel.MsgPanel} The attached Message Panel.
	*/
	getMsgPanel : function() {
		return this._msgPanel;
	},

	/**
	* @method getBottomPanel
	* @memberof butor.App#
	* @description Gets the bottom panel.
	* @return {butor.panel.BottomPanel} The attached Bottom Panel.
	*/
	getBottomPanel: function() {
		return this._bottomPanel;
	},

	/**
	* @method showMsg
	* @memberof butor.App#
	* @description Shows a specific message in the bottom panel.
	* @see butor.panel.MsgPanel#showMsg
	* @param {butor.App#MsgDescription} data - Data to display.
	* @example
	* // The data needs to contain specific fields
	*
	*	var data = {
	*		'msg': 'This is a message',  //the message to display in MsgPanel
	*		'isError': false, //if this message is an error
	* 		'isWarning': true,	 //if the message is a warning
	*		//else, the message is an info or a custom level
	*		//secific levels can be used for specific usages.
	*	 }
	*/
	showMsg: function(data) {
		if (this._msgPanel) {
			this._msgPanel.showMsg(data);
		}
	},

	/**
	* @method hideMsg
	* @memberof butor.App#
	* @description Hides the message panel.
	*/
	hideMsg: function() {
		if (this._msgPanel) {
			this._msgPanel.hideMsg();
		}
	},

	/**
	* @method error
	* @memberof butor.App#
	* @description Writes an App error in the logging output.
	* @param {string|string[]|Error} msg - Message to display as an error.
	* @see butor.panel.MsgPanel#error
	*/
	error: function() {
		if (this._msgPanel) {
			this._msgPanel.error(arguments);
		}
	},

	/**
	* @method info
	* @memberof butor.App#
	* @description Writes an App info in the logging output.
	* @param {string|string[]} msg - Message to display as information.
	* @see butor.panel.MsgPanel#info
	*/
	info : function() {
		if (this._msgPanel) {
			this._msgPanel.info(arguments);
		}
	},

	/**
	* @method warning
	* @memberof butor.App#
	* @description Writes an App warning in the logging output.
	* @param {string|string[]} msg - Message to display as a warning.
	* @see butor.panel.MsgPanel#warning
	*/
	warning : function() {
		if (this._msgPanel) {
			this._msgPanel.warning(arguments);
		}
	},

	/**
	* @method setLang
	* @memberof butor.App#
	* @description Sets the language of the App at the starting.
	* @param {string} lang - Language of the App.
	*/
	setLang : function(lang) {
		LOGGER.info('Setting language to ' + lang + ' ...');
		$("#switchLangLink").html(
				'<i class="icon-flag"></i> '
				+ (lang === 'fr' ? 'English' : 'Français'));

		this._bundle.setLang(lang);
	},

	/**
	* @method switchLang
	* @memberof butor.App#
	* @description Switches the language of the App during the current session.
	* @param {string} lang - Language of the App.
  * @fires butor.App#langChanged
	*/
	switchLang : function(lang) {
		if (butor.Utils.isEmpty(lang)) {
			lang = this.getLang() === 'fr' ? 'en' : 'fr';
		}
		LOGGER.info('Swiching language to ' + lang + ' ...');
		this.setLang(lang);
		this.translate();
		this.fire('langChanged', lang);
	},

	/**
	* @method translate
	* @memberof butor.App#
	* @description Translates the elements of the App to the current language.
	*/
	translate : function() {
		this.translateElem($(document), this.getId());
	},

	/**
	* @method translateElem
	* @memberof butor.App#
	* @description Translate the page (specified by the jQuerySelector)
	* and all its children with the specified bundle.
	* @param {jQuerySelector} jqElem - jQuery Selector of the element to translate.
	* @param {string} bundleId - ID of the bundle.
	* @see butor.Bundle
	*/
	translateElem : function(jqElem, bundleId) {
		var self = this;
		jqElem.find(".bundle").each(function(index_, elem_) {
			var je = $(this);
			var sys = je.attr('sys') || bundleId || '';
			var key = je.prop('key');
			if (butor.Utils.isEmpty(key)) {// first time. keep the key
				key = je.text();
				je.prop('key', sys +';' +key);
			} else {
				var pos = key.indexOf(";");
				if (pos > -1) {
					sys = key.substring(0, pos);
					key = key.substring(pos +1);
				}
			}
			je.html(self._bundle.get(key, sys));
		});
	},

	/**
	 * @method tr
	 * @memberof butor.App#
	 * @description Translate a text.
	 * @param {string} txt - Text to translate.
	 * @param {string} bundleId - ID of the bundle.
	 */
	tr: function(txt, bundleId) {
		return this._bundle.get(txt, bundleId);
	}
});

/**
 * @description History changed event
 *
 * This event gives takes the return value of `$.bbq.getState(...)` function.
 * @event butor.App#historyChanged
 * @param {jQueryElement} state - jQuery bbq state.
 * @see {@link http://benalman.com/code/projects/jquery-bbq/docs/files/jquery-ba-bbq-js.html#jQuery.bbq.getState jQuery.bbq.getState documentation}
 */

/**
 * @description State changed event
 *
 * This event gives takes the return value of `$.bbq.getState(...)` function.
 * @event butor.App#state-changed
 * @param {jQueryElement} state - jQuery bbq state.
 * @see {@link http://benalman.com/code/projects/jquery-bbq/docs/files/jquery-ba-bbq-js.html#jQuery.bbq.getState  jQuery.bbq.getState documentation}
 */

/**
 * @description Theme loaded event
 * @event butor.App#theme-loaded
 * @param {string} themeName - Name of the loaded theme.
 */

/**
 * @description Language changed event
 * @event butor.App#langChanged
 * @param {string} langName - Name of the language.
 */


//# sourceURL=butor.app.js
