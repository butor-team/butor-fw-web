/*
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
butor = butor || {};

/**
 * @namespace butor.panel
 */
butor.panel = butor.panel || {};

/**
 * @class MsgPanel
 * @memberof butor.panel
 * @description Displays messages in the application UI (by default: **on the top**).
 *
 * The msgPanel open a panel on the top of its parent.
 * It is widely used in the butor framework to display info, warning and error message dedicated to the user.
 * Some properties of the panel can be edited before showing it :
 * {@link butor.panel.MsgPanel#setPosition position},
 * {@link butor.panel.MsgPanel#setDelay delay} and
 * {@link butor.panel.MsgPanel#setErrorDelay errorDelay}.
 *
 * @param {Object} parent - Parent panel of this one.
 * @param {Number} width - Width of the panel.
 * @param {Number} top - Top position of the panel.
 * @param {Number} bundleID - ID of the bundle linked with the panel.
 *
 * @example
 * var panel = new butor.panel.MsgPanel($('body'), '400px', '100px');
 */
butor.panel.MsgPanel = butor.panel.MsgPanel || butor.Class.define({
	_parent: null,
	_autoHideTimer: null,
	_appId: null,
	_panel: null,
	_close: null,
	_sign: null,
	_strong: null,
	_msg: null,
	_errorDelay: 12000,
	_delay: 9000,
	construct: function(parent, width, top, appId) {
		this.base();
		this._parent = parent || $('body');
		this._appId = appId;
		this._panel = $(
			'<div class="alert hidden msgPanel" style="margin:2px;max-width:' +
			width + ';position:fixed;top:' + top + ';min-height:22px;">');
		this._close = $(
			'<i class="icon-remove fa fa-close" style="margin:3px;margin-left:10px;">'
		);
		this._close.css("float", "right").css("cursor", "pointer")
			.appendTo(this._panel);
		this._sign = $(
			'<i class="icon-info-sign fa fa-info" style="margin-left:5px;margin-right:5px;">'
		);
		this._sign.appendTo(this._panel);
		this._panel.append(" ");
		this._strong = $('<strong>');
		this._strong.appendTo(this._panel);
		this._msg = $('<span>');
		this._msg.appendTo(this._panel);
		this._parent.append(this._panel);
		this._close.click($.proxy(function() {
			this.hide();
		}, this));
		this.hide();
		/*
		this._panel.hover(function(e) {
				$(this).addClass('semi-transparent');
			},
			function(e) {
				$(this).removeClass('semi-transparent');
			});
		*/
	},

	/**
	 * @method show
	 * @memberof butor.panel.MsgPanel#
	 * @description Shows the message.
	 * @access private
	 * @param {string|string[]|Map|Error} data - Data to display.
	 * @param {(string|string[])} data.msg - Message to display.
	 * @param {Boolean} [data.isError] - Flag which notifies an error.
	 * @param {Boolean} [data.isWarning] - Flag which notifies a warning.
	 *
	 * @example
	 * var simple_msg = {'msg': 'Hello world!'};
	 * var warning_msg = {'msg': 'This is a warning!', 'isWarning': true};
	 * var error_msg = {'msg': 'This is an error!', 'isError': true};
	 * panel.show(simple_msg);
	 * panel.show(warning_msg);
	 * panel.show(error_msg);
	 */
	show: function(data) {
		if (!data)
			return;

		if (!data['msg'])
			data = this._prepData(data);
		if (!data['msg'])
			return;

		if (this._autoHideTimer) {
			clearTimeout(this._autoHideTimer);
			this._autoHideTimer = null;
		}

		this._sign.removeAttr('class');
		if (data['isError']) {
			this._sign.addClass('icon-bolt fa fa-bolt');
			this._panel.removeClass("alert-info alert-warning");
			this._panel.addClass("alert-error alert-danger");
		} else if (data['isWarning']) {
			this._sign.addClass('icon-warning-sign fa fa-warning');
			this._panel.removeClass("alert-info alert-error alert-danger");
			this._panel.addClass("alert-warning");
		} else {
			this._sign.addClass('icon-info-sign fa fa-info');
			this._panel.removeClass("alert-error alert-danger alert-warning");
			this._panel.addClass("alert-info");
		}

		this._msg.html(butor.Utils.escapeStr(data['msg']).replace(/\n/g, '<br>'));
		this._panel.removeClass("hidden").css('height', '').stop().slideDown(
			"fast");
		var left = (this._parent.width() / 2) - (this._panel.outerWidth() / 2);
		this._panel.css('left', left + "px");
		if (data.sticky)
			return;

		var delay = this._delay;
		if (data['isError']) {
			delay = this._errorDelay;
		}
		this._autoHideTimer = setTimeout($.proxy(this.hide, this), delay);
	},

	/**
	 * @method setPosition
	 * @memberof butor.panel.MsgPanel#
	 * @description Sets the position of the MsgPanel.
	 * @param {Number} position - Position of the panel.
	 *
	 * @example
	 * Position: <select id='selectID' onchange='changeSelect()'></select>
	 *
	 * @example
	 * var opts = {
	 * 	showEmpty: false,
	 * 	idFN: 'position',
	 * 	descFN: 'position',
	 * 	sorted: true
	 * };
	 *
	 * var cssPositions = [
	 * 	{position: 'static'},
	 * 	{position: 'absolute'},
	 * 	{position: 'fixed'},
	 * 	{position: 'relative'},
	 * 	{position: 'initial'},
	 * 	{position: 'inherit'},
	 * ];
	 *
	 * butor.Utils.fillSelect($('#selectID'), cssPositions, opts);
	 *
	 * var changeSelect = function() {
	 * 	var id = $('#selectID option:selected').val();
	 * 	panel.setPosition(id);
	 * }
	 */
	setPosition: function(position) {
		this._panel.css('position', position);
	},

	/**
	 * @method setDelay
	 * @memberof butor.panel.MsgPanel#
	 * @description Sets the delay of display for simple messages.
	 *
	 * **Warning**: For error messages use {@link butor.panel.MsgPanel#setErrorDelay setErrorDelay}.
	 * @param {Number} delay - Duration between the message appearance and when it is hidden (in seconds).
	 *
	 * @example
	 * Delay: <input id="delayInput" type="number"value='0'/>
	 * <button id='delayButton' type='button' value='Set a delay' onclick='onSetDelay();'>Set a delay</button>
	 * <br/>
	 *
	 * @example
	 * var onSetDelay = function() {
	 * 	panel.setDelay(document.getElementById('delayInput').value);
	 * }
	 */
	setDelay: function(delay) {
		this._delay = delay;
	},

	/**
	 * @method setErrorDelay
	 * @memberof butor.panel.MsgPanel#
	 * @description Sets the delay of display for error messages.
	 * @param {Number} delay - Duration between the message appearance and when it is hidden (in seconds).
	 */
	setErrorDelay: function(errorDelay) {
		this._errorDelay = errorDelay;
	},

	/**
	 * @method hideMsg
	 * @memberof butor.panel.MsgPanel#
	 * @description Hides the displayed message.
	 */
	hideMsg: function() {
		this.hide();
	},

	/**
	 * @method showMsg
	 * @memberof butor.panel.MsgPanel#
	 * @description Shows the message.
	 * @param {object|Error} data - Data to display.
	 * @param {string|string[]} data.msg - Message to display.
	 * @param {Boolean} [data.isError] - Flag which notifies an error.
	 * @param {Boolean} [data.isWarning] - Flag which notifies a warning.
	 */
	showMsg: function(data) {
		this.show(data);
	},

	/**
	 * @method hide
	 * @memberof butor.panel.MsgPanel#
	 * @description Hides the message.
	 */
	hide: function() {
		if (this._panel.hasClass("hidden")) {
			return;
		}
		this._panel.stop().slideUp("fast", function() {
			$(this).addClass("hidden");
		});
		clearTimeout(this._autoHideTimer);
		this._autoHideTimer = null;
	},

	/**
	 * @method _prepData
	 * @access private
	 * @memberof butor.panel.MsgPanel#
	 * @description Prepares data to be displayed in the MsgPanel.
	 * @param {Map|string|string[]|Error} data - Data displayed in the panel.
	 */
	_prepData: function(data) {
		if (!data)
			return;

		if (typeof(data) === "string") {
			data = {
				'msg': data
			};

		} else if (data instanceof Error) {
			data = {
				'msg': data.stack,
				'isError': true
			};

		} else if (data.length !== null) {
			var msg = '';
			for (var i = 0, c = data.length; i < c; i += 1) {
				msg += ' ' + data[i];
			}
			data = {
				'msg': msg
			};

		} else {
			data = {
				'msg': data + ""
			};
		}

		return data;
	},

	/**
	 * @method info
	 * @memberof butor.panel.MsgPanel#
	 * @description Displays data in the MsgPanel as an information.
	 * @param {string|string[]} data - Data to display as an information.
	 *
	 * @example
	 * Info: <input id='infoInput' type="text" value="This is an info!"/><br/>
	 * <button id='infoButton' type='button' value='Send an info' onclick='onSendInfo();'>Send an info</button>
	 *
	 * @example
	 * var onSendInfo = function() {
	 * 	panel.info(document.getElementById('infoInput').value);
	 * }
	 */
	info: function(data) {
		data = this._prepData(data);
		data['isError'] = false;
		this.show(data);
	},

	/**
	 * @method warning
	 * @memberof butor.panel.MsgPanel#
	 * @description Displays data in the MsgPanel as a warning.
	 * @param {string|string[]} data - Data to display as a warning.
	 *
	 * @example
	 * Warning: <input id='warningInput' type="text" value="This is an warning!"/><br/>
	 * <button id='warningButton' type='button' value='Send an warning' onclick='onSendWarning();'>Send an warning</button>
	 *
	 * @example
	 * var onSendWarning = function() {
	 * 	panel.warning(document.getElementById('warningInput').value);
	 * }
	 */
	warning: function(data) {
		data = this._prepData(data);
		data['isWarning'] = true;
		this.show(data);
	},

	/**
	 * @method error
	 * @memberof butor.panel.MsgPanel#
	 * @description Displays data in the MsgPanel as an error.
	 * @param {string|string[]|Error} data - Message to display as an error.
	 *
	 * @example
	 * Error: <input id='errorInput' type="text" value="This is an error!"/><br/>
	 * <button id='errorButton' type='button' value='Send an error' onclick='onSendError();'>Send an error</button>
	 *
	 * @example
	 * var onSendError = function() {
	 * 	panel.error(document.getElementById('errorInput').value);
	 * }
	 */
	error: function(data) {
		data = this._prepData(data);
		data['isError'] = true;
		this.show(data);
	},

	/**
	 * @method tr
	 * @memberof butor.panel.MsgPanel#
	 * @description Translates a key/value contained in the panel.
	 * @param {String} key - Key of the value to translate.
	 * @return {String} The translated value.
	 */
	tr: function(key) {
		return App.Bundle.get(key, this._appId);
	},

	/**
	 * @method getPanel
	 * @memberof butor.panel.MsgPanel#
	 * @description Gets the panel.
	 * @return {jQueryElement} The panel structure as a jQuery element.
	 */
	getPanel: function() {
		return this._panel;
	},
});


/**
 * @class BottomPanel
 * @memberof butor.panel
 * @description Manager of a bottom panel.
 *
 * Creates a panel in the bottom for the specified container.
 * It can display many messages for the user, but he needs to remove them manually by default.
 * In addition, it can be integrally hidden by clicking on *arrow* button near the panel.
 * Only the size position of the panel is configurable.
 *
 * @param {Number} left - Left position of the panel.
 * @param {Number} right - Right position of the panel.
 * @example
 * .panelElement * {
 * 	display: inline-block;
 * }
 *
 * .panelElement button {
 * 	position: absolute;
 * 	right: 4px;
 * }
 *
 * @example
 * Message: <input id='addMsgInput' type="text" value="This is an message!"/>
 * <button id='addButton' type='button' value='Add a message' onclick='onAddMessage();'>Add a message</button>
 *
 * @example
 * var onAddMessage = function() {
 * 	var msg = document.getElementById('addMsgInput').value;
 * 	panel.createPanelElement(msg);
 * }
 *
 * butor.example.BottomPanel = butor.example.BottomPanel || butor.panel.BottomPanel.define({
 * 	_msg_panel_html: null,
 * 	_example_panel: "<button type='button' onclick='panel.removeElementEvent(this);'>Remove</button>",
 * 	_elt_class_name: 'panelElement',
 * 	_length: 0,
 *
 * 	construct: function(options) {
 * 		this.base(options.left, options.right);
 * 		this._elt_class_name = this._elt_class_name || options.eltClassName;
 * 	},
 *
 * 	createPanelElement: function(msg) {
 * 		var jqe = $(document.createElement('div'));
 * 		jqe.addClass(this._elt_class_name);
 * 		jqe.append('<p>' + msg + '</p>');
 *
 * 		jqe.append(this._example_panel);
 * 		this.add(jqe.clone());
 * 		this._length++;
 * 	},
 *
 * 	removeElementEvent: function(e) {
 * 		var msg = $(e).parent();
 * 		console.info(msg.attr('class'));
 * 		this
 * 		this.remove($(e).parent());
 * 		this._length--;
 *
 * 		if (this._length == 0) {
 * 			this.hide();
 * 		}
 * 	}
 * });
 *
 * divTest.innerHTML = control_panel_html;
 * body.appendChild(divTest);
 *
 * var panelOptions = {
 * 	'left': '100px',
 * 	'right': '100px',
 * }
 *
 * var panel = new butor.example.BottomPanel(panelOptions);
 */
butor.panel.BottomPanel = butor.panel.BottomPanel || butor.Class.define({
	_left: null,
	_right: null,
	_visible: false,
	_panel: null,
	_msg: null,
	_prgBar: null,
	_handle: null,
	_icon: null,
	_body: null,
	construct: function(left, right) {
		//this.base();
		left = left || '200px';
		right = right || '200px';

		this._left = left;
		this._right = right;

		this._panel = $('<div class="bottom-panel"></div>')
			.css('left', this._left).css('right', this._right).appendTo($('body'));
		this._body = $(
			'<div class="bottom-panel-body well well-sm" style="display:none;"></div>'
		).appendTo(this._panel);

		this._msg = $('<div class=""></div>');
		this._msg.css('padding', '2px').appendTo(this._body).hide();

		this._prgBar = $('<div>');
		this._prgBar.appendTo(this._body).hide();

		// show / hide handle
		this._handle = $('<div class="label label-default">')
			.css('position', 'absolute').css('height', '20px').css(
				'right', '-25px').css('bottom', '0px').css(
				'cursor', 'pointer').appendTo(this._panel);

		this._icon = $('<i>');
		this._icon.addClass('fa fa-chevron-up fa fa-white')
			.appendTo(this._handle);

		this._handle.click($.proxy(function() {
			this._visible ? this.hide() : this.show();
		}, this));

		this.hide();
	},

	/**
	 * @method display
	 * @access private
	 * @memberof butor.panel.BottomPanel#
	 * @description Displays the BottomPanel.
	 */
	_display: function() {
		var self = this;
		if (this._visible) {
			// show
			this._body.slideDown(function() {
				// then show or stay hidden
				//self._panel.css('height', self._visible ? '' : '4px').slideDown(
				//		function() {
				//			//self._handle.css('height', self._visible ? '30px' : '20px');
				//		});
			});
		} else {
			// hide
			this._body.slideUp();
		}
		this._handle.slideUp(function() {
			self._handle.css('height', self._visible ? '22px' : '20px');
			self._icon.removeClass('fa-chevron-up fa-chevron-down').addClass(
				self._visible ? 'fa fa-chevron-down' : 'fa fa-chevron-up');
			self._handle.slideDown();
		});
	},

	/**
	 * @method show
	 * @memberof butor.panel.BottomPanel#
	 * @description Shows the BottomPanel.
	 */
	show: function() {
		if (this._visible)
			return;
		this._visible = true;
		this._display();
	},

	/**
	 * @method hide
	 * @memberof butor.panel.BottomPanel#
	 * @description Hide the BottomPanel.
	 */
	hide: function() {
		if (!this._visible)
			return;
		this._visible = false;
		this._display();
	},

	/**
	 * @method toggle
	 * @memberof butor.panel.BottomPanel#
	 * @description Toggles the BottomPanel.
	 *
	 */
	toggle: function() {
		this._visible = !visible;
		this._display();
	},

	/**
	 * @method showMsg
	 * @memberof butor.panel.BottomPanel#
	 * @description Shows the message of the BottomPanel.
	 * @param {String} msg - Message to display.
	 */
	showMsg: function(msg) {
		this._prgBar.hide();
		this._msg.html(butor.Utils.escapeStr(msg)).show();
		this.show();
	},

	/**
	 * @method hideMsg
	 * @memberof butor.panel.BottomPanel#
	 * @description Hides the message of the BottomPanel.
	 */
	hideMsg: function() {
		this._msg.html('');
		this.hide();
	},

	/**
	 * @method progress
	 * @memberof butor.panel.BottomPanel#
	 * @description Updates the progress bar beside the bottom panel.
	 *
	 * *This method calculates automatically the good pourcentage of
	 * the progression when it is called
	 * with the current value and total value.
	 * A message can be displayed on the progression
	 * in order to indicate the current state of the loading task.*
	 * @param {Number} count - Current count of the progress.
	 * @param {Number} total - Total value of the progress.
	 * @param {String} [message='Loading'] - Message to display during the progress.
	 */
	progress: function(count, total, msg) {
		this._prgBar.show();
		this.show();
		var prcnt = parseInt((count / total) * 100);
		this._prgBar.progressbar({
			value: prcnt
		});

		if (!msg)
			msg = App.tr('Loading');
		this._msg.html(butor.Utils.escapeStr(msg));
		this._msg.show();
	},

	/**
	 * @method add
	 * @memberof butor.panel.BottomPanel#
	 * @description Adds an element in the panel.
	 * @param {jQueryElement} element - Element to add.
	 *
	 * @example
	 * var hello = $('<p>Hello World!</p>');
	 * panel.add(hello);
	 */
	add: function(elm) {
		elm.css('padding', '2px').appendTo(this._body);
		this.show();
	},

	/**
	 * @method remove
	 * @memberof butor.panel.BottomPanel#
	 * @description Removes an element of the panel.
	 * @param {jQueryElement} element - Element to remove.
	 *
	 * @example
	 * var hello = $('<p>Hello World!</p>');
	 * panel.add(hello);
	 * panel.remove(hello);
	 */
	remove: function(elm) {
		elm.remove();
	},

	/**
	 * @method find
	 * @memberof butor.panel.BottomPanel#
	 * @description Finds an element of the panel.
	 * @param {string} selector - String used as jQuery selector in the panel.
	 * @return {jQueryElement} elem - Found element.
	 */
	find: function(selector) {
		return this._body.find(selector);
	}
});

/**
 * @class ProgressPanel
 * @memberof butor.panel
 * @description Panel which displays a progress bar.
 *
 * The progress panel displays a specific componant with a progress bar, it provides the method {@link butor.panel.ProgressPanel#progress progress} which automatically calculates the good pourcentage of the progression. It can be set in any container by getting the panel with the method {@link butor.panel.ProgressPanel#getPanel getPanel}.
 *
 * @example
 * <button id='restartButton' type='button' value='Restart' onclick='progress_panel.restart();'>Restart</button>
 *
 * @example
 * butor.example.ProgressPanel = butor.example.ProgressPanel || butor.panel.ProgressPanel.define({
 * 	_progressTimer: null,
 * 	_count: 0,
 * 	_total: 100,
 *
 * 	construct: function(delay) {
 * 		this.base();
 * 		this._self = this;
 * 		this._progressTimer = new butor.Timer($.proxy(this._autoProgress, this), delay, false);
 * 	},
 *
 * 	_autoProgress: function() {
 * 		this._count += 10;
 *
 * 		if (this._count <= this._total) {
 * 			this.progress(this._count, this._total, 'Progress loading...');
 * 		}
 * 		else {
 * 			this.progress(this._total, this._total, 'Progess finished.');
 * 			this._progressTimer.stop();
 * 		}
 * 	},
 *
 * 	start: function() {
 * 		this._progressTimer.start();
 * 	},
 *
 * 	restart: function() {
 * 		this._count = 0;
 * 		this._progressTimer.restart();
 * 	}
 * });
 *
 * var progress_panel = new butor.example.ProgressPanel(100);
 *
 * $('body').append(progress_panel.getPanel());
 * $('body').append(control_panel_html);
 *
 * progress_panel.start();
 */
butor.panel.ProgressPanel = butor.panel.ProgressPanel || butor.Class.define({
	construct: function() {
		//this.base();
		this._panel = $('<div>');
		this._panel.addClass("xlabel xlabel-default").css('position', 'relative');
		this._msg = $('<div class="control-label">');
		this._msg.css('padding', '2px').appendTo(this._panel);
		this._prgBar = $('<div style="height:10px;">');
		this._prgBar.appendTo(this._panel).hide();
	},

	/**
	 * @method getPanel
	 * @memberof butor.panel.ProgressPanel#
	 * @description Gets the panel.
	 * @return {jQueryElement} Panel structure as a jQuery element.
	 */
	getPanel: function() {
		return this._panel;
	},

	/**
	 * @method show
	 * @memberof butor.panel.ProgressPanel#
	 * @description Shows the ProgressPanel.
	 */
	show: function() {
		this._panel.show();
	},

	/**
	 * @method hide
	 * @memberof butor.panel.ProgressPanel#
	 * @description Hides the ProgressPanel.
	 */
	hide: function() {
		this._panel.hide();
	},

	/**
	 * @method showMsg
	 * @memberof butor.panel.ProgressPanel#
	 * @description Shows the message contained in the ProgressPanel.
	 * @param {String} data - Data displayed in the panel.
	 */
	showMsg: function(msg) {
		this._prgBar.hide();
		this._msg.html(butor.Utils.escapeStr(msg)).show();
		this.show();
	},

	/**
	 * @method hideMsg
	 * @memberof butor.panel.ProgressPanel#
	 * @description Hides the message contained in the ProgressPanel.
	 */
	hideMsg: function() {
		this._msg.html('').hide();
	},

	/**
	 * @method progress
	 * @memberof butor.panel.ProgressPanel#
	 * @description Updates the progress bar beside the progress panel.
	 *
	 * This method calculates automatically the good pourcentage of
	 * the progression when it is called
	 * with the current value and maximum value.
	 * The message will be displayed on the progression
	 * in order to indicate the current state of the loading task.
	 * @param {Number} count - Current value of the progress.
	 * @param {Number} total - Total value of the progress.
	 * @param {String} [msg='Loading'] - Message to display during the progress.
	 */
	progress: function(count, total, msg) {
		this._prgBar.show();
		this.show();
		var prcnt = parseInt((count / total) * 100);
		this._prgBar.progressbar({
			value: prcnt
		});

		if (msg) {
			this._msg.html(butor.Utils.escapeStr(msg)).show();
		}
	}
});

//# sourceURL=butor.panels.js
