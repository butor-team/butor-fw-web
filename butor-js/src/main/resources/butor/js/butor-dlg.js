/*
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @namespace butor.dlg
 */
butor.dlg = butor.dlg || {};

/**
 * @typedef {Map} DialogConfigurationArgs
 * @description
 * JavaScript map used to configure the configure the dialog when it will be shown.
 * this structure is used in a lot of place of the framework and
 * it can be extended on your own way.
 *
 * @memberof butor.dlg.Dialog#
 * @property {Function} callBack - Function which returns the configurated callback object.
 * @property {jQueryElement} target - jQuery element targeted by the object.
 * @property {String} mode - Current mode of the linked object.
 * @property {String} id - ID of the linked object.
 * @see butor.dlg.Dialog#show
 */

/**
 * @typedef {Map} DlgOpts
 * @description
 * JavaScript map used to configure the dialog box. All the flags of the structure
 * are optional and they can be changed by using the following setters:
 *
 *  - {@link butor.dlg.Dialog#setResizable `setResizable(resizable)`}
 *  - {@link butor.dlg.Dialog#setMaximizable `setMaximizable(maximizable)`}
 *  - {@link butor.dlg.Dialog#setMinimizable `setMinimizable(minimizable)`}
 *  - {@link butor.dlg.Dialog#setDraggable `setDraggable(draggable)`}
 *  - {@link butor.dlg.Dialog#setModal `setModal(modal)`}
 *
 * @memberof butor.dlg.Dialog#
 * @property {string} title - Name of the dialog.
 * @property {number} width - Width of the dialog.
 * @property {number} height - Heigth of the dialog.
 * @property {boolean} [resizable=false] - Flag to make the dialog resizable.
 * @property {boolean} [maximizable=false] - Flag to make the dialog maximizable.
 * @property {boolean} [minimizable=false] - Flag to make the dialog minimizable.
 * @property {boolean} [draggable=false] - Flag to make the dialog draggable.
 * @property {boolean} [modal=false] - Flag to make the dialog modal.
 * @property {string} bundleId - ID of the bundle linked to the dialog.
 */

/**
 * @class Dialog
 * @abstract
 * @memberof butor.dlg
 * @required {http://fstoke.me/jquery/window/ jQuery Window Plugin}
 * @description Creates a dialog window.
 *
 * This abstract class provides a set of methods to make a dialog box
 * for the platform. The following methods needs to be implemented:
 *
 *  - _init()
 *
 * The method `init()` initialilizes the dialog box and **must call the base Class
 * by calling the method `this.base()` in order to call the MsgPanel interface**.
 *
 * The dialog box can be used and configurated by both methods:
 *
 *  - setUrl(url)
 *  - setHtml(html)
 *
 * These methods takes arguments in order to set up the dialog and you **must**
 * use one of them in order to build the dialog. These methods are automaticaly
 * created as properties of the Dialog class.
 *
 * @param {butor.dlg#dlgOpts} dlgOpts - Options of the dialog, it is used to configure
 * the dialog, see {@link butor.dlg.Dialog#DlgOpts} for more information.
 *
 * @example
 * <div id='exampleDlg'>
 * 	<div class="dlg-tool-bar row">
 * 		<div class="col-sm-12">
 * 			<ul id="tabs" class="nav nav-pills" style="margin-bottom: 2px;">
 * 				<li id="tabFirst" class="active"><a><i class="icon-lock"></i>First</a></li>
 * 				<li id="tabSecond"><a><i class="icon-filter"></i>Second</a></li>
 * 			</ul>
 * 		</div>
 * 	</div>
 * 	<div class="dlg-content">
 * 		<div id="firstPanel">
 * 			lorem_ipsum
 * 		</div>
 * 		<div id="secondPanel" style="display: none">
 * 			Hi, this is a second tab !
 * 			<br/>
 * 			<button class="btn btn-primary" onClick='onDlgMsg(this)'>Send an info</button>
 * 			<button class="btn btn-warning" onClick='onDlgMsg(this)'>Send a warning</button>
 * 			<button class="btn btn-danger" onClick='onDlgMsg(this)'>Send an error</button>
 * 		</div>
 * 	</div>
 * 	<div class="dlg-button-bar align-right">
 * 		<button class="btn btn-default">Cancel</button>
 * 		<button type="button" class="btn btn-default">Ok</button>
 * 	</div>
 * </div>
 *
 * @example
 * //Custom implementation of the abstract class butor.dlg.Dialog
 * butor.ExampleDlg = butor.ExampleDlg || butor.dlg.Dialog.define({
 *   //Constructor
 *   construct: function(html) {
 *     //Define the options of the dialog box
 *     dlgOpts = {
 *       title : "ExampleDlg",
 *       width : 500,
 *       height : 430,
 *       resizable: true,
 *       maximizable: true,
 *       bundleId: 'exampleBundle'
 *     };
 *
 *     //Call the base classe (butor.dlg.Dialog#construct)
 *     this.base(dlgOpts);
 *
 *     //Set the URL (or the HTML) of the Dialog
 *     //setUrl('/example/ExampleDlg.html');
 *     this.setHtml(html);
 *   },
 *   //Implementation of the initializer
 *   _init: function() {
 *     this.base();
 *     //Some stuff...
 *   },
 *   //Custom methods
 * });
 *
 * var exampleDlg = new butor.ExampleDlg($('#exampleDlg'));
 * exampleDlg.show();
 *
 * var onDlgMsgPanelRequest = function(e) {
 * 	if (!example_dlg || !e.className) {
 * 		return;
 * 	}
 *
 * 	switch (e.className.split(' ')[1]) {
 * 		case 'btn-danger':
 * 			example_dlg.msgError('This is an error!');
 * 			break;
 * 		case 'btn-warning':
 * 			example_dlg.msgWarn('This is a warning!');
 * 			break;
 * 		default:
 * 			example_dlg.msgInfo('This is an info.');
 * 	}
 * }
 */

butor.dlg.Dialog = butor.dlg.Dialog || butor.Class.define({
	statics: {
		/**
		 * @var {Object} _registry
		 * @access private
		 * @memberof butor.dlg.Dialog
		 * @description Registry for the dialog.
		 */
		_registry: {},

		/**
		 * @method getDlg
		 * @memberof butor.dlg.Dialog
		 * @description Gets the dialog.
		 * @param {Number} id - The ID of the expected dialog.
		 */
		getDlg: function(id) {
			return butor.dlg.Dialog._registry[id];
		}
	},
	_args: null,
	_jqe: null,
	_anchor: null,
	_dom: null,
	_jqe: null,
	_msgPanel: null,
	properties: {
		url: {
			type: 'String'
		},
		html: {
			type: 'String'
		}
	},
	construct: function(dlgOpts) {
		var doc = $(document);
		if (doc.attr('show.bs.modal.isSet') != '1') {
			doc.attr('show.bs.modal.isSet', '1');
			doc.on('show.bs.modal', '.modal', function(event) {
				var zIndex = 1040 + (10 * $('.modal:visible').length);
				$(this).css('z-index', zIndex);
				setTimeout(function() {
					$('.modal-backdrop')
							.not('.modal-stack').css(
									'z-index', zIndex - 1)
							.addClass('modal-stack');
				}, 0);
			}).on("hidden.bs.modal", function (e) {
				if($('.modal:visible').length) {
					$('.modal-backdrop').first().css('z-index', parseInt($('.modal:visible').last().css('z-index')) - 10);
					$('body').addClass('modal-open');
				}
			});
		}
		
		this._dlgOpts = dlgOpts;
		this._id = butor.Utils.getUUID();
		butor.dlg.Dialog._registry[this._id] = this;
		/*
		 * if (!this._dlgOpts) { throw new Error("Missing options."); } if
		 * (!this._dlgOpts.url && !this._dlgOpts.html) { throw new Error(
		 * "Missing url and html options. Should provide one them."); }
		 */
	},

	/**
	 * @method _init
	 * @access private
	 * @memberof butor.dlg.Dialog#
	 * @description Initializes the dialog.
	 */
	_init: function() {
		// TODO to be overloaded by dialogs impl (subclasses).
		this._createMsgPanel();
	},

	/**
	 * @method _createMsgPanel
	 * @access private
	 * @memberof butor.dlg.Dialog#
	 * @description Creates a default message panel.
	 */
	_createMsgPanel: function() {
		this._msgPanel = new butor.panel.MsgPanel(this._jqe.find(".modal-dialog"),
			'80%', '24px');
		this._msgPanel.hide();

		// keep msg panel visible when pop scroll up and down
		var self = this;
		this._jqe.closest('.modal').scroll(function() {
			var st = $(this).scrollTop();
			
			self._msgPanel._panel && self._msgPanel._panel.css('top', st);
		});
	},

	/**
	 * @method getMsgPanel
	 * @memberof butor.dlg.Dialog#
	 * @description Gets the current MessagePanel of the Dialog.
	 * @returns {butor.panel.MsgPanel} The current message panel.
	 * @see butor.panel.MsgPanel
	 */
	getMsgPanel: function() {
		return this._msgPanel;
	},

	/**
	 * @method msgError
	 * @memberof butor.dlg.Dialog#
	 * @description Displays an error message in the dialog.
	 * @param {String} message - Error message to display.
	 */
	msgError: function(msg) {
		this._msgPanel.error(msg);
	},

	/**
	 * @method msgInfo
	 * @memberof butor.dlg.Dialog#
	 * @description Displays an info message in the dialog.
	 * @param {String} message - Info message to display.
	 */
	msgInfo: function(msg) {
		this._msgPanel.info(msg);
	},

	/**
	 * @method msgWarn
	 * @memberof butor.dlg.Dialog#
	 * @description Displays an warning message in the dialog.
	 * @param {String} message - Warning message to display.
	 */
	msgWarn: function(msg) {
		this._msgPanel.warning(msg);
	},

	/**
	 * @method showMsg
	 * @memberof butor.dlg.Dialog#
	 * @description Shows a message.
	 * @param {String} message - Message to show.
	 */
	showMsg: function(msg) {
		this._msgPanel.showMsg(msg);
	},

	/**
	 * @method hideMsg
	 * @memberof butor.dlg.Dialog#
	 * @description Hides the current MessagePanel of the Dialog.
	 */
	hideMsg: function() {
		this._msgPanel.hide();
	},

	/**
	 * @method getId
	 * @memberof butor.dlg.Dialog#
	 * @description Gets the dialog ID.
	 * @return {Number} The ID of the dialog.
	 */
	getId: function() {
		return this._id;
	},

	/**
	 * @method getWin
	 * @memberof butor.dlg.Dialog#
	 * @description Gets the window of the dialog.
	 * @return {Object} The window of the dialog.
	 */
	getWin: function() {
		return this._jqe;
	},

	/**
	 * @method resize
	 * @memberof butor.dlg.Dialog#
	 * @description Resizes the dialog.
	 * @param {Number} width - Width to resize.
	 * @param {Number} heigth - Heigth to resize.
	 */
	resize: function(w, h) {
//TODO
//		this._jqe && this._jqe.resize(w, h);
//		this._jqe.find('.window_frame').width(w).height(h);
	},

	/**
	 * @method setTitle
	 * @memberof butor.dlg.Dialog#
	 * @description Sets the title of the dialog.
	 * @param {String} title - Title of the dialog.
	 */
	setTitle: function(title) {
		this._dlgOpts.title = title;
		this._jqe.find('.modal-title').html(title);
	},

	/**
	 * @method getTitle
	 * @memberof butor.dlg.Dialog#
	 * @description Get the title of the dialog.
	 */
	getTitle: function() {
		this._jqe.find('.modal-title').html();
	},

	/**
	 * @method maximize
	 * @memberof butor.dlg.Dialog#
	 * @description Maximizes the dialog.
	 */
	maximize: function() {
		//TODO maximize and restore position absolute, top, bottom, etc
		//this.getContainer().css({'width':'100%', 'height':'100%'});
	},

	/**
	 * @method minimize
	 * @memberof butor.dlg.Dialog#
	 * @description Minimizes the dialog.
	 */
	minimize: function() {
		this._jqe.minimize();
	},

	/**
	 * @method restore
	 * @memberof butor.dlg.Dialog#
	 * @description Restores the dialog.
	 */
	restore: function() {
		this._jqe.restore();
	},

	/**
	 * @method hide
	 * @memberof butor.dlg.Dialog#
	 * @description Hides the dialog.
	 */
	hide: function() {
		//TODO this._jqe.modal('hide');
	},

	/**
	 * @method width
	 * @memberof butor.dlg.Dialog#
	 * @description Gets the width of the dialog.
	 * @return {Number} The width of the dialog.
	 */
	width: function() {
		return this.getContainer().width();
	},

	/**
	 * @method height
	 * @memberof butor.dlg.Dialog#
	 * @description Gets the height of the dialog.
	 * @return {Number} The height of the dialog.
	 */
	height: function() {
		return this.getContainer().height();
	},

	/**
	 * @method setResizable
	 * @memberof butor.dlg.Dialog#
	 * @description Decides if the dialog is resizable or not.
	 * @param {Boolean} resizable - Resizable switch.
	 */
	setResizable: function(resizable) {
		this._dlgOpts.resizable = resizable;
		this._jqe && this._jqe.setResizable(resizable);
	},

	/**
	 * @method setDraggable
	 * @memberof butor.dlg.Dialog#
	 * @description Decides if the dialog is draggable or not.
	 * @param {Boolean} draggable - Draggable switch.
	 */
	setDraggable: function(draggable) {
		this._dlgOpts.draggable = draggable;
		this._jqe && this._jqe.setDraggable(draggable);
	},

	/**
	 * @method setMaximizable
	 * @memberof butor.dlg.Dialog#
	 * @description Decides if the dialog is maximizable or not.
	 * @param {Boolean} maximizable - Maximizable switch.
	 */
	setMaximizable: function(maximizable) {
		this._dlgOpts.maximizable = maximizable;
		this._jqe && this._jqe.setMaximizable(maximizable);
	},

	/**
	 * @method setMinimizable
	 * @memberof butor.dlg.Dialog#
	 * @description Decides if the dialog is minimizable or not.
	 * @param {Boolean} minimizable - Minimizable switch.
	 */
	setMinimizable: function(minimizable) {
		this._dlgOpts.minimizable = minimizable;
		this._jqe && this._jqe.setMinimizable(minimizable);
	},

	/**
	 * @method setModal
	 * @memberof butor.dlg.Dialog#
	 * @description Decides if the dialog is modal or not.
	 * @param {Boolean} modal - Modal switch.
	 */
	setModal: function(modal) {
		this._dlgOpts.modal = modal;
		this._jqe && this._jqe.setModal(modal);
	},

	/**
	 * @method getDom
	 * @memberof butor.dlg.Dialog#
	 * @description Gets the DOM linked to the dialog.
	 * @return {JQueryElement} The JQueryElement of the dialog.
	 */
	getDom: function() {
		return this._jqe;
	},

	getContainer: function() {
		return this._jqe.find('.modal-dialog');
	},
	
	/**
	 * @method close
	 * @memberof butor.dlg.Dialog#
	 * @description Closes the dialog.
	 */
	close: function() {
//		if (!this._jqe || this._jqe.attr('_isClosing_') === '1') {
//			return;
//		}
		
		//TODO
		//this._jqe.hide().modal('hide'); //prevent scrollbar glitch - shift to the left
		this._jqe.modal('hide');
		//TODO cleanup
	},

	/**
	 * @method mask
	 * @memberof butor.dlg.Dialog#
	 * @description Masks a message in the dialog.
	 * @param {String} msg - Message to mask.
	 */
	mask: function(msg) {
		this.getContainer().mask(msg);
	},

	/**
	 * @method unmask
	 * @memberof butor.dlg.Dialog#
	 * @description Unmasks the dialog.
	 */
	unmask: function() {
		this.getContainer().unmask();
	},

	/**
	 * @method _showDlg
	 * @access private
	 * @memberof butor.dlg.Dialog#
	 * @description Shows the dialog.
	 * @param {HTMLObject} html - HTML of the dialog.
	 * @fires butor.dlg.Dialog#resized
	 * @fires butor.dlg.Dialog#maximized
	 * @fires butor.dlg.Dialog#minimized
	 * @fires butor.dlg.Dialog#cascaded
	 */
	_showDlg: function(html) {
		var jqe = $('<div>');

		var _setup = $.proxy(function() {
			this._jqe = jqe;
			this._dom = jqe; //backward compatibility. Still reference to _dom
			
			App.translateElem(jqe, this._dlgOpts.bundleId);
	
			this._configDlg();
	
			jqe.modal({
				'backdrop' : 'static',
				'show' : true,
				'keyboard' : true
			});
	//		if (this._dlgOpts.maximized || this.width() > $(window).width() || this.height() >
	//			$(window).height()) {
	//			this.maximize();
	//		}
	//		this._wrp = $.proxy(this._windowResized, this);
	//		$(window).bind('resize', this._wrp);
			
			jqe.on('shown.bs.modal', $.proxy(function(e) {
				this._init();
				this.fire('shown');
				this.fire('resized');
			}, this));
			jqe.on('hidden.bs.modal', $.proxy(function(e) {
				//if (this._jqe.attr('_isClosing_') === '1') {
					delete butor.dlg.Dialog._registry[this._id];
					this._jqe.remove();
					$(window).unbind('resize', this._wrp);
				//}
				//this._jqe.attr('_isClosing_', '1');
				//this.close();
				this.fire('closed');
			}, this));
		}, this);
		
		if ($(html).find('.modal-dialog')[0]) {
			jqe = html.attr('id', this._id).appendTo($('body'));
			_setup();
			
		} else {
			html = $('<div>' +(html.find ? html.html() : html) +'</div>');
			
			jqe.load('/res/butor/butor-dlg.html', $.proxy(function() {
				jqe = jqe.find('#dlg-tmpl').attr('id', this._id).appendTo($('body'));
				
				if (this._dlgOpts['modal-size']) {
					jqe.find('.modal-dialog').addClass(this._dlgOpts['modal-size']);
				} else {
					jqe.find('.modal-dialog').addClass('modal-lg');
				}
				var dtb = html.find('.dlg-tool-bar');
				var dc = html.find('.dlg-content');
				var dbb = html.find('.dlg-button-bar');
				var db = jqe.find('.modal-body').empty();
				if (dc[0]) {
					if (dtb[0]) {
						db.append(dtb.html());
					}
					
					if (!dbb[0]) {
						jqe.find('.modal-footer').hide();
					} else {
						jqe.find('.modal-footer').html(dbb.html());
					}
					
					db.append(dc.html());
				} else {
					db.append(html.html());
				}
				_setup();
			}, this));
		}
	},
	
	setWidth : function(w) {
//TODO
//		if (w != null) {
//			this._jqe.find('.modal-dialog').css('width', this._dlgOpts.width);
//		}
	},
	setHeight : function(h) {
//TODO
//		if (h != null) {
//			h -= this._jqe.find('.modal-header').height() -this._jqe.find('.modal-footer').height();
//			this._jqe.find('.modal-body').css('height', h);
//		}
	},
	_configDlg : function() {
		this.setWidth(this._dlgOpts.width);
		this.setHeight(this._dlgOpts.height);
		this.setTitle(this._dlgOpts.title);
/*
			showRoundCorner: true,
			title: this._dlgOpts.title,
			content: html,
			// footerContent : "HELLO",
			// showFooter: false,
			maxWidth: -1,
			maxHeight: -1,
			width: this._dlgOpts.width,
			height: this._dlgOpts.height,
			// headerClass: '',
			// frameClass: '',
			scrollable: this._dlgOpts.scrollable || false,
			draggable: this._dlgOpts.draggable == null ? true : this._dlgOpts.draggable,
			resizable: this._dlgOpts.resizable == null ? false : this._dlgOpts.resizable,
			maximizable: this._dlgOpts.maximizable == null ? false : this._dlgOpts.maximizable,
			minimizable: this._dlgOpts.minimizable == null ? false : this._dlgOpts.minimizable,
			showModal: typeof this._dlgOpts.modal !== 'undefined' ? this._dlgOpts.modal : true,
			closable: this._dlgOpts.closable == null ? true : this._dlgOpts.closable,
			afterResize: function() {
				dlg.fire('resized');
			},
			afterMaximize: function() {
				dlg.fire('resized');
				dlg.fire('maximized');
				dlg._jqe.resize();
			},
			afterMinimize: function() {
				dlg.fire('resized');
				dlg.fire('minimized');
			},
			afterCascade: function() {
				dlg.fire('resized');
				dlg.fire('cascaded');
			},
			onClose: function(wnd) {
				// TODO delete App.dlg.registry[_id];
				if (dlg._anchor) {
					dlg._anchor.remove();
				}
				this.html = null;
				dlg._anchor = null;
				if (dlg._jqe.attr('_isClosing_') === '1') {
					return;
				}
				dlg._jqe.attr('_isClosing_', '1');
				dlg.fire('closed');
			}
*/
	},

	/**
	 * @method _windowResized
	 * @access private
	 * @memberof butor.dlg.Dialog#
	 * @description Fires a "resized" event for the dialog.
	 */
	 //TODO window plugin do not fire resize event if is it maximized
	_windowResized: function() {
		//TODO window plugin do not fire resize event if is it maximized
		// if browser window is resized
		if (this._isMaximized) {
			this.fire('resized');
		}
	},

	/**
	 * @method _normalizeArgs
	 * @access private
	 * @memberof butor.dlg.Dialog#
	 * @description Normilizes the dialog arguments.
	 */
	_normalizeArgs: function() {
		if (this._args) {
			if (this._args.callBack) {
				this._args.callback = this._args.callBack;
			}
		}
	},

	/**
	 * @method show
	 * @memberof butor.dlg.Dialog#
	 * @description Shows the dialog.
	 * @param {Map} args - Arguments which configure the dialog box when it is displayed. See description about {@link butor.dlg.Dialog#DialogConfigurationArgs DialogConfigurationArgs} for more information.
	 */
	show: function(args) {
		if (this._jqe) {
			this._jqe.show();
			return;
		}
		if (this.getUrl()) {
			this._loadUrl(this.getUrl(), args);
		} else if (this.getHtml()) {
			this._loadHtml(this.getHtml(), args);
		} else {
			this._loadHtml(
				'<span class="alert alert-error">No URL or Html was set for this dialog!</span>',
				args);
		}
	},

	/**
	 * @method _loadHtml
	 * @access private
	 * @memberof butor.dlg.Dialog#
	 * @description Loads the HTML contained in the dialog.
	 */
	_loadHtml: function(html, args) {
		this._args = args;
		this._normalizeArgs();
		if (this._jqe == null) {
			this._showDlg(html);
		}
	},

	/**
	 * @method _loadUrl
	 * @access private
	 * @memberof butor.dlg.Dialog#
	 * @description Loads the URL of the dialog.
	 */
	_loadUrl: function(url, args) {
		this._args = args;
		this._normalizeArgs();
		if (url.indexOf('?') === -1) {
			url += '?';
		} else {
			url += '&';
		}
		url += '?t=' + new Date().getTime();
		var anchor = $('<div>');
		var dlg = this;
		anchor.load(url, function() {
			if (dlg._jqe == null) {
				dlg._showDlg(anchor.html());
			}
		});
	},

	/**
	 * @method tr
	 * @memberof butor.dlg.Dialog#
	 * @description Translates a text.
	 * @param {String} key - The key of the text to translate.
	 * @see butor.App#tr
	 */
	tr: function(key) {
		return App.tr(key, this._dlgOpts.bundleId);
	}
});

/**
 * Notifies dialog resizing.
 * @event butor.dlg.Dialog#resized
 */

/**
 * Notifies dialog maximizing.
 * @event butor.dlg.Dialog#maximized
 */

/**
 * Notifies dialog minifying.
 * @event butor.dlg.Dialog#minimized
 */

/**
 * Notifies the dialog is cascaded.
 * @event butor.dlg.Dialog#cascaded
 */

//# sourceURL=butor.bdocs.dlg-bootstrap.js
