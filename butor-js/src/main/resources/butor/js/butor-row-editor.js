/*
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
//TODO Specification of editors (signatures, examples)?

 /**
	* @class RowEditor
	* @memberof butor
	* @description Editor of table rows.
	* @param {object} args - Arguments of the RowEditor.
	* @param {String[]} args.fields - Editor names/IDs.
	* @param {Function} args.formatter - Function which formats cells.
	* @param {Function} args.editor - Function which edits cells.
	*/
butor.RowEditor = butor.RowEditor || butor.Class.define({
	_row : null,
	_args : null,

	construct : function(args) {
		this.base();
		this._args = args;
	},

	/**
	 * @method blur
	 * @memberof butor.RowEditor#
	 * @description Blurs the selected row.
	 */
	blur : function() {
		this.edit(null);
	},

	/**
	 * @method edit
	 * @memberof butor.RowEditor#
	 * @description Edits the selected row.
	 * @param {Object} selectedTd - Selected row.
	 * @fires butor.RowEditor#data-changed
	 */
	edit : function(selectedTd) {
		if (selectedTd) {
			var row = selectedTd.parent();
			if (selectedTd[0].tagName.toLowerCase() == 'tr') {
				row = selectedTd;
			}
			if (this._row && row && this._row[0] == row[0]) {
				return;
			}
		}
		if (this._row) {
			//remove edit mode
			var data = this._row.data('data');
			if (data) {
				for (var i=0;i<this._args.fields.length;i+=1) {
					var id = this._args.fields[i];
					var jqe = this._row.find('.' +id +' > .editor');
					var editor = jqe.data('editor');
					data[id] = editor.getValue();
					jqe.parent().html(this._args.formatter(id, data[id], data)||'');
				}
				this.fire('data-changed', this._row);
			}
		}
		this._row = row;
		if (!row) {
			return;
		}
		var data = row.data('data');
		if (data) {
			var selectedEditor = null;
			for (var i=0;i<this._args.fields.length;i+=1) {
				var id = this._args.fields[i];
				var td = row.find('.' +id);
				var editor = this._args.editor(id, data);
				var jqe = editor.getJqe();
				jqe.addClass('editor');
				jqe.data('editor', editor);
				td.html(jqe);
				editor.setValue(data[id]);
				if (selectedTd.hasClass(id)) {
					// focus selected cell
					selectedEditor = editor;
				} else if (selectedEditor == null) {
					// focus first editable cell
					selectedEditor = editor;
				}
			}
			if (selectedEditor) {
				selectedEditor.focus();
			}
		}
		this._row = row;
	},

	/**
	 * @method refresh
	 * @memberof butor.RowEditor#
	 * @description Refreshes the selected row.
	 * @param {Object} row - Selected row.
	 */
	refresh : function(row) {
		if (!this._row) {
			return;
		}
		var data = this._row.data('data');
		if (data) {
			for (var i=0;i<this._args.fields.length;i+=1) {
				var id = this._args.fields[i];
				var jqe = this._row.find('.' +id +' > .editor');
				var editor = jqe.data('editor')
				if (editor) {
					editor.setValue(data[id]);
				}
			}
		}
	}
});

/**
 * @event butor.RowEditor#data-changed
 * @description Notifies a data change.
 * @param {object} row - Row object.
 */

/**
 * @class CellEditor
 * @memberof butor
 * @description Editor of table cells.
 * @param {jQuerySelector} jqe - jQuery Selector for the cell to edit.
 * @param {Function} valueHandler - Handler which operates the selected cell.
 */
butor.CellEditor = butor.CellEditor || butor.Class.define({
	_jqe : null,
	_valueHandler : null,
	construct : function(jqe, valueHandler) {
		this.base();
		this._jqe = $(jqe);
		this._valueHandler = valueHandler;
	},

	/**
	 * @method getValue
	 * @memberof butor.CellEditor#
	 * @description Gets the selected value.
	 * @return {Number} Selected cell.
	 */
	getValue : function() {
		return this._valueHandler ? this._valueHandler() : this._jqe.val();
	},

	/**
	 * @method setValue
	 * @memberof butor.CellEditor#
	 * @description Sets the selected value.
	 * @param {Number} value - New value of the selected cell.
	 */
	setValue : function(value) {
		if (this._valueHandler) {
			this._valueHandler(value);
		} else {
			this._jqe.val(value);
		}
	},

	/**
	 * @method getJqe
	 * @memberof butor.CellEditor#
	 * @description Gets the current jQuery selector.
	 * @return {Object} jQuery Selector of the selected cell.
	 */
	getJqe : function() {
		return this._jqe;
	},

	/**
	 * @method focus
	 * @memberof butor.CellEditor#
	 * @description Sets focus on the edited cell.
	 */
	focus : function() {
		this._jqe.focus && this._jqe.focus();
	}
});

/**
 * @class FloatCellEditor
 * @memberof butor
 * @description Editor of table FloatCells (*cells with float content*).
 * @param {jQuerySelector} jqe - jQuery Selector on the FloatCell to edit.
 * @param {Function} valueHandler - Handler which operates the selected cell.
 */
butor.FloatCellEditor = butor.FloatCellEditor || butor.Class.define({
	_jqe : null,
	_valueHandler : null,
	construct : function(jqe, valueHandler) {
		this.base();
		this._jqe = $(jqe);
		this._valueHandler = valueHandler;
	},

	/**
	 * @method getValue
	 * @memberof butor.FloatCellEditor#
	 * @description Gets the selected value.
	 * @return {Float} Selected cell.
	 */
	getValue : function() {
		var value = this._valueHandler ? this._valueHandler() : this._jqe.val();
		if (!butor.Utils.isEmpty(value)) {
			var fv = parseFloat(value);
			if (isNaN(fv)) {
				value = 0.0;
			} else {
				value = fv;
			}
		}
		return value;
	},

	/**
	 * @method setValue
	 * @memberof butor.FloatCellEditor#
	 * @description Sets the selected value.
	 * @param {Number} value - New value of the selected cell.
	 */
	setValue : function(value) {
		if (this._valueHandler) {
			this._valueHandler(value);
		} else {
			this._jqe.val(value);
		}
	},

	/**
	 * @method getJqe
	 * @memberof butor.FloatCellEditor#
	 * @description Gets the current jQuery selector.
	 * @return {jQuerySelector} jQuery selector on the selected cell.
	 */
	getJqe : function() {
		return this._jqe;
	},

	/**
	 * @method focus
	 * @memberof butor.FloatCellEditor#
	 * @description Sets focus on the edited cell.
	 */
	focus : function() {
		this._jqe.focus && this._jqe.focus();
	}
});

//# sourceURL=butor.rowEditor.js
