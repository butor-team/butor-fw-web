/*
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/** @deprecated use butor.Upload */
Upload = function(url_, listener_, scope_) {
	var uid = function() {
		return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
	};

	var __id = uid() +'-' +uid() + "-" + uid() + "-" + uid() + "-" + uid() + "-"
				+ uid() + uid() + uid();
	var __listener = listener_;
	var __scope = scope_;
	var __url = url_;
	var __form = null;
	var __if = document.createElement('iframe');

	document.body.appendChild(__if);
	__if.id = __id;
	__if.width = 0;
	__if.height = 0;
	__if.style.display = 'none';

	// let the iframe initialize before adding the __form ino it!
	setTimeout(function() {
		var form = document.createElement('form');
		form.action = __url;
		form.method = 'post';
		form.enctype = 'multipart/form-data';
		__if.contentDocument.body.appendChild(form);
		// after the post of the form (upload), the result will trigger an load event. this
		// will let us know the the upload is finished.
		// TODO later show progress bars...
		__if.onpost = function() {
			__dispatchNotification({type: "upload-started"});
		};
		__if.onload = function() {
//					id = __if.contentDocument.getElementById('uploadId').value;
			try {
				var msg = null;
				if (__if.contentDocument.body.firstChild) {
					msg = __if.contentDocument.body.firstChild.textContent;
				}
				if (msg && msg.toLowerCase().indexOf('ok') === 0) {
					__dispatchNotification({type: 'upload-succeeded', data: msg});
				} else {
					__dispatchNotification({type: 'upload-failed', data: msg});
				}
				__if.parentNode.removeChild(__if);

			} catch (ex) {
				alert('Upload():' + ex);
			}
		};
		__form = form;
		__dispatchNotification({type: 'upload-ready'});
	}, 500);

	/**
 	* @method __dispatchNotification
	* @access private
	* @memberof butor.Upload#
 	* @description Dispatch the notification to the inner listener.
 	* @param {Object} notif - Notification.
 	*/
	var __dispatchNotification = function(notif_) {
		notif_.uploadId = __id;
		if (__listener)
			__listener.call(__scope, notif_);
	};

	return {
		getId: function() {
			return __id;
		},

		dispose: function() {
			this.clear();
			__if.parentNode.removeChild(__if);
		},

		clear: function() {
			for (var ii = 0; ii < __form.elements.length; ii++) {
				var ff = __form.elements[ii];
				__form.removeChild(ff);
			}
		},
//		attachFile : function(name_) {
//			var upwin = openLocation({url:'upload.jsp?ts='+ new Date().time, inNewWindow:true,width:400,height:100,
//				status:'no', toolbar:'no', location:'no', menubar:'no', catchUnload:true});
//			setTimeout(function() {
//				upwin.focus();
//				upwin.init(name_);
//			}, 500);
//		},

		removeFile: function(name_) {
			for (var ii = 0; ii < __form.elements.length; ii++) {
				var ff = __form.elements[ii];
				if (ff.name === name_) {
					__form.removeChild(ff);
					__dispatchNotification({type: 'file-removed', name: ff.name, value: ff.value});
					break;
				}
			}
		},

		addFile: function(fileElm_) {
			// replace element;
			this.removeFile(fileElm_.name);
			__form.appendChild(fileElm_);
			__dispatchNotification({type: 'file-added', data: fileElm_.value});
		},

		addField: function(name_, value_) {
			var field = document.createElement('input');
			field.name = name_;
			field.value = value_;
			field.type = 'hidden';

			// replace element;
			this.removeField(name_);
			__form.appendChild(field);
		},

		removeField: function(name_) {
			for (var ii = 0; ii < __form.elements.length; ii++) {
				var ff = __form.elements[ii];
				if (ff.name === name_) {
					__form.removeChild(ff);
					break;
				}
			}
		},

		startUpload: function(params_) {
			if (params_) {
				for (var key in params_) {
					var val = params_[key];
					var elm = document.createElement('input');
					elm.name = key;
					elm.value = val;
					elm.type = 'hidden';
					__form.appendChild(elm);
				}
			}
			if (__form.elements.length === 0) {
				__dispatchNotification({type: "upload-failed", data: "nothing-to-upload"});
				return;
			}
			__form.submit();
		}
	};
};

/**
 * @class Upload
 * @memberof butor
 * @description Resources uploader.
 *
 * This uploader creates an iFrame where all the files' url are stores.
 * Once all the files selected, the uploader submits each file url in order to be treated by any Back-End service.
 *
 * @param {String} url - URL of the file.
 * @fires butor.Upload#upload-ready
 * @fires butor.Upload#upload-started
 * @fires butor.Upload#upload-finished
 * @fires butor.Upload#file-added
 * @fires butor.Upload#file-removed
 */
butor.Upload = butor.Upload || butor.Class.define({
	_url : null,
	_form : null,
	_if : null,
	_id : null,

	construct : function(url) {
		this._url = url;
		this._if = $('<iframe style="display:none;" width="0" height="0">');
		$('body').append(this._if);
		this.reset();
	},

	/**
	 * @method getId
	 * @description Gets the current ID.
	 * @memberof butor.Upload#
	 * @return {Number} ID.
	 */
	getId : function() {
		return this._id;
	},

	/**
	 * @method reset
	 * @description Reloads the form and all the upload triggers.
	 * @memberof butor.Upload#
	 */
	reset : function() {
		this.clear();
		var self = this;
		this._id = butor.Utils.getUUID();
		this._if.attr('id', this._id);
//		, 'XXonload': function() {
//			try {
//				//var resp = null;
//				var fc = self._if[0].contentDocument.body.firstChild;
//				if (fc) {
//					var resp = fc.textContent;
//					self.fire('upload-finished', resp);
//					//self.dispose();
//				}
//
//			} catch (ex) {
//				LOGGER.error('Upload():' + ex);
//			}
//		}});

		this._form && this._form.remove();

		// let the iframe initialise before adding the form
		setTimeout($.proxy(function() {
			this._form = $('<form>').attr({'action':this._url, 'method':'post','enctype':'multipart/form-data'});
			this._if.contents().find('body').append(this._form);

			this._if.bind('load', $.proxy(function() {
				this.fire('upload-finished');
				this.dispose();
			}, this));

			this._form.bind('submit', function() {
				self.fire('upload-started');
			});
			this.fire('upload-ready');
		}, this), 100);
	},

	/**
	 * @method getResponse
	 * @memberof butor.Upload#
	 * @description Gets the response of the uploading request.
	 */
	getResponse : function() {
		var fc = this._if[0].contentDocument.body.firstChild;
		if (fc) {
			return fc.textContent;
		}
		return null;
	},

	/**
	 * @method dispose
	 * @memberof butor.Upload#
	 * @description Clears the DOM of the iframe used for the upload setup.
	 */
	dispose : function() {
		this.clear();
		this._if && this._if.remove();
	},

	/**
	 * @method clear
	 * @memberof butor.Upload#
	 * @description Clears all elements in the form.
	 */
	clear : function() {
		this._form && this._form.empty();
	},

	/**
	* @method removeFile
	* @memberof butor.Upload#
	* @description Removes the uploaded file.
	* @param {name} name - Name of the file to remove.
	*/
	removeFile : function(name) {
		this.removeField(name);
	},

	/**
	 * @method addFile
	 * @memberof butor.Upload#
	 * @description Adds a new file to the form.
	 * @param {jQueryElement} fileJqe - jQuery element of the file to add.
	 */
	addFile : function(fileJqe) {
		this._checkSanity();
		// replace element;
		this.removeFile(fileJqe.attr('name'));
		this._form.append(fileJqe);

		this.fire('file-added', fileJqe.val());
	},

	/**
	* @method addField
	* @memberof butor.Upload#
	* @description Adds a field in the form.
	* @param {String} name - Name of the field. If a field exists yet with this name, it is replaced by the new field.
	* @param {String} value - Default value of the input.
	*/
	addField : function(name, value) {
		this._checkSanity();
		var field = $('<input>');
		field.attr({'name' : name, 'value' : value, 'type' : 'hidden'});

		// replace element;
		this.removeField(name);
		this._form.append(field);
	},

	/**
	* @method removeField
	* @memberof butor.Upload#
	* @description Removes a field in the form.
	* @param {String} name - Name of the field.
	*/
	removeField : function(name) {
		this._checkSanity();
		var ff = this._form.find('[name="' +name +'"]');
		if (ff[0]) {
			ff.remove();
			this.fire('file-removed', {'name': ff.attr('name'), 'value': ff.val});
		}
	},

	/**
	* @method startUpload
	* @memberof butor.butor.Upload#
	* @description Starts the upload by sending upload parameters.
	* @param {Array} params - Parameters to send for the upload.
	*/
	startUpload : function(params) {
		this._checkSanity();
		if (params) {
			for (var key in params) {
				var val = params[key];
				this.addField(key, val);
			}
		}
		if (this._form.find('*').length == 0) {
			throw "butor.Upload.startUpload : Nothing to upload"
		}
		this._form.submit();
	},
	_checkSanity : function() {
		if (!this._if || !this._form) {
			throw "butor.Upload._checkSanity : Illegal state. Missing iframe."
		}
	}
});

/**
 * Upload started event.
 * @event butor.Upload#upload-started
 */

/**
 * Upload ready event.
 * @event butor.Upload#upload-ready
 */

/**
 * Upload finished event.
 * @event butor.Upload#upload-failed
 */

/**
 * File added event.
 * @event butor.Upload#file-added
 * @param {string} fileUrl - Url of the file to upload.
 */

/**
 * File removed event.
 * @event butor.Upload#file-removed
 */

//# sourceURL=butor.upload.js
