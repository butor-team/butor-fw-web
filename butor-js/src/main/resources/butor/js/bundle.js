/*
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
butor.bundles = {
	'Portal': {'fr': 'Portail'},
	'All': {'fr': 'Tout'},
	'all-f': {'en': 'All', 'fr': 'Toute'},
	'(All)': {'fr': '(Tout)'},
	'(all-f)': {'en': '(All)', 'fr': '(Toute)'},
	'None': {'fr': 'Aucun'},
	'none-f': {'fr': 'Aucune'},
	'top': {'en': 'TOP', 'fr': 'HAUT'},
	'More': {'fr': 'Plus'},
	'options': {'en': 'Options', 'fr': 'Options'},
	'profile': {'en': 'Profile', 'fr': 'Profil'},
	'Email': {'fr': 'Courriel'},
	'active-m': {'en': 'Active', 'fr': 'Actif'},
	'active-f': {'en': 'Active', 'fr': 'Active'},
	'Creation date': {'fr': 'Date de création'},
	'Start date': {'fr': 'Date de début'},
	'End date': {'fr': 'Date de fin'},
	'Updated': {'fr': 'M.À.J.'},
	'Updated by': {'fr': 'M.À.J. par'},
	'Filter': {'fr': 'Filtre'},
	'Theme': {'fr': 'Thème'},
	'Status': {'fr': 'Statut'},
	'Amount': {'fr': 'Montant'},
	'Account': {'fr': 'Compte'},
	'User': {'fr': 'Usager'},
	'#': {'en': '#', 'fr': '#'},
	'ID': {'en': 'ID', 'fr': 'ID'},
	// actions
	'Sign out': {'fr': 'Déconnecter'},
	'Sign in': {'fr': 'Se connecter'},
	'Change password': {'fr': 'Modifier mot de passe'},
	'Change challenge questions': {'fr': 'Modifier questions de sécurité'},
	'Cancel': {'fr': 'Annuler'},
	'Close': {'fr': 'Fermer'},
	'Modify': {'fr': 'Modifier'},
	'Apply': {'fr' : 'Appliquer'},
	'Add': {'fr': 'Ajouter'},
	'Remove': {'fr': 'Supprimer'},
	'Delete': {'fr': 'Effacer'},
	'Refresh': {'fr': 'Actualiser'},
	'Reset': {'fr': 'Initialiser'},
	'Save': {'fr': 'Sauvegarder'},
	'Submit': {'fr': 'Soumettre'},
	'View': {'fr': 'Consulter'},
	'Preview': {'fr': 'Apperçu'},
	'Validate': {'fr': 'Valider'},
	'Edit': {'fr': 'Modifier'},
	'Back': {'fr': 'Retour'},
	'Search': {'fr': 'Recherche'},
	'Select': {'fr': 'Sélectionner'},
	'Loading': {'en': 'Loading ...', 'fr': 'Chargement en cours ...'},
	'Saving': {'en': 'Saving ...', 'fr': 'Sauvegarde en cours ...'},
	'Initializing': {'en': 'Initializing ...', 'fr': 'Initialisation en cours ...'},
	'Searching': {'en': 'Searching ...', 'fr': 'Recherche en cours ...'},
	'Working': {'en': 'Working ...', 'fr': 'En cours ...'},
	'Saved': {'fr': 'Sauvegarde effectuée'},
	'From': {'fr': 'De'},
	'To': {'fr': 'À'},
	'from': {'fr': 'de'},
	'to': {'fr': 'à'},
	'--- select ---': {'fr': '--- choisir ---'},

	// messages
	'Start date is not valid!' : {'fr': "La date de début n'est pas valide!"},
	'End date is not valid!' : {'fr': "La date de fin n'est pas valide!"},
	'Start date must be greater or equal to end date!' : {'fr': 'La date de début doit être inférieure ou égale à la date de fin'},
	'More rows!' : {'fr':'Plus de rangées!'},
	'Scroll to bottom to load more rows' : {'fr':"Défiler jusqu'en bas pour charger plus de rangées"},
	'INCOMPLETE_TABLE_SORT_WARN' :
		{'fr':"Attention! Les rangées ne sont pas toutes chargées. Le trie s'applique seulement aux rangées présentes dans cette table. Pour charger plus de rangées, défiler jusqu'à la fin de la table.",
		'en':'Attention! Not all rows are loaded! Sort apply only on rows present in this table. To load more rows, scroll down to the end of table.'},

	'Your session has timed out': {'fr' : 'Votre session a expiré'},
	'SESSION_TIMEDOUT': {'en' : 'Your session has timed out', 'fr' : 'Votre session a expiré'},
	'SERVICE_TIMEOUT': {'en': 'Service call timed out! Please retry in few moments',
		'fr': 'Temps réponse dépassé! SVP reéssayer dans quelques instants'},
	'SERVICE_NOT_AVAILABLE': {'en': 'Service not available! Please retry in few moments', 'fr': 'Service non disponible! SVP reéssayer dans quelques instants'},
	'Service unreachable! Please retry in few moments': {'fr': 'Impossible de communiquer avec le service! SVP reéssayer dans quelques instants'},
	'Service call timed out! Please retry in few moments': {'fr': 'Temps réponse dépassé! SVP reéssayer dans quelques instants'},
	'SERVICE_FAILURE': {'en': 'Service failure!', 'fr': 'Echec service!'},
	'UPDATE_FAILURE': {'en': 'Failed to update! Data has been changed by other party. Please retry.',
		'fr': 'Mise à jour impossible! Les données ont été modifiées depuis la dernière lecture. SVP reéssayer.'},
	'INSERT_FAILURE': {'en': 'Failed to insert! Please retry.',
		'fr': 'Ajout impossible! SVP reéssayer.'},
	'INVALID_ARG': {'en': '{1} is not valid!', 'fr': '{1} n\'est pas valide!'},
	'MISSING_ARG': {'en': '{1} is required!', 'fr': '{1} est obligatoire!'}
};
//# sourceURL=butor.bundles.js
