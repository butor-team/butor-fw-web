0. Se placer dans le dossier de la jsdoc
    cd non-du-projet/src/main/jsdoc/

# Installation de JSDoc locale
1. Installation de Node.js

  + OSX : “brew install node”
  + Fedora : “sudo yum install nodejs npm”
  + voir [**NodeJS**](https://nodejs.org/en/download/package-manager/)

  + Il faut s'assurer que le binaire `node`existe. L'installation de node sous linux installe le binaire `nodejs`. Faire un symilink 

2. Installation de JSDoc3

  + npm install jsdoc (_voir_ [github.com/jsdoc3](https://github.com/jsdoc3/jsdoc))

# Utilisation de JSDoc au sein du projet
3. Ajouter les permissions

    chmod u+x ./makedoc.sh

3. Compiler la JSDoc

    ./makedoc.sh

4. Le résultat se trouve dans le dossier `out`

# **Optionnel: tout est contenu dans le script `makedoc.sh`** Utilisation de JSDoc3 en local

  + “chmod u+x ~/node_modules/jsdoc/jsdoc.js”
  + La configuration des templates/plugins/includes sont dans le fichier conf.json
    node ~/node_modules/jsdoc/jsdoc.js -c ./conf.json

Le résultat se trouve dans le répertoire :
    “<répertoire d’execution de la commande précédente>/out”

# Désinstaller JSDoc3

    npm uninstall jsdoc (avec le switch -g si installé globalement)
