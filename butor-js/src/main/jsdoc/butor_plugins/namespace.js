exports.handlers = {
	newDoclet: function(e) {
		// e.doclet will refer to the newly created doclet
		// you can read and modify properties of that doclet if you wish
		if (e.doclet.kind === 'namespace') {
			/*TEST*
			console.log(e.doclet.name);
			console.log(e.doclet.longname);
			console.log(e.doclet.memberof);
			console.log('---');
			/**/

			e.doclet.memberof = null;
			e.doclet.shortname = e.doclet.name;
			e.doclet.name = e.doclet.longname;
		}

		if (e.doclet.kind === 'class') {
			/*TEST*
			console.log(e.doclet.name);
			console.log(e.doclet.longname);
			console.log(e.doclet.memberof);
			console.log('---');
			/**/

			e.doclet.shortname = e.doclet.name;
			e.doclet.name = e.doclet.longname;
		}
	}
};
