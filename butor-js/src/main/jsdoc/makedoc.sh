#!/bin/sh

# GLOBAL
RED=`tput setaf 1`
YELLOW=`tput setaf 3`
BOLD=`tput bold`
RESET=`tput sgr0`
CURRENT="$PWD"

function print_info {
  echo "${YELLOW}${BOLD}$1${RESET}"
}

# NODE SETUP
NODE_MODULES="$CURRENT/node_modules";

function install_node_dependencies {
  if [ ! -d $NODE_MODULES ]; then
    print_info "build missing npm dependencies"
    npm install;
  fi
}

# JSDOC SETUP
JSDOC_HOME="$CURRENT/node_modules/jsdoc"
BUTOR_TEMPLATE="$CURRENT/butor_template";
BUTOR_PLUGINS="$CURRENT/butor_plugin";


### Prepare butor-plugins
rm -r $JSDOC_HOME/templates/butor
cp -R $BUTOR_TEMPLATE $JSDOC_HOME/templates/
mv $JSDOC_HOME/templates/butor_template $JSDOC_HOME/templates/butor

### Prepare butor-template
cp $BUTOR_PLUGINS/* $JSDOC_HOME

### Make JSDoc
./node_modules/.bin/jsdoc -c ./conf.json
