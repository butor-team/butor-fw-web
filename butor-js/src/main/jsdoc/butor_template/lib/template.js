


var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var utility;
(function (utility) {
    var AttributesUtility = (function () {
        function AttributesUtility() {
        }
        AttributesUtility.prototype.formatSignatureAttributes = function (item) {
            var attributes = [];
            if (item.optional) {
                attributes.push('opt');
            }
            if (item.nullable) {
                attributes.push('nullable');
            }
            else {
                attributes.push('non-null');
            }
            if (item.readonly) {
                attributes.push('readonly');
            }
            return attributes;
        };
        return AttributesUtility;
    })();
    var ParameterUtility = (function (_super) {
        __extends(ParameterUtility, _super);
        function ParameterUtility() {
            _super.apply(this, arguments);
        }
        ParameterUtility.prototype.updateItemname = function (item) {
            var itemName = item.name || '';
            if (item.variable)
                itemName = "&hellip;" + itemName;
            return {
                name: itemName,
                attributes: this.formatSignatureAttributes(item)
            };
        };
        ParameterUtility.prototype.formatParameters = function (parameters) {
            var params = [];
            for (var i = 0; i < parameters.length; i++) {
                if (parameters[i].name && parameters[i].name.indexOf('.') === -1) {
                    params.push(this.updateItemname(parameters[i]));
                }
            }
            return params;
        };
        return ParameterUtility;
    })(AttributesUtility);
    utility.ParameterUtility = ParameterUtility;
    var ReturnUtility = (function (_super) {
        __extends(ReturnUtility, _super);
        function ReturnUtility() {
            _super.apply(this, arguments);
        }
        ReturnUtility.prototype.getSignatureTypes = function (item) {
            var returnTypeNames = [];
            if (item.type) {
                item.type.names.forEach(function (name) {
                    if (name) {
                        returnTypeNames.push(name);
                    }
                });
            }
            return returnTypeNames;
        };
        ReturnUtility.prototype.formatReturns = function (returns) {
            var returnAttributes = [];
            var returnTypes = [];
            for (var i = 0; i < returns.length; i++) {
                this.formatSignatureAttributes(returns[i]).forEach(function (item) {
                    returnAttributes.push(item);
                });
                this.getSignatureTypes(returns[i]).forEach(function (item) {
                    returnTypes.push(item);
                });
            }
            return {
                types: returnTypes,
                attributes: returnAttributes
            };
        };
        return ReturnUtility;
    })(AttributesUtility);
    utility.ReturnUtility = ReturnUtility;
})(utility || (utility = {}));
var utility;
(function (utility) {
    var useless_properties = [
        '___id',
        '___s'
    ];
    function clean(doclet) {
        for (var property in doclet) {
            if (useless_properties.indexOf(property) !== -1) {
                delete doclet[property];
            }
        }
        return doclet;
    }
    utility.clean = clean;
})(utility || (utility = {}));





var publishers;
(function (publishers) {
    var Publisher = (function () {
        function Publisher(doclet) {
            this._doclet = null;
            this._kind = null;
            this._see = null;
            this._ancestors = [];
            this._id = null;
            this._doclet = utility.clean(doclet);
            this._kind = doclet.kind;
            this._see = [];
        }
        Publisher.prototype.makeSeeLinks = function (createLink) {
            if (!this._doclet.see)
                return;
            for (var _i = 0, _a = this._doclet.see; _i < _a.length; _i++) {
                var hash = _a[_i];
                if (!/^(#.+)/.test(hash)) {
                    this._see.push(hash);
                }
            }
        };
        Publisher.prototype.extractHTMLFromSimpleTag = function (tag) {
            var beginTagIndex = tag.indexOf('>');
            var endTagIndex = tag.indexOf('<', 2);
            return {
                begin: tag.slice(0, beginTagIndex + 1),
                content: tag.slice(beginTagIndex + 1, endTagIndex),
                end: tag.slice(endTagIndex, tag.length)
            };
        };
        Publisher.prototype.makeAncestors = function (getAncestorLinks) {
            var ancestors = getAncestorLinks(this._doclet);
            if (ancestors && ancestors.length != 0) {
                for (var i = 0; i < ancestors.length; i++) {
                    var regex = /href="([\.\$\(\)a-zA-Z]+)">\.?([\.\$\(\)a-zA-Z]+)</g;
                    var result = regex.exec(ancestors[i]);
                    this._ancestors.push({
                        href: result[1],
                        content: result[2]
                    });
                }
            }
        };
        Publisher.prototype.makeSignatureID = function (longnameToUrl) {
            var url = longnameToUrl[this._doclet.longname];
            if (url.indexOf('#') > -1) {
                this._id = url.split(/#\.?|\//).pop();
            }
            else {
                this._id = this._doclet.longname.split(/\./).pop();
            }
        };
        Publisher.prototype.makeSignatureParameter = function () { };
        Publisher.prototype.makeSignatureReturn = function () { };
        Publisher.prototype.makeSignature = function (dep) {
            this.makeAncestors(dep.getAncestorLinks);
            this.makeSignatureID(dep.longnameToUrl);
            this.makeSignatureParameter();
            this.makeSignatureReturn();
        };
        Object.defineProperty(Publisher.prototype, "ancestors", {
            get: function () { return this._ancestors; },
            set: function (ancestors) {
                this._ancestors = ancestors;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Publisher.prototype, "doclet", {
            get: function () { return this._doclet; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Publisher.prototype, "introduce", {
            get: function () { return this._doclet.longname; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Publisher.prototype, "kind", {
            get: function () { return this._doclet.kind; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Publisher.prototype, "signature", {
            get: function () { return null; },
            enumerable: true,
            configurable: true
        });
        return Publisher;
    })();
    publishers.Publisher = Publisher;
})(publishers || (publishers = {}));

var tests;
(function (tests) {
    var ModelsTest = (function () {
        function ModelsTest() {
            this.type_doclet = {
                names: ['type'],
            };
            this.property_doclet = {
                name: 'property',
                optional: true,
                nullable: false,
                defaultvalue: 'property',
                type: this.type_doclet,
                description: 'This is a property',
            };
            this.data_doclet = {
                kind: "Data",
                longname: "DataDoclet",
            };
            this.member_doclet = {
                kind: "Member",
                longname: "MemberDoclet",
                meta: {
                    code: {},
                    path: '',
                    shortpath: '',
                    filename: '',
                    range: [0, 1],
                    lineno: 2
                },
                description: "This is a member",
                scope: "Member's scope",
                name: "Member",
                comment: "Comment",
                memberof: "Data",
                examples: [{
                        caption: '',
                        code: '<p></p>'
                    }],
                requires: ""
            };
            this.class_doclet = {
                kind: "Class",
                longname: "ClassDoclet",
                meta: {
                    code: {},
                    path: '',
                    shortpath: '',
                    filename: '',
                    range: [0, 1],
                    lineno: 2
                },
                description: "This is a class",
                scope: "Member's scope",
                name: "Member",
                comment: "Comment",
                memberof: "Data",
                examples: [{
                        caption: '',
                        code: '<p></p>'
                    }],
                requires: "",
                params: [this.parameter_doclet],
                fires: 'Event',
                shortname: 'Class',
            };
            this.return_doclet = {
                name: "ReturnValue",
                type: this.type_doclet,
                description: "This is a return"
            };
            this.parameter_doclet = {
                name: 'parameter',
                optional: false,
                nullable: true,
                defaultvalue: 'parameter',
                type: this.type_doclet,
                description: 'This is a parameter',
                properties: [this.property_doclet],
                variable: false
            };
            this.package_doclet = {
                kind: "File",
                longname: "FileDoclet",
                files: ["file"],
            };
            this.namespace_doclet = {
                kind: "Namespace",
                longname: "NamespaceDoclet",
                meta: {
                    code: {},
                    path: '',
                    shortpath: '',
                    filename: '',
                    range: [0, 1],
                    lineno: 2
                },
                description: "This is a namespace",
                scope: "Namespace's scope",
                name: "Namespace",
                comment: "Comment",
                memberof: "Data",
                examples: [{
                        caption: '',
                        code: '<p></p>'
                    }],
                requires: "",
                shortname: "n"
            };
            this.constant_doclet = {
                kind: "Constant",
                longname: "ConstantDoclet",
                meta: {
                    code: {},
                    path: '',
                    shortpath: '',
                    filename: '',
                    range: [0, 1],
                    lineno: 2
                },
                description: "This is a constant",
                scope: "Constant's scope",
                name: "Constant",
                comment: "Comment",
                memberof: "Data",
                examples: [{
                        caption: '',
                        code: '<p></p>'
                    }],
                requires: "",
                type: this.type_doclet,
                defaultvalue: "CONSTANT"
            };
            this.file_doclet = {
                kind: "File",
                longname: "FileDoclet",
                meta: {
                    code: {},
                    path: '',
                    shortpath: '',
                    filename: '',
                    range: [0, 1],
                    lineno: 2
                },
                description: "This is a file",
                scope: "File's scope",
                name: "File",
                comment: "Comment",
                memberof: "Data",
                examples: [{
                        caption: '',
                        code: '<p></p>'
                    }],
                requires: "",
                copyright: "?",
                licence: "?",
                preserveName: true
            };
            this.function_doclet = {
                kind: "Function",
                longname: "FunctionDoclet",
                meta: {
                    code: {},
                    path: '',
                    shortpath: '',
                    filename: '',
                    range: [0, 1],
                    lineno: 2
                },
                description: "This is a function",
                scope: "Function's scope",
                name: "Function",
                comment: "Comment",
                memberof: "Data",
                examples: [{
                        caption: '',
                        code: '<p></p>'
                    }],
                requires: "",
                params: [this.parameter_doclet],
                returns: [this.return_doclet],
                fires: "Event"
            };
            this.event_doclet = {
                kind: "Event",
                longname: "EventDoclet",
                meta: {
                    code: {},
                    path: '',
                    shortpath: '',
                    filename: '',
                    range: [0, 1],
                    lineno: 2
                },
                description: "This is a event",
                scope: "Event's scope",
                name: "Event",
                comment: "Comment",
                memberof: "Data",
                examples: [{
                        caption: '',
                        code: '<p></p>'
                    }],
                requires: "",
                params: [this.parameter_doclet]
            };
            this.typedef_doclet = {
                kind: "Typedef",
                longname: "TypedefDoclet",
                meta: {
                    code: {},
                    path: '',
                    shortpath: '',
                    filename: '',
                    range: [0, 1],
                    lineno: 2
                },
                description: "This is a typedef",
                scope: "Typedef's scope",
                name: "Typedef",
                comment: "Comment",
                memberof: "Data",
                examples: [{
                        caption: '',
                        code: '<p></p>'
                    }],
                requires: "",
                properties: [this.property_doclet]
            };
        }
        Object.defineProperty(ModelsTest.prototype, "class", {
            get: function () { return this.class_doclet; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelsTest.prototype, "namespace", {
            get: function () { return this.namespace_doclet; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelsTest.prototype, "constant", {
            get: function () { return this.constant_doclet; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelsTest.prototype, "package", {
            get: function () { return this.package_doclet; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelsTest.prototype, "file", {
            get: function () { return this.file_doclet; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelsTest.prototype, "function", {
            get: function () { return this.function_doclet; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelsTest.prototype, "event", {
            get: function () { return this.event_doclet; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelsTest.prototype, "typedef", {
            get: function () { return this.typedef_doclet; },
            enumerable: true,
            configurable: true
        });
        ModelsTest.prototype.test = function () {
            console.log('## TEST: MODELS');
            console.log(this.data_doclet.kind);
            console.log(this.member_doclet.kind);
            console.log(this.package_doclet.kind);
            console.log(this.class_doclet.kind);
            console.log(this.namespace_doclet.kind);
            console.log(this.constant_doclet.kind);
            console.log(this.file_doclet.kind);
            console.log(this.function_doclet.kind);
            console.log(this.event_doclet.kind);
            console.log(this.typedef_doclet.kind);
            console.log("---");
            console.log(this.property_doclet.name);
            console.log(this.parameter_doclet.name);
            console.log(this.return_doclet.description);
            console.log(this.type_doclet.names);
        };
        return ModelsTest;
    })();
    tests.ModelsTest = ModelsTest;
})(tests || (tests = {}));


var publishers;
(function (publishers) {
    var ClassPublisher = (function (_super) {
        __extends(ClassPublisher, _super);
        function ClassPublisher(doclet) {
            _super.call(this, doclet);
            this._param_signature = null;
            this._return_signature = null;
        }
        Object.defineProperty(ClassPublisher.prototype, "introduce", {
            get: function () {
                var doclet = this.doclet;
                return doclet.shortname.replace(/\s+/g, '');
            },
            enumerable: true,
            configurable: true
        });
        ClassPublisher.prototype.makeSignatureParameter = function () {
            var doclet = this.doclet;
            var util = new utility.ParameterUtility();
            if (doclet.params)
                this._param_signature = util.formatParameters(doclet.params);
        };
        ClassPublisher.prototype.makeSignatureReturn = function () {
            var doclet = this.doclet;
            var util = new utility.ReturnUtility();
            if (doclet.returns)
                this._return_signature = util.formatReturns(doclet.returns);
        };
        ClassPublisher.prototype.isInstanciate = function () {
            var doclet = this.doclet;
            return (doclet.virtual ? false : true);
        };
        Object.defineProperty(ClassPublisher.prototype, "doclet", {
            get: function () { return this._doclet; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ClassPublisher.prototype, "signature", {
            get: function () {
                return {
                    instanciate: (this.isInstanciate() ? "new" : null),
                    ancestors: this.ancestors,
                    name: this._id,
                    parameters: this._param_signature,
                    returns: this._return_signature,
                };
            },
            enumerable: true,
            configurable: true
        });
        return ClassPublisher;
    })(publishers.Publisher);
    publishers.ClassPublisher = ClassPublisher;
})(publishers || (publishers = {}));

var publishers;
(function (publishers) {
    var ConstantPublisher = (function (_super) {
        __extends(ConstantPublisher, _super);
        function ConstantPublisher(doclet) {
            _super.call(this, doclet);
        }
        return ConstantPublisher;
    })(publishers.Publisher);
    publishers.ConstantPublisher = ConstantPublisher;
})(publishers || (publishers = {}));

var publishers;
(function (publishers) {
    var EventPublisher = (function (_super) {
        __extends(EventPublisher, _super);
        function EventPublisher(doclet) {
            _super.call(this, doclet);
        }
        return EventPublisher;
    })(publishers.Publisher);
    publishers.EventPublisher = EventPublisher;
})(publishers || (publishers = {}));

var publishers;
(function (publishers) {
    var FilePublisher = (function (_super) {
        __extends(FilePublisher, _super);
        function FilePublisher(doclet) {
            _super.call(this, doclet);
        }
        Object.defineProperty(FilePublisher.prototype, "introduce", {
            get: function () {
                var doclet = this._doclet;
                var pathFiles = doclet.name.split('/');
                return pathFiles[pathFiles.length - 1].replace(/\s+/g, '');
            },
            enumerable: true,
            configurable: true
        });
        return FilePublisher;
    })(publishers.Publisher);
    publishers.FilePublisher = FilePublisher;
})(publishers || (publishers = {}));

var publishers;
(function (publishers) {
    var FunctionPublisher = (function (_super) {
        __extends(FunctionPublisher, _super);
        function FunctionPublisher(doclet) {
            _super.call(this, doclet);
        }
        FunctionPublisher.prototype.makeSignatureParameter = function () {
            var doclet = this._doclet;
            if (doclet.params) {
                var util = new utility.ParameterUtility();
                this._param_signature = util.formatParameters(doclet.params);
            }
        };
        FunctionPublisher.prototype.makeSignatureReturn = function () {
            var doclet = this._doclet;
            if (doclet.returns) {
                var util = new utility.ReturnUtility();
                this._return_signature = util.formatReturns(doclet.returns);
            }
        };
        Object.defineProperty(FunctionPublisher.prototype, "doclet", {
            get: function () {
                return this._doclet;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FunctionPublisher.prototype, "signature", {
            get: function () {
                return {
                    ancestors: this.ancestors,
                    name: this.doclet.name,
                    parameters: this._param_signature,
                    returns: this._return_signature
                };
            },
            enumerable: true,
            configurable: true
        });
        return FunctionPublisher;
    })(publishers.Publisher);
    publishers.FunctionPublisher = FunctionPublisher;
})(publishers || (publishers = {}));

var publishers;
(function (publishers) {
    var JQueryPluginPublisher = (function (_super) {
        __extends(JQueryPluginPublisher, _super);
        function JQueryPluginPublisher(doclet) {
            _super.call(this, doclet);
            this._kind = 'jQueryPlugin';
        }
        Object.defineProperty(JQueryPluginPublisher.prototype, "doclet", {
            get: function () {
                return this._doclet;
            },
            enumerable: true,
            configurable: true
        });
        JQueryPluginPublisher.prototype.isInstanciate = function () {
            if (this.doclet.mustBeInitialized) {
                return true;
            }
            else {
                return false;
            }
        };
        Object.defineProperty(JQueryPluginPublisher.prototype, "signature", {
            get: function () {
                return {
                    instanciate: (this.isInstanciate() ? '' : null),
                    ancestors: this.ancestors,
                    name: this.doclet.name,
                    parameters: this._param_signature,
                    returns: this._return_signature
                };
            },
            enumerable: true,
            configurable: true
        });
        return JQueryPluginPublisher;
    })(publishers.ClassPublisher);
    publishers.JQueryPluginPublisher = JQueryPluginPublisher;
})(publishers || (publishers = {}));

var publishers;
(function (publishers) {
    var MemberPublisher = (function (_super) {
        __extends(MemberPublisher, _super);
        function MemberPublisher(doclet) {
            _super.call(this, doclet);
        }
        Object.defineProperty(MemberPublisher.prototype, "introduce", {
            get: function () {
                var doclet = this._doclet;
                return doclet.name.replace(/\s+/g, '');
            },
            enumerable: true,
            configurable: true
        });
        return MemberPublisher;
    })(publishers.Publisher);
    publishers.MemberPublisher = MemberPublisher;
})(publishers || (publishers = {}));

var publishers;
(function (publishers) {
    var NamespacePublisher = (function (_super) {
        __extends(NamespacePublisher, _super);
        function NamespacePublisher(doclet) {
            _super.call(this, doclet);
        }
        Object.defineProperty(NamespacePublisher.prototype, "introduce", {
            get: function () {
                var doclet = this._doclet;
                return doclet.shortname.replace(/\s+/g, '');
            },
            enumerable: true,
            configurable: true
        });
        return NamespacePublisher;
    })(publishers.Publisher);
    publishers.NamespacePublisher = NamespacePublisher;
})(publishers || (publishers = {}));

var publishers;
(function (publishers) {
    var PackagePublisher = (function (_super) {
        __extends(PackagePublisher, _super);
        function PackagePublisher(doclet) {
            _super.call(this, doclet);
        }
        return PackagePublisher;
    })(publishers.Publisher);
    publishers.PackagePublisher = PackagePublisher;
})(publishers || (publishers = {}));

var publishers;
(function (publishers) {
    var SingletonPublisher = (function (_super) {
        __extends(SingletonPublisher, _super);
        function SingletonPublisher(doclet) {
            _super.call(this, doclet);
        }
        Object.defineProperty(SingletonPublisher.prototype, "signature", {
            get: function () {
                return {
                    ancestors: this.ancestors,
                    name: this._id,
                    parameters: this._param_signature,
                    returns: this._return_signature
                };
            },
            enumerable: true,
            configurable: true
        });
        return SingletonPublisher;
    })(publishers.ClassPublisher);
    publishers.SingletonPublisher = SingletonPublisher;
})(publishers || (publishers = {}));

var publishers;
(function (publishers) {
    var TypedefPublisher = (function (_super) {
        __extends(TypedefPublisher, _super);
        function TypedefPublisher(doclet) {
            _super.call(this, doclet);
        }
        return TypedefPublisher;
    })(publishers.Publisher);
    publishers.TypedefPublisher = TypedefPublisher;
})(publishers || (publishers = {}));


var tests;
(function (tests) {
    var SingletonModelTest = (function (_super) {
        __extends(SingletonModelTest, _super);
        function SingletonModelTest() {
            _super.apply(this, arguments);
            this.singleton_doclet = {
                kind: "Singleton",
                longname: "SingletonDoclet",
                meta: {
                    code: {},
                    path: '',
                    shortpath: '',
                    filename: '',
                    range: [0, 1],
                    lineno: 2
                },
                description: "This is a Singleton",
                scope: "Singleton's scope",
                name: "Singleton",
                comment: "Comment",
                memberof: "Data",
                examples: [{
                        caption: '',
                        code: '<p></p>'
                    }],
                requires: "",
                params: [this.parameter_doclet],
                fires: 'Event',
                shortname: 'Singleton',
                singleton: true,
            };
        }
        Object.defineProperty(SingletonModelTest.prototype, "singleton", {
            get: function () { return this.singleton_doclet; },
            enumerable: true,
            configurable: true
        });
        SingletonModelTest.prototype.test = function () {
            _super.prototype.test.call(this);
            console.log('---');
            console.log(this.singleton_doclet.kind);
        };
        return SingletonModelTest;
    })(tests.ModelsTest);
    tests.SingletonModelTest = SingletonModelTest;
})(tests || (tests = {}));


var tests;
(function (tests) {
    var JQueryPluginModelTest = (function (_super) {
        __extends(JQueryPluginModelTest, _super);
        function JQueryPluginModelTest() {
            _super.apply(this, arguments);
            this.jquery_plugin_doclet = {
                kind: "JQueryPlugin",
                longname: "JQueryPluginDoclet",
                meta: {
                    code: {},
                    path: '',
                    shortpath: '',
                    filename: '',
                    range: [0, 1],
                    lineno: 2
                },
                description: "This is a JQueryPlugin",
                scope: "JQueryPlugin's scope",
                name: "JQueryPlugin",
                comment: "Comment",
                memberof: "Data",
                examples: [{
                        caption: '',
                        code: '<p></p>'
                    }],
                requires: "",
                params: [this.parameter_doclet],
                returns: [this.return_doclet],
                fires: 'Event',
                shortname: 'JQueryPlugin',
                singleton: true,
                plugin: true,
                mustBeInitialized: true
            };
        }
        Object.defineProperty(JQueryPluginModelTest.prototype, "jQueryPlusginDoclet", {
            get: function () { return this.jquery_plugin_doclet; },
            enumerable: true,
            configurable: true
        });
        JQueryPluginModelTest.prototype.test = function () {
            _super.prototype.test.call(this);
            console.log("---");
            console.log(this.jquery_plugin_doclet.kind);
        };
        return JQueryPluginModelTest;
    })(tests.SingletonModelTest);
    tests.JQueryPluginModelTest = JQueryPluginModelTest;
})(tests || (tests = {}));












var tests;
(function (tests) {
    var PublisherTest = (function () {
        function PublisherTest() {
            this._publishers = [];
            this._package_publishers = [];
            this._class_publishers = [];
            this._file_publishers = [];
            this._function_publishers = [];
            this._event_publishers = [];
            this._typedef_publishers = [];
            this._namespace_publishers = [];
            this._constant_publishers = [];
        }
        PublisherTest.prototype.pushPublisher = function (doclet) {
            switch (doclet.kind) {
                case 'package':
                    this._package_publishers.push(new publishers.PackagePublisher(doclet));
                    break;
                case 'class':
                    if (doclet.jquery_plugin) {
                        this._class_publishers.push(new publishers.JQueryPluginPublisher(doclet));
                    }
                    else if (doclet.singleton) {
                        this._class_publishers.push(new publishers.SingletonPublisher(doclet));
                    }
                    else {
                        this._class_publishers.push(new publishers.ClassPublisher(doclet));
                    }
                    break;
                case 'file':
                    this._file_publishers.push(new publishers.FilePublisher(doclet));
                    break;
                case 'function':
                    this._function_publishers.push(new publishers.FunctionPublisher(doclet));
                    break;
                case 'event':
                    this._event_publishers.push(new publishers.EventPublisher(doclet));
                    break;
                case 'typedef':
                    this._typedef_publishers.push(new publishers.TypedefPublisher(doclet));
                    break;
                case 'namespace':
                    this._namespace_publishers.push(new publishers.NamespacePublisher(doclet));
                    break;
                case 'constant':
                    this._constant_publishers.push(new publishers.ConstantPublisher(doclet));
                    break;
                default:
                    this._publishers.push(new publishers.MemberPublisher(doclet));
                    console.warn('doclet ' + doclet.longname + 'is not wrapped for templating!');
            }
        };
        PublisherTest.prototype.wrap = function (doclet) {
            var publisher = null;
            switch (doclet.kind) {
                case 'package':
                    publisher = new publishers.PackagePublisher(doclet);
                    break;
                case 'class':
                    if (doclet.jquery_plugin) {
                        publisher = new publishers.JQueryPluginPublisher(doclet);
                    }
                    else if (doclet.singleton) {
                        publisher = new publishers.SingletonPublisher(doclet);
                    }
                    else {
                        publisher = new publishers.ClassPublisher(doclet);
                    }
                    break;
                case 'file':
                    publisher = new publishers.FilePublisher(doclet);
                    break;
                case 'function':
                    publisher = new publishers.FunctionPublisher(doclet);
                    break;
                case 'event':
                    publisher = new publishers.EventPublisher(doclet);
                    break;
                case 'typedef':
                    publisher = new publishers.TypedefPublisher(doclet);
                    break;
                case 'namespace':
                    publisher = new publishers.NamespacePublisher(doclet);
                    break;
                case 'constant':
                    publisher = new publishers.ConstantPublisher(doclet);
                    break;
                default:
                    publisher = new publishers.MemberPublisher(doclet);
                    console.warn('doclet ' + doclet.longname + 'is not wrapped for templating!');
            }
            return publisher;
        };
        PublisherTest.prototype.displayPublishersIntroduction = function () {
            var publishers = this.publishers;
            for (var i = 0; i < publishers.length; i++) {
                console.log(publishers[i].introduce);
            }
        };
        PublisherTest.prototype.displayPublishersKind = function () {
            var publishers = this.publishers;
            for (var i = 0; i < publishers.length; i++) {
                console.log(publishers[i].kind);
            }
        };
        PublisherTest.prototype.displayPublishersAncestors = function () {
            var publishers = this.publishers;
            for (var i = 0; i < publishers.length; i++) {
                console.log(publishers[i].ancestors);
            }
        };
        PublisherTest.prototype.displayPublishersSignature = function (kind) {
            var publishers = this.getPublishersOfKind(kind);
            for (var i = 0; i < publishers.length; i++) {
                if (publishers[i].signature) {
                    console.log(publishers[i].signature);
                    if (publishers[i].signature.parameters)
                        for (var j = 0; j < publishers[i].signature.parameters.length; j++)
                            console.log(publishers[i].signature.parameters[j].attributes);
                    console.log("-----");
                }
            }
        };
        Object.defineProperty(PublisherTest.prototype, "publishers", {
            get: function () {
                var publishers = [];
                this._publishers.forEach(function (item) { publishers.push(item); });
                this._class_publishers.forEach(function (item) { publishers.push(item); });
                this._package_publishers.forEach(function (item) { publishers.push(item); });
                this._namespace_publishers.forEach(function (item) { publishers.push(item); });
                this._function_publishers.forEach(function (item) { publishers.push(item); });
                this._typedef_publishers.forEach(function (item) { publishers.push(item); });
                this._file_publishers.forEach(function (item) { publishers.push(item); });
                this._event_publishers.forEach(function (item) { publishers.push(item); });
                this._constant_publishers.forEach(function (item) { publishers.push(item); });
                return publishers;
            },
            enumerable: true,
            configurable: true
        });
        PublisherTest.prototype.displayFunctionPublishersReturns = function () {
            var returns = [];
            for (var i = 0; i < this._function_publishers.length; i++) {
                var doclet = this._function_publishers[i].doclet;
                returns.push(doclet.returns);
            }
        };
        PublisherTest.prototype.getPublishersOfKind = function (kind) {
            var publishers = null;
            switch (kind) {
                case 'package':
                    publishers = this._package_publishers;
                    break;
                case 'class':
                    publishers = this._class_publishers;
                    break;
                case 'file':
                    publishers = this._file_publishers;
                    break;
                case 'function':
                    publishers = this._function_publishers;
                    break;
                case 'event':
                    publishers = this._event_publishers;
                    break;
                case 'typedef':
                    publishers = this._typedef_publishers;
                    break;
                case 'namespace':
                    publishers = this._namespace_publishers;
                    break;
                case 'constant':
                    publishers = this._constant_publishers;
                    break;
                default:
                    publishers = this.publishers;
            }
            return publishers;
        };
        return PublisherTest;
    })();
    tests.PublisherTest = PublisherTest;
})(tests || (tests = {}));

var writers;
(function (writers) {
    var SignatureWriter = (function () {
        function SignatureWriter() {
            this._exclude_attributes = ['non-null'];
        }
        SignatureWriter.prototype.writeAttributes = function (attributes) {
            var str = "";
            if (attributes) {
                str = str + "<span class='signature-attributes'>";
                for (var j = 0; j < attributes.length; j++) {
                    var attribute = attributes[j];
                    if (this._exclude_attributes.indexOf(attribute) === -1) {
                        str = str + attribute;
                        if (j < attributes.length - 1)
                            str = str + ", ";
                    }
                }
                if (str[str.length - 2] === ",") {
                    str = str.slice(0, -2);
                }
                str = str + "</span>";
            }
            return str;
        };
        SignatureWriter.prototype.writeParameters = function (parameters) {
            var str = "";
            str = str + "<span class='signature'>(";
            if (parameters) {
                for (var i = 0; i < parameters.length; i++) {
                    var parameter = parameters[i];
                    str = str + parameter.name + this.writeAttributes(parameter.attributes);
                    if (i < parameters.length - 1)
                        str = str + ", ";
                }
            }
            str = str + ")</span>";
            return str;
        };
        SignatureWriter.prototype.writeAncestors = function (ancestors) {
            var str = "";
            if (ancestors) {
                str = str + "<span class='ancestors'>";
                for (var i = 0; i < ancestors.length; i++) {
                    var ancestor = ancestors[i];
                    str = str + "<a href='" + ancestor.href + "'>" + ancestor.content + "</a>&#46;";
                }
                str = str + "</span>";
            }
            return str;
        };
        SignatureWriter.prototype.writeReturn = function (returns) {
            var str = "";
            if (returns && returns.types && returns.types.length > 0) {
                str = str + "<span class='type-signature'> &rarr; {" + returns.types[0] + "}</span>";
            }
            return str;
        };
        SignatureWriter.prototype.writeSignature = function (signature) {
            var str = "";
            if (signature.instanciate) {
                str = str + "<span class='type-signature'>" + signature.instanciate + "</span> ";
            }
            str = str + this.writeAncestors(signature.ancestors);
            str = str + signature.name;
            str = str + this.writeParameters(signature.parameters);
            str = str + this.writeReturn(signature.returns);
            return str;
        };
        return SignatureWriter;
    })();
    writers.SignatureWriter = SignatureWriter;
})(writers || (writers = {}));

module.exports.tests = tests;
module.exports.writers = writers;