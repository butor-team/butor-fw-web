var Gulp = require('gulp');

Gulp.task('default', function() {
  // place code for your default task here
});

Gulp.task('copy-plugins', function() {
  Gulp.src('butor_plugins/*.js')
    .pipe(Gulp.dest('./node_modules/jsdoc/plugins/'));
});

Gulp.task('copy-template', function() {
  Gulp.src('butor_template/**')
    .pipe(Gulp.dest('./node_modules/jsdoc/templates/butor/'));
});

Gulp.task('doc', function(done) {
  var exec = require('child_process').exec;
  exec('rm out/*');
  exec('node ./node_modules/.bin/jsdoc -c ./conf.json', undefined, done);
});

Gulp.task('watch', function() {
  Gulp.watch('../resources/butor/js/*.js', ['doc']);
});
