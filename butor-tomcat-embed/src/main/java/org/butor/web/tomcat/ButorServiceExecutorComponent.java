/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.web.tomcat;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;

import javax.servlet.http.HttpServletResponse;

import org.butor.json.CommonRequestArgs;
import org.butor.json.JsonHelper;
import org.butor.json.JsonResponseHeader;
import org.butor.json.JsonResponseMessage;
import org.butor.json.JsonServiceRequest;
import org.butor.json.service.BinResponseHandler;
import org.butor.json.service.Context;
import org.butor.json.service.ResponseHandler;
import org.butor.json.service.ServiceManager;
import org.butor.utils.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

/**
 * 
 */
public class ButorServiceExecutorComponent {
	protected Logger logger = LoggerFactory.getLogger(getClass());
	private ServiceManager serviceManager;
	private ExecutorService executor;
	private int maxPayloadLengthToLog = -1;
	private Set<String> servicesToNotLogArgs = Collections.emptySet();
	protected Set<String> servicesToNotLogResponses = Collections.emptySet();

	private JsonHelper jsh = new JsonHelper();
	
	public void execute(JsonServiceRequest req, InputStream attachmentIS, HttpServletResponse resp) throws Exception {

		final String logReqInfo = String.format("ns: %s, service: %s, reqId: %s, sessionId: %s, userId: %s, domain: %s",  
				req.getNamespace(), req.getService(), req.getReqId(), req.getSessionId(), req.getUserId(), req.getDomain());

		boolean logArgs = !this.servicesToNotLogArgs.contains(req.getNamespace() +";" +req.getService());
		if (logArgs) {
			if (maxPayloadLengthToLog < 0 || req.getServiceArgsJson().length() <= maxPayloadLengthToLog) {
				logger.info(String.format("REQUEST: %s, args: %s",	
					logReqInfo, req.getServiceArgsJson()));
			} else {
				if (logger.isDebugEnabled()) {
					logger.debug(String.format("REQUEST: %s, args: %s",	
						logReqInfo, req.getServiceArgsJson()));
				} else {
					int argsLen = req.getServiceArgsJson().length();
					String chunck = req.getServiceArgsJson().substring(0, maxPayloadLengthToLog);
					logger.info(String.format("REQUEST: %s, args: %s... %d chars (truncated - full args in debug level)",
						logReqInfo, chunck, argsLen));
				}
			}
		} else {
			logger.info(String.format("REQUEST: %s, args: %s",	
				logReqInfo, "/*censored*/"));
		}

		OutputStream pos = resp.getOutputStream();
		
		Runnable worker = null;
		if (serviceManager.isBinary(req)) {
			if(attachmentIS != null){
				worker = createBinWorker(attachmentIS, pos, req, logReqInfo);
			}
			else{
				worker = createBinWorker(null, pos, req, logReqInfo);
			}
		} else {
			resp.setContentType("text/json");

			worker = createJsonWorker(pos, req, logReqInfo);
		}
		worker.run();
	}

	public Runnable createJsonWorker(final OutputStream pos, final JsonServiceRequest req,
			final String logReqInfo) {
		
		final boolean logResponse = !this.servicesToNotLogResponses
				.contains(req.getNamespace() + ";" + req.getService());
		

		final ResponseHandler<Object> streamer = new ResponseHandler<Object>() {
			@Override
			public void end() {
				// ok
			}

			@Override
			public boolean addRow(Object row_) {
				if (row_ == null) {
					return false;
				}
				try {
					String chunk = jsh.serialize(row_);
					if (logResponse && logger.isDebugEnabled()) {
						logger.debug("RESPONSE: {}, row: {}", logReqInfo, chunk);
					}
					pos.write(chunk.getBytes());
					pos.write(0);// null Delimiter "//---".getBytes()
					pos.flush();
					return true;
				} catch (IOException e) {
					logger.warn("Failed while writing a response row!", e);
				}
				return false;
			}

			@Override
			public boolean addMessage(Message message_) {
				try {
					JsonResponseMessage msg = new JsonResponseMessage(
							req.getReqId());
					msg.setMessage(message_);
					String chunk = jsh.serialize(msg);
					if (logResponse && logger.isDebugEnabled()) {
						logger.debug("RESPONSE: {}, message: {}", logReqInfo,
								chunk);
					}
					pos.write(chunk.getBytes());
					pos.write(0);// null Delimiter
					pos.flush();
					return true;
				} catch (IOException e) {
					logger.warn("Failed while writing a response message!", e);
				}
				return false;
			}

			@Override
			public Type getResponseType() {
				return Object.class;
			}
		};
		final Context ctx = new Context() {
			@Override
			public ResponseHandler<Object> getResponseHandler() {
				return streamer;
			}

			@Override
			public CommonRequestArgs getRequest() {
				return req;
			}
		};

		Runnable worker = new Runnable() {
			@Override
			public void run() {
				long time = System.currentTimeMillis();
				boolean success = false;
				Date serviceCallTimestamp = new Date();
				try {
					// delimiter to start response
					pos.write(0);// null Delimiter

					JsonResponseHeader header = new JsonResponseHeader(
							req.getReqId());
					pos.write(jsh.serialize(header).getBytes());
					pos.write(0);// null Delimiter
					pos.flush();

					invoke(ctx);
					//pos.close();
					success = true;
				} catch (Exception e) {
					logger.warn("Failed while invoking service!", e);
				} finally {
					long elapsed = System.currentTimeMillis() - time;
					Object[] args = new Object[] { logReqInfo,
							Boolean.valueOf(success), Long.valueOf(elapsed) };
					logger.info("STATS: {}, success: {}, elapsed: {} ms", args);
					
					// submit (use thread pool for JDBC insertion)
//					jdbcLogging(req, ctx, success, serviceCallTimestamp, elapsed);
				}
			}
		};

		return worker;
	}

	public Runnable createBinWorker(final InputStream is, final OutputStream pos,
			final JsonServiceRequest req, final String logReqInfo) {

		final boolean logResponse = !this.servicesToNotLogResponses
				.contains(req.getNamespace() + ";" + req.getService());

		final ResponseHandler<byte[]> streamer = new BinResponseHandler() {
			boolean contentTypeSet = false;

			@Override
			public OutputStream getOutputStream() {
				return pos;
			}

			@Override
			public InputStream getInputStream() {
				return is;
			}

			@Override
			public void setContentType(String contentType,
					Map<String, String> headers) {
				if (Strings.isNullOrEmpty(contentType)) {
					return;
				}
				if (contentTypeSet) {
					return;
				}
				contentTypeSet = true;
				this.addRow("___content_type___\n".getBytes());
				this.addRow(("Content-Type:" + contentType + "\n").getBytes());
				if (headers != null) {
					for (String h : headers.keySet()) {
						this.addRow((h + ":" + headers.get(h) + "\n")
								.getBytes());
					}
				}
				this.addRow("\n".getBytes());
			}

			@Override
			public void end() {
				// ok
			}

			@Override
			public boolean addRow(byte[] row_) {
				if (row_ == null) {
					return false;
				}
				try {
					pos.write(row_);
					pos.flush();
					return true;
				} catch (IOException e) {
					logger.warn("Failed while writing a response row!", e);
				}
				return false;
			}

			@Override
			public boolean addMessage(Message message_) {
				if (is != null) {
					try {
						JsonResponseMessage msg = new JsonResponseMessage(
								req.getReqId());
						msg.setMessage(message_);
						String chunk = jsh.serialize(msg);
						if (logResponse && logger.isDebugEnabled()) {
							logger.debug("RESPONSE: {}, message: {}",
									logReqInfo, chunk);
						}
						pos.write(chunk.getBytes());
						pos.write(0);// null Delimiter
						pos.flush();
						return true;
					} catch (IOException e) {
						logger.warn("Failed while writing a response message!",
								e);
					}

				} else {
					logger.error("Unsupported! return Message on binary reponse handler");
				}
				return false;
			}

			@Override
			public Type getResponseType() {
				return byte[].class;
			}
		};
		final Context ctx = new Context() {
			@Override
			public ResponseHandler getResponseHandler() {
				return streamer;
			}

			@Override
			public CommonRequestArgs getRequest() {
				return req;
			}
		};
		Runnable worker = new Runnable() {
			@Override
			public void run() {
				long time = System.currentTimeMillis();
				boolean success = false;
				Date serviceCallTimestamp = new Date();
				try {
					invoke(ctx);
					pos.close();
					success = true;
				} catch (Exception e) {
					logger.warn("Failed while invoking service!", e);
				} finally {
					long elapsed = System.currentTimeMillis() - time;
					Object[] args = new Object[] { logReqInfo,
							Boolean.valueOf(success), Long.valueOf(elapsed) };
					logger.info("STATS: {}, success: {}, elapsed: {} ms", args);
					
//					jdbcLogging(req, ctx, success, serviceCallTimestamp, elapsed);
				}
			}

			
		};

		return worker;
	}

	public void invoke(Context ctx) {
		serviceManager.invoke(ctx);
	}
	public ExecutorService getExecutor() {
		return executor;
	}

	public void setExecutor(ExecutorService executor) {
		this.executor = executor;
	}

	public ServiceManager getServiceManager() {
		return this.serviceManager;
	}

	public void setServiceManager(ServiceManager serviceManager) {
		this.serviceManager = serviceManager;
	}

	public void setMaxPayloadLengthToLog(int maxPayloadLengthToLog) {
		this.maxPayloadLengthToLog = maxPayloadLengthToLog;
	}

	public void setServicesToNotLogArgs(Set<String> servicesToNotLogArgs) {
		this.servicesToNotLogArgs = servicesToNotLogArgs;
		if (this.servicesToNotLogArgs == null) {
			this.servicesToNotLogArgs = Collections.emptySet();
		}
	}

	public void setServicesToNotLogResponses(Set<String> servicesToNotLogResponses) {
		this.servicesToNotLogResponses = servicesToNotLogResponses;
		if (this.servicesToNotLogResponses == null) {
			this.servicesToNotLogResponses = Collections.emptySet();
		}
	}
}