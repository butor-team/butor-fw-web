/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package org.butor.web.tomcat;

import java.io.File;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.catalina.Context;
import org.apache.catalina.LifecycleEvent;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.LifecycleListener;
import org.apache.catalina.WebResourceRoot;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.ContextConfig;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.webresources.StandardRoot;
import org.apache.coyote.http11.Http11NioProtocol;
import org.apache.tomcat.util.scan.StandardJarScanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.client.repackaged.com.google.common.base.Strings;

/**
 * @author asawan
 *
 */
public class EmbeddedTomcatStarter {
	public final static String PORT = "connector.port";
	public final static String WEB_ROOT = "webRoot";
	public final static String CONTEXT_PATH = "contextPath";
	public final static String MAX_CONNECTIONS = "connector.maxConnections";
	public final static String MIN_SPARE_THREADS = "connector.minSpareThreads";
	public final static String MAX_THREADS = "connector.maxThreads";
	public final static String CONNECTION_TIMEOUT = "connector.connectionTimeout";
	
	private static Logger logger = LoggerFactory.getLogger(EmbeddedTomcatStarter.class);

	public static void main(String[] args) throws Exception {
		String msg = String.format("Supported JVM args : %s, %s, %s, %s, %s, %s, %s", 
				PORT, WEB_ROOT, CONTEXT_PATH, MAX_CONNECTIONS, MIN_SPARE_THREADS, 
				MAX_THREADS, CONNECTION_TIMEOUT);
		System.out.println(msg);
		logger.info(msg);

		String webappDirLocation = System.getProperty(WEB_ROOT);
		if (Strings.isNullOrEmpty(webappDirLocation)) {
			msg = String.format("ERROR! Missing JVM arg {}", WEB_ROOT);
			System.err.println(msg);
			logger.error(msg);
			return;
		}

		String port = System.getProperty(PORT);
		if (Strings.isNullOrEmpty(port)) {
			port = "8080";
		}

		String contextPath = System.getProperty(CONTEXT_PATH);
		if (Strings.isNullOrEmpty(contextPath)) {
			contextPath = "";
		}

		msg = String.format("Configuring webapp with port=\"%s\", contextPath=\"%s\", basedir=\"%s\" ...", 
			port, contextPath, new File(webappDirLocation).getAbsolutePath());
		System.out.println(msg);
		logger.info(msg);

		ContextConfig contextConfig = new ContextConfig() {
			private boolean invoked = false;

			@Override
			public void lifecycleEvent(LifecycleEvent event) {
				if (!invoked) {
					StandardJarScanner scanner = new StandardJarScanner();
					scanner.setScanClassPath(false);
					scanner.setScanManifest(false);
					((Context) event.getLifecycle()).setJarScanner(scanner);
					invoked = true;
				}
				super.lifecycleEvent(event);
			}
		};

		Connector connector = new Connector(Http11NioProtocol.class.getName());
		connector.setPort(Integer.valueOf(port));
		
		// print current/default connector properties
		logger.info("connector defaults settings : maxConnections={}, minSpareThreads={}, maxThreads={}, connectionTimeout={}", 
				connector.getProperty("maxConnections"), 
				connector.getProperty("minSpareThreads"), 
				connector.getProperty("maxThreads"),
				connector.getProperty("connectionTimeout"));

		String val = System.getProperty(MAX_CONNECTIONS);
		if (!Strings.isNullOrEmpty(val)) {
			connector.setProperty("maxConnections", val);
		}
		val = System.getProperty(MAX_THREADS);
		if (!Strings.isNullOrEmpty(val)) {
			connector.setProperty("maxThreads", val);
		}
		val = System.getProperty(MIN_SPARE_THREADS);
		if (!Strings.isNullOrEmpty(val)) {
			connector.setProperty("minSpareThreads", val);
		}
		val = System.getProperty(CONNECTION_TIMEOUT);
		if (!Strings.isNullOrEmpty(val)) {
			connector.setProperty("connectionTimeout", val);
		}

		final Tomcat tomcat = new Tomcat();
		tomcat.getService().addConnector(connector);
		tomcat.setConnector(connector);
		
		// print current connector properties
		logger.info("connector settings : maxConnections={}, minSpareThreads={}, maxThreads={}, connectionTimeout={}", 
				connector.getProperty("maxConnections"), 
				connector.getProperty("minSpareThreads"), 
				connector.getProperty("maxThreads"),
				connector.getProperty("connectionTimeout"));

		StandardContext ctx = (StandardContext) tomcat.addWebapp(tomcat.getHost(), contextPath,
				new File(webappDirLocation).getAbsolutePath(), (LifecycleListener) contextConfig);
		ctx.setWorkDir(new File(webappDirLocation, "work").getAbsolutePath());

		final AtomicBoolean contextStarted = new AtomicBoolean(false);
		ctx.addApplicationLifecycleListener(new ServletContextListener() {
			@Override
			public void contextInitialized(ServletContextEvent sce) {
				contextStarted.set(true);
				System.out.println("Context initialized");
				logger.info("Context initialized");
			}
			
			@Override
			public void contextDestroyed(ServletContextEvent sce) {
				// failed to start context
				contextStarted.set(false);
				System.out.println("Context destroyed");
				logger.info("Context destroyed");
			}
		});

		WebResourceRoot resources = new StandardRoot(ctx);
		ctx.setResources(resources);

		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				System.out.println("Stoping tomcat ...");
				logger.info("Stoping tomcat ...");
				try {
					tomcat.stop();
				} catch (LifecycleException e) {
					e.printStackTrace();
				}
			}
		});

		tomcat.start();
		
		if (contextStarted.get()) {
			tomcat.getServer().await();
		}
	}
}
