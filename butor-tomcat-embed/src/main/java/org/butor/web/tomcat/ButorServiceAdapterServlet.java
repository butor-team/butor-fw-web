/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.web.tomcat;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.butor.json.JsonHelper;
import org.butor.json.JsonServiceRequest;
import org.butor.utils.ApplicationException;
import org.butor.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.google.common.base.Strings;
import com.google.common.io.ByteStreams;

@SuppressWarnings("serial")
public class ButorServiceAdapterServlet extends HttpServlet {
	private Logger logger = LoggerFactory.getLogger(getClass());
	private JsonHelper jsh = new JsonHelper();

	private ButorServiceExecutorComponent serviceCmp = null;

	private boolean allowHtmlTagsInServicesArgs = true;

	private String requestBaseArgsPattern = "^[a-zA-Z0-9_\\\\.\\\\-]+$";
	private String[] requestBaseArgs = new String[] {"sessionId", "reqId", "service", "lang", "streaming"};

	@Override
	public void init(ServletConfig config) {
		WebApplicationContext ctxt = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());
		serviceCmp = checkNotNull((ButorServiceExecutorComponent) ctxt.getBean("jsonHandlerCmp"),
			"Missing jsonHandlerCmp spring bean!");
	}

	public void setAllowHtmlTagsInServicesArgs(boolean allowHtmlTagsInServicesArgs) {
		this.allowHtmlTagsInServicesArgs = allowHtmlTagsInServicesArgs;
	}
	public void setRequestBaseArgsPattern(String requestBaseArgsPattern) {
		this.requestBaseArgsPattern = requestBaseArgsPattern;
	}
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
//			if (serviceCmp == null) {
//				WebApplicationContext ctxt = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
//				serviceCmp = checkNotNull((ButorServiceExecutorComponent) ctxt.getBean("jsonHandlerCmp"),
//					"Missing jsonHandlerCmp spring bean!");
//			}
			
			final JsonServiceRequest fjr;
	
			// Check that we have a file upload request
			boolean isMultipart = req.getContentType() != null
					&& req.getContentType().toLowerCase().indexOf("multipart") > -1;
			if (isMultipart) {
				// upload service handler is a service with a single argument (Context)
				fjr = findMultipPartJsonRequest(req);
				serviceCmp.execute(fjr, req.getInputStream(), resp);
				
			} else {
				fjr = findJsonRequest(req);
				serviceCmp.execute(fjr, null, resp);
			}
		} catch (Exception e) {
			logger.error("Failed!", e);
		}
	}
	
	protected JsonServiceRequest findMultipPartJsonRequest(final HttpServletRequest req)
			throws IOException {
		boolean isMultipart = req.getContentType().toLowerCase().indexOf("multipart") > -1;
		if (!isMultipart) {
			return null;
		}

		// Assume 4 first parts are (in no particular order) session id, service name, language and request id.
		// Do not read other parts. the upload handler (the service) will do read all the parts
		StringBuffer line = new StringBuffer();
		Map<String, String> argsMap = new HashMap<String, String>();

		boolean valueNext = false;
		String name = null;
		InputStream is = req.getInputStream();
		String boundary = null;
		while (true) {
			int b = is.read();
			if (b == -1) {
				break;
			} else if (b != 10 & b != 13) {
				line.append((char) b);
			}

			if (b == 10) {
				String str = line.toString();
				if (boundary == null) {
					boundary = str;
				} else {
					//lineCount += 1;
					if (str.equals(boundary)) {
						valueNext = false;
					} else {
						if (str.toLowerCase().startsWith("content-disposition")) {
							int pos = str.indexOf("name=");
							if (pos > -1) {
								String[] toks = str.split(";");
								name = toks[1].split("=")[1].replaceAll("\"", "");
							} else {
								throw new IllegalArgumentException(
										"second line should contain the part name!");
							}

						} else if (Strings.isNullOrEmpty(str)) {
							valueNext = true;

						} else if (valueNext) {
							argsMap.put(name, str);
						}
					}
				}
				line.setLength(0);
			}

			if (argsMap.size() == 4) {
				break;
			}
		}

		JsonServiceRequest jr = new JsonServiceRequest();
		String userId = null;
		if (req.getUserPrincipal() != null) {
			userId = req.getUserPrincipal().getName();
		}
		jr.setUserId(userId);
		jr.setSessionId(argsMap.get("sessionId"));
		jr.setDomain(req.getServerName());
		jr.setReqId(argsMap.get("reqId"));
		jr.setService(argsMap.get("service"));
		jr.setLang(argsMap.get("lang"));
		jr.setNamespace(argsMap.get("namespace"));

		return jr;
	}

	protected JsonServiceRequest findJsonRequest(final HttpServletRequest req) throws IOException {
		//InputStream is = null;
		String qs;
		Map<String, Object> argsMap = null;

		if (req.getMethod().equalsIgnoreCase("post")) {
			qs = new String(ByteStreams.toByteArray(req.getInputStream()));
			// deserialize into map to pass the validation of args bellow
			argsMap = jsh.deserialize(qs, Map.class);

		} else {
			qs = req.getQueryString();
			argsMap = new HashMap<String, Object>();
			String[] toks = qs.split("&");
			for (String tok : toks) {
				tok = URLDecoder.decode(tok, "utf-8");
				String[] pair = tok.split("=");
				argsMap.put(pair[0], pair.length > 1 ? pair[1] : null);
			}
		}

		if (requestBaseArgsPattern != null && requestBaseArgs != null) {
			for (String arg : requestBaseArgs) {
				if (argsMap.get(arg) == null)
					continue;
				String val = argsMap.get(arg).toString();
				if (!Strings.isNullOrEmpty(val) && !val.matches(requestBaseArgsPattern)) {
					ApplicationException.exception(String.format("bad pattern of arg %s=%s", arg, val));
				}
			}
		}

		JsonServiceRequest jr = new JsonServiceRequest();
		jr.setSessionId((String)argsMap.get("sessionId"));
		jr.setUserId((String)argsMap.get("userId"));
		jr.setDomain(req.getServerName());
		jr.setReqId((String)argsMap.get("reqId"));
		jr.setService((String)argsMap.get("service"));
		jr.setLang((String)argsMap.get("lang"));
		jr.setNamespace((String)argsMap.get("namespace"));
		jr.setStreaming(false);
		if (argsMap.get("streaming") != null) {
			String val = argsMap.get("streaming").toString();
			jr.setStreaming(StringUtil.isEmpty(val) ? false : val.equalsIgnoreCase("yes") || val.equalsIgnoreCase("true"));
		}
		
		if (argsMap.get("rowsPerChunk") != null) {
			String val = argsMap.get("rowsPerChunk").toString();
			if (!StringUtil.isEmpty(val)) {
				try {
					jr.setRowsPerChunk(new BigDecimal(val).intValue());
				} catch (NumberFormatException e) {
					logger.warn("Failed to parse request rowsPerChunk {}", val);
				}
			}
		}
		
		String serviceArgsJson = Strings.nullToEmpty((String)argsMap.get("serviceArgsJson"));
		if (!allowHtmlTagsInServicesArgs) {
			serviceArgsJson = serviceArgsJson.replaceAll("\\\\u003c|<", "&lt;").replaceAll("\\\\u003e|>", "&gt;");
		}
		jr.setServiceArgsJson(serviceArgsJson);

		return jr;
	}
}
