/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.butor.sso;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.ServletException;

import org.apache.oltu.oauth2.as.issuer.OAuthIssuer;
import org.apache.oltu.oauth2.as.issuer.ValueGenerator;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.butor.auth.common.user.User;
import org.butor.checksum.CommonChecksumFunction;
import org.butor.json.JsonHelper;
import org.butor.sso.UserInfoProvider;
import org.butor.sso.oauth2.AccessTokenValidator;
import org.butor.sso.oauth2.AuthToken;
import org.butor.sso.oauth2.OAuth2Filter;
import org.butor.sso.oauth2.OAuthToken;
import org.butor.sso.oauth2.OauthTokenAuthenticator;
import org.butor.sso.oauth2.UserProfileAPIKeyAuthenticator;
import org.butor.sso.oauth2.servlet.OAuth2AuthTokenRequestHandler;
import org.junit.Test;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import com.google.api.client.util.Base64;
import com.google.common.net.HttpHeaders;
import com.google.common.reflect.TypeToken;

public class TestOAuth {

	final String API_KEY;
	final String API_KEY_HASHED;
	public TestOAuth() {
		API_KEY = "90489e0c-990d-41fe-a455-3866f1e9928a";
		API_KEY_HASHED = CommonChecksumFunction.SHA256.generateChecksum(API_KEY);
		System.out.println("API Key:" +API_KEY +", hashed:" +API_KEY_HASHED);
	}
	
	private class TestValueGenerator implements ValueGenerator {

		private String nextValue = "";

		@Override
		public String generateValue() throws OAuthSystemException {
			return nextValue;
		}

		@Override
		public String generateValue(String param) throws OAuthSystemException {
			return nextValue ;
		}
		
		public void setNextValue(String value) {
			nextValue=value;
		}
		
	}
	OAuthIssuer oauthIssuer = new OAuthIssuer(){

		@Override
		public String accessToken() throws OAuthSystemException {
			return "1234";
		}

		@Override
		public String authorizationCode() throws OAuthSystemException {
			return null;
		}

		@Override
		public String refreshToken() throws OAuthSystemException {
			return "4321";
		}};
		OauthTokenAuthenticator apiKeyValidator = new OauthTokenAuthenticator() {
			@Override
			public boolean authenticateClientSecret(String clientId, String clientSecret, String newAccesToken,
					String newRefreshToken) {
				return "user".equals(clientId) && CommonChecksumFunction.SHA256.validateChecksum(clientSecret, API_KEY_HASHED);
			}

			@Override
			public boolean authenticateRefreshToken(String clientId, String refreshToken, String newAccesToken,
					String newRefreshToken) {
				return "4321".equals(refreshToken);
				
			}

			@Override
			public long getTicketTTL() {
				return 1800;
			}
		};
	@Test
	public void testGetToken() throws ServletException, IOException {
		OAuth2AuthTokenRequestHandler servlet = new OAuth2AuthTokenRequestHandler();
		servlet.setOauthIssuer(oauthIssuer);
		servlet.setApiKeyValidator(apiKeyValidator);
		String user = "user";
		MockHttpServletResponse response = testGetToken(servlet, user, API_KEY);
		assertEquals(200,response.getStatus());
		assertEquals("no-cache",response.getHeader("Pragma"));
		assertEquals("no-store",response.getHeader("Cache-Control"));
		String contentAsString = response.getContentAsString();
		assertNotNull(contentAsString);
		
		AuthToken testToken = new JsonHelper().deserialize(contentAsString, TypeToken.of(AuthToken.class).getType());
		assertEquals(1800, testToken.getExpires_in());
		assertEquals("4321", testToken.getRefresh_token());
		assertEquals("1234", testToken.getAccess_token());
	}

	private MockHttpServletResponse testGetToken(OAuth2AuthTokenRequestHandler servlet, String user, String secret)
			throws ServletException, IOException {
		MockHttpServletRequest request = new MockHttpServletRequest("POST","/oauth2/getAccessToken");
		MockHttpServletResponse response = new MockHttpServletResponse();
		String basicDigestHeaderValue = "Basic " + new String(Base64.encodeBase64((user+":"+secret).getBytes()));
		request.addHeader(HttpHeaders.AUTHORIZATION,basicDigestHeaderValue);
		request.addHeader(HttpHeaders.ACCEPT,"application/json");
		request.setContentType("application/x-www-form-urlencoded");
		request.addParameter("grant_type", "client_credentials");
		servlet.handleRequest(request, response);
		return response;
	}
	
	
	private MockHttpServletResponse testRefreshToken(OAuth2AuthTokenRequestHandler servlet, String user, String secret, String refreshToken)
			throws ServletException, IOException {
		MockHttpServletRequest request = new MockHttpServletRequest("POST","/oauth2/getAccessToken");
		MockHttpServletResponse response = new MockHttpServletResponse();
		String basicDigestHeaderValue = "Basic " + new String(Base64.encodeBase64((user+":"+secret).getBytes()));
		request.addHeader(HttpHeaders.AUTHORIZATION,basicDigestHeaderValue);
		request.addHeader(HttpHeaders.ACCEPT,"application/json");
		request.setContentType("application/x-www-form-urlencoded");
		request.addParameter("refresh_token", refreshToken);
		request.addParameter("grant_type", "refresh_token");
		servlet.handleRequest(request, response);
		return response;
	}
	
	@Test
	public void testRefreshToken() throws ServletException, IOException {
		OAuth2AuthTokenRequestHandler servlet = new OAuth2AuthTokenRequestHandler();
		servlet.setOauthIssuer(oauthIssuer);
		
		servlet.setApiKeyValidator(apiKeyValidator);
		MockHttpServletResponse response = testRefreshToken(servlet,"user",API_KEY,"4321");
		String contentAsString = response.getContentAsString();
		assertEquals(200,response.getStatus());
		assertEquals("no-cache",response.getHeader("Pragma"));
		assertEquals("no-store",response.getHeader("Cache-Control"));
		assertNotNull(contentAsString);

		AuthToken testToken = new JsonHelper().deserialize(contentAsString, TypeToken.of(AuthToken.class).getType());
		assertEquals(1800, testToken.getExpires_in());
		assertEquals("4321", testToken.getRefresh_token());
		assertEquals("1234", testToken.getAccess_token());
	}
	
	@Test
	public void testRefreshTokenMemory() throws ServletException, IOException, OAuthSystemException {
		OAuth2AuthTokenRequestHandler servlet = new OAuth2AuthTokenRequestHandler();
		TestValueGenerator tvg = new TestValueGenerator();
		tvg.setNextValue("1234");
		OAuthIssuer oat = mock(OAuthIssuer.class);
		when(oat.accessToken()).thenReturn("1234");
		when(oat.refreshToken()).thenReturn("4321");
		
		
		servlet.setOauthIssuer(oat);
		
		UserProfileAPIKeyAuthenticator memApiKeyValidator = new UserProfileAPIKeyAuthenticator();
		memApiKeyValidator.setTicketTTL(1800);
		UserInfoProvider infoProvider = mock(UserInfoProvider.class);
		User u = new User();
		u.setAttribute("apiKey", API_KEY_HASHED);
		when(infoProvider.readUser(eq("user"),any(String.class),any(String.class),any(String.class),any(String.class))).thenReturn(u);
		memApiKeyValidator.setInfoProvider(infoProvider);
		TokenManagerMock tokenManager = new TokenManagerMock();
		OAuthToken t = mock(OAuthToken.class);
		tokenManager.refreshTokens.put("1723", t);
		
		assertNotNull(tokenManager.getRefreshTokens().get("1723"));
		assertNull(tokenManager.getRefreshTokens().get("4321"));

		memApiKeyValidator.setTokenManager(tokenManager);
		
		servlet.setApiKeyValidator(memApiKeyValidator);
		MockHttpServletResponse response = testRefreshToken(servlet,"user",API_KEY,"1723");
		assertEquals(200,response.getStatus());
		String contentAsString = response.getContentAsString();
		
		// we make sure that the old token has been removed
		assertNull(tokenManager.getRefreshTokens().get("1723")); 
		
		// we make sure that the new token is there
		assertNotNull(tokenManager.getRefreshTokens().get("4321"));
		
		// we make sure that the new token is there
		assertNotNull(tokenManager.getAccessTokens().get("1234")); 
		
		// we make sure that both token have been added to the client's token list
		assertTrue(tokenManager.getTokensByUser().get("user").contains("4321")); 
		assertTrue(tokenManager.getTokensByUser().get("user").contains("1234"));
		
		assertEquals("no-cache",response.getHeader("Pragma"));
		assertEquals("no-store",response.getHeader("Cache-Control"));
		assertNotNull(contentAsString);

		AuthToken testToken = new JsonHelper().deserialize(contentAsString, TypeToken.of(AuthToken.class).getType());
		assertEquals(1800, testToken.getExpires_in());
		assertEquals("4321", testToken.getRefresh_token());
		assertEquals("1234", testToken.getAccess_token());
	}
	
	
	@Test
	public void testGetNewTokenMemory() throws ServletException, IOException, OAuthSystemException {
		OAuth2AuthTokenRequestHandler servlet = new OAuth2AuthTokenRequestHandler();
		TestValueGenerator tvg = new TestValueGenerator();
		tvg.setNextValue("1234");
		OAuthIssuer oat = mock(OAuthIssuer.class);
		TokenManagerMock tokenManager = new TokenManagerMock();
		
		when(oat.accessToken()).thenReturn("1234");
		when(oat.refreshToken()).thenReturn("4321");
		
		
		servlet.setOauthIssuer(oat);
		
		UserProfileAPIKeyAuthenticator memApiKeyValidator = new UserProfileAPIKeyAuthenticator();
		UserInfoProvider infoProvider = mock(UserInfoProvider.class);
		User u = new User();
		u.setAttribute("apiKey", API_KEY_HASHED);
		when(infoProvider.readUser(eq("user"),any(String.class),any(String.class),any(String.class),any(String.class))).thenReturn(u);
		memApiKeyValidator.setInfoProvider(infoProvider);
		
		OAuthToken t = mock(OAuthToken.class);
		when(t.getClientId()).thenReturn("user");
		
		//simulation of a get new token when tokens for that clients are already in memory.
		tokenManager.saveAccessToken("user", "1723", "1234",0);
		
		memApiKeyValidator.setTokenManager(tokenManager);
		memApiKeyValidator.setTicketTTL(1800);
		servlet.setApiKeyValidator(memApiKeyValidator);
		MockHttpServletResponse response = testGetToken(servlet,"user",API_KEY);
		assertEquals(200,response.getStatus());
		String contentAsString = response.getContentAsString();
		
		// we make sure that the old tokens has been removed
		assertNull(tokenManager.getAccessTokens().get("1723")); 
		assertNull(tokenManager.getRefreshTokens().get("1234")); 
		
		// we make sure that the new access token is there
		assertNotNull(tokenManager.getAccessTokens().get("1234")); 
		// we make sure that the new refresh token is there
		assertNotNull(tokenManager.getRefreshTokens().get("4321"));
		
		// we make sure that both token have been added to the client's token list
		assertTrue(tokenManager.getTokensByUser().get("user").contains("4321")); 
		assertTrue(tokenManager.getTokensByUser().get("user").contains("1234"));
		
		assertEquals("no-cache",response.getHeader("Pragma"));
		assertEquals("no-store",response.getHeader("Cache-Control"));
		assertNotNull(contentAsString);

		AuthToken testToken = new JsonHelper().deserialize(contentAsString, TypeToken.of(AuthToken.class).getType());
		assertEquals(1800, testToken.getExpires_in());
		assertEquals("4321", testToken.getRefresh_token());
		assertEquals("1234", testToken.getAccess_token());
	}
	
	
	@Test
	public void testValidateTokenMemory() throws ServletException, IOException, OAuthSystemException {
		OAuth2Filter servlet = new OAuth2Filter();
		TestValueGenerator tvg = new TestValueGenerator();
		tvg.setNextValue("1234");
		OAuthIssuer oat = mock(OAuthIssuer.class);
		TokenManagerMock tokenManager = new TokenManagerMock();
		
		when(oat.accessToken()).thenReturn("1234");
		when(oat.refreshToken()).thenReturn("4321");
		
		

		UserProfileAPIKeyAuthenticator memApiKeyValidator = new UserProfileAPIKeyAuthenticator();
		UserInfoProvider infoProvider = mock(UserInfoProvider.class);
		User u = new User();
		u.setAttribute("apiKey", API_KEY_HASHED);
		when(infoProvider.readUser(eq("user"),any(String.class),any(String.class),any(String.class),any(String.class))).thenReturn(u);
		memApiKeyValidator.setInfoProvider(infoProvider);
		
		OAuthToken t = mock(OAuthToken.class);
		when(t.getClientId()).thenReturn("user");
		
		//simulation of a get new token when tokens for that clients are already in memory.
		tokenManager.saveAccessToken("user", "1723", "1234",0);
		
		memApiKeyValidator.setTokenManager(tokenManager);
		
		servlet.setAccessTokenValidator(memApiKeyValidator);
		MockHttpServletResponse response = testValidateAccessToken(servlet,"1723");
		assertEquals(200,response.getStatus());
		
		
		
		assertNotNull(tokenManager.getAccessTokens().get("1723")); 
		assertNotNull(tokenManager.getRefreshTokens().get("1234")); 
		// we make sure that both token have been added to the client's token list
		assertTrue(tokenManager.getTokensByUser().get("user").contains("1723")); 
		assertTrue(tokenManager.getTokensByUser().get("user").contains("1234"));
		assertEquals(2,tokenManager.getTokensByUser().get("user").size());

		//try to get in with the refresh token (which is forbidden)
		response = testValidateAccessToken(servlet,"1234");
		assertEquals(401,response.getStatus());
	}
	
	
	@Test
	public void testValidateToken() throws ServletException, IOException {
		OAuth2Filter servlet = new OAuth2Filter();
		AccessTokenValidator validator  = mock(AccessTokenValidator.class);
		OAuthToken t = mock(OAuthToken.class);
		when(t.getClientId()).thenReturn("user");
		
		when(validator.validateAccessToken(any(String.class))).thenReturn(null).thenReturn(t);
		servlet.setAccessTokenValidator(validator);
		String accessToken = "1234";
		MockHttpServletResponse response = testValidateAccessToken(servlet, accessToken);
		assertEquals(401,response.getStatus());
		response = testValidateAccessToken(servlet, accessToken);
		assertEquals(200,response.getStatus());
	}

	private MockHttpServletResponse testValidateAccessToken(OAuth2Filter servlet,
			String accessToken) throws ServletException, IOException {
		MockFilterChain filter = new MockFilterChain();
		MockHttpServletRequest request = new MockHttpServletRequest("GET","/oauth2/validateAccessToken");
		MockHttpServletResponse response = new MockHttpServletResponse();
		request.addHeader(HttpHeaders.AUTHORIZATION,"Bearer "+accessToken );
		servlet.doFilter(request, response,filter);
		return response;
	}
	

}
