/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.butor.sso;

import java.util.List;
import java.util.Map;

import org.butor.sso.oauth2.MemoryOAuth2TokenManager;
import org.butor.sso.oauth2.OAuth2TokenManager;
import org.butor.sso.oauth2.OAuthToken;

import com.google.common.collect.Maps;

class TokenManagerMock implements OAuth2TokenManager {
	final OAuth2TokenManager tokenManger;
	final Map<String, List<String>> tokensByUser;
	final Map<String, OAuthToken> refreshTokens;
	final Map<String, OAuthToken> accessTokens;
	
	public TokenManagerMock(){
		accessTokens = Maps.newHashMap();
		refreshTokens = Maps.newHashMap();
		tokensByUser = Maps.newHashMap();
		tokenManger = new MemoryOAuth2TokenManager(accessTokens, refreshTokens, tokensByUser);
	}

	public boolean validateRefreshToken(String refreshToken) {
		return tokenManger.validateRefreshToken(refreshToken);
	}

	public OAuthToken validateAccessToken(String accessToken) {
		return tokenManger.validateAccessToken(accessToken);
	}

	public void saveAccessToken(String clientId, String accessToken, String refreshToken, long ttl) {
		tokenManger.saveAccessToken(clientId, accessToken, refreshToken, ttl);
	}

	public Map<String, List<String>> getTokensByUser() {
		return tokensByUser;
	}

	public Map<String, OAuthToken> getRefreshTokens() {
		return refreshTokens;
	}

	public Map<String, OAuthToken> getAccessTokens() {
		return accessTokens;
	}
}
