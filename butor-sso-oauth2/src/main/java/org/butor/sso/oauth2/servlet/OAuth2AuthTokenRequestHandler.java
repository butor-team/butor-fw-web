/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.sso.oauth2.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.oltu.oauth2.as.issuer.MD5Generator;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuer;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuerImpl;
import org.apache.oltu.oauth2.as.request.OAuthTokenRequest;
import org.apache.oltu.oauth2.as.response.OAuthASResponse;
import org.apache.oltu.oauth2.common.error.OAuthError;
import org.apache.oltu.oauth2.common.error.OAuthError.TokenResponse;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthResponse;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.butor.sso.oauth2.OauthTokenAuthenticator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.HttpRequestHandler;

import com.google.common.net.HttpHeaders;

/**
 * OAuth 2.0 based authentication and bearer token generator 
 * Using Apache Oltu
 * 
 * on RFC 6750
 * 
 * @author tbussier
 *
 */
public class OAuth2AuthTokenRequestHandler  implements HttpRequestHandler {

	protected OAuthIssuer oauthIssuer = new OAuthIssuerImpl(new MD5Generator()); 
	protected OauthTokenAuthenticator apiKeyValidator;
	

	protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	

	@Override
	public void handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		OAuthTokenRequest oauthRequest = null;
		try {
			try {
				oauthRequest = new OAuthTokenRequest(request);
				String clientId =oauthRequest.getClientId();
				String grantType = oauthRequest.getGrantType();
				
				String newAccessToken = oauthIssuer.accessToken();
				String newRefreshToken = oauthIssuer.refreshToken();
				if (GrantType.REFRESH_TOKEN.toString().equals(grantType)) {
					String refreshToken = oauthRequest.getRefreshToken();
					if (!apiKeyValidator.authenticateRefreshToken(clientId, refreshToken,newAccessToken,newRefreshToken)) {
						throw OAuthProblemException.error(TokenResponse.INVALID_GRANT);
					}
				}  else if (GrantType.CLIENT_CREDENTIALS.toString().equals(grantType)) {
					if (!apiKeyValidator.authenticateClientSecret(clientId,oauthRequest.getClientSecret(),newAccessToken,newRefreshToken)) {
						throw OAuthProblemException.error(TokenResponse.INVALID_CLIENT);
					}
				} else {
					throw OAuthProblemException.error(TokenResponse.UNSUPPORTED_GRANT_TYPE);
				}


				OAuthResponse r = OAuthASResponse.tokenResponse(HttpServletResponse.SC_OK).setAccessToken(newAccessToken)
						.setExpiresIn(String.valueOf(apiKeyValidator.getTicketTTL())).setRefreshToken(newRefreshToken).buildJSONMessage();

				for (Entry<String, String> h : r.getHeaders().entrySet()) {
					response.setHeader(h.getKey(), h.getValue());
				}
				response.setHeader(HttpHeaders.PRAGMA, "no-cache");
				response.setHeader(HttpHeaders.CACHE_CONTROL, "no-store");
				response.setStatus(r.getResponseStatus());
				PrintWriter pw = response.getWriter();
				pw.print(r.getBody());
				pw.flush();
				pw.close();
			} catch (OAuthProblemException ex) {
				logger.warn("Exception while processing token request",ex);
				OAuthResponse r = OAuthResponse.errorResponse(HttpServletResponse.SC_UNAUTHORIZED).error(ex).buildJSONMessage();
				response.sendError(r.getResponseStatus());
				PrintWriter pw = response.getWriter();
				pw.print(r.getBody());
				pw.flush();
				pw.close();
			}
		} catch (OAuthSystemException e) {
			logger.error("{}",e);
			try {
				OAuthResponse r = OAuthResponse.errorResponse(HttpServletResponse.SC_BAD_REQUEST).error(OAuthProblemException.error(OAuthError.TokenResponse.INVALID_REQUEST)).buildJSONMessage();
				response.sendError(r.getResponseStatus());
				PrintWriter pw = response.getWriter();
				pw.print(r.getBody());
				pw.flush();
				pw.close();
			} catch (OAuthSystemException e1) {
				logger.error("Unable to send response ?!! {}",e);
			}
		}
	}
	
	public void setOauthIssuer(OAuthIssuer oauthIssuer) {
		this.oauthIssuer = oauthIssuer;
	}
	public void setApiKeyValidator(OauthTokenAuthenticator apiKeyValidator) {
		this.apiKeyValidator = apiKeyValidator;
	}
}
