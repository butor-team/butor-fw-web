/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.sso.oauth2;

import org.butor.auth.common.user.User;
import org.butor.checksum.CommonChecksumFunction;
import org.butor.sso.UserInfoProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

public class UserProfileAPIKeyAuthenticator implements OauthTokenAuthenticator,AccessTokenValidator {
	
	private UserInfoProvider infoProvider;
	private OAuth2TokenManager tokenManager;
	private long ttl;
	
	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public boolean authenticateRefreshToken(String clientId, String refreshToken, String newAccessToken,String newRefreshToken) {
		boolean isValid = tokenManager.validateRefreshToken(refreshToken);
		if (isValid) {
			tokenManager.saveAccessToken(clientId, newAccessToken, newRefreshToken, ttl);
			return true;
		}
		return false;
	}

	public void setInfoProvider(UserInfoProvider infoProvider) {
		this.infoProvider = infoProvider;
	}

	@Override
	public boolean authenticateClientSecret(String clientId, String apiKey, String newAccesToken, String newRefreshToken) {
		User user = infoProvider.readUser(clientId, null, null, null, null);
		if (user == null) {
			return false;
		}
		//hashed
		String hak = Strings.nullToEmpty((String)user.getAttribute("apiKey"));
		if (CommonChecksumFunction.SHA256.validateChecksum(apiKey, (String)hak)) {
			tokenManager.saveAccessToken(clientId, newAccesToken, newRefreshToken, ttl);
			return true;
		}
		logger.warn("Invalid API Key of user {}! keys hash do not match", clientId);
		return false;
	}
	
	@Override
	public OAuthToken validateAccessToken(String accessToken) {
		return tokenManager.validateAccessToken(accessToken);
	}

	public void setTokenManager(OAuth2TokenManager tokenManager) {
		this.tokenManager = tokenManager;
	}

	@Override
	public long getTicketTTL() {
		return ttl;
	}


	public void setTicketTTL(long ttl) {
		this.ttl = ttl;
	}




	

}
