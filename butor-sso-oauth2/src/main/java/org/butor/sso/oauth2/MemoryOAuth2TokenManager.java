/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.sso.oauth2;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.google.common.collect.Lists;

public class MemoryOAuth2TokenManager implements OAuth2TokenManager {
	protected final Map<String, OAuthToken> tokens;
	protected final Map<String,OAuthToken> refreshTokens;
	protected final Map<String,List<String>> tokensByUser;

	public MemoryOAuth2TokenManager(Map<String, OAuthToken> tokens, Map<String,OAuthToken> refreshTokens,Map<String,List<String>> tokensByUser) {
		super();
		this.tokens = tokens;
		this.refreshTokens = refreshTokens;
		this.tokensByUser=tokensByUser;
	}



	@Override
	public boolean validateRefreshToken(String token) {
		// 1 - Validate the refresh token (existance in the map is enough)
		// since the refresh token operation contains the authorization header
		// 2 - Remove the token from the map
		OAuthToken oat = refreshTokens.remove(token);
		if (oat==null) {
			return false;
		}
		cleanTokensForClient(oat.getClientId());
		return true;
	}

	@Override
	public OAuthToken validateAccessToken(String token) {
		OAuthToken oat = tokens.get(token);
		if (oat==null) {
			return null;
		}
		if (oat.getExpirationTime() < System.currentTimeMillis()) {
			tokens.remove(token);
			return null;
		}
		return oat;
	}

	@Override
	public void saveAccessToken(String clientId, String accessToken, String refreshToken, long ttl) {
		long expirationTimestamp = ttl <= 0 ? Long.MAX_VALUE : TimeUnit.SECONDS.toMillis(ttl) +  System.currentTimeMillis();
		OAuthTokenImpl oat = new OAuthTokenImpl(clientId,accessToken,refreshToken, expirationTimestamp);

		// remove previously generated tokens.
		cleanAndAddTokensForClient(clientId,accessToken,refreshToken);

		tokens.put(accessToken, oat);
		refreshTokens.put(refreshToken,oat);
	}

	private void cleanTokensForClient(String clientId) {
		cleanAndAddTokensForClient(clientId,null,null);
	}

	private void cleanAndAddTokensForClient(String clientId, String accessToken, String refreshToken) {
		List<String> clientTokens = tokensByUser.get(clientId);
		if (clientTokens != null) {
			Iterator<String> it = clientTokens.iterator();
			while (it.hasNext()) {
				String t = it.next();
				tokens.remove(t);
				refreshTokens.remove(t);
				it.remove();
			}
		} else {
			clientTokens = Lists.newArrayList();
			tokensByUser.put(clientId,clientTokens);
		}
		if (refreshToken != null) {
			clientTokens.add(refreshToken);
		}
		if (accessToken != null) {
			clientTokens.add(accessToken);
		}
	}

}
