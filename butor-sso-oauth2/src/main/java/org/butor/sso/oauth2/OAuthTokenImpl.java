/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.sso.oauth2;

public class OAuthTokenImpl implements OAuthToken{

	private static final long serialVersionUID = -4976428701001180928L;
	
	private final String userId;
	private final long expirationTime;
	private final String accessToken;
	private final String refreshToken;
	
	public OAuthTokenImpl(String userId,String accessToken,String refreshToken, long l) {
		super();
		this.userId = userId;
		this.accessToken = accessToken;
		this.refreshToken = refreshToken;
		this.expirationTime = l;
	}

	/* (non-Javadoc)
	 * @see org.butor.sso.oauth2.OAuthToken#getExpirationTime()
	 */
	@Override
	public long getExpirationTime() {
		return expirationTime;
	}

	/* (non-Javadoc)
	 * @see org.butor.sso.oauth2.OAuthToken#getClientId()
	 */
	@Override
	public String getClientId() {
		return userId;
	}

	/* (non-Javadoc)
	 * @see org.butor.sso.oauth2.OAuthToken#getAccessToken()
	 */
	@Override
	public String getAccessToken() {
		return accessToken;
	}

	/* (non-Javadoc)
	 * @see org.butor.sso.oauth2.OAuthToken#getRefreshToken()
	 */
	@Override
	public String getRefreshToken() {
		return refreshToken;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accessToken == null) ? 0 : accessToken.hashCode());
		result = prime * result + (int) (expirationTime ^ (expirationTime >>> 32));
		result = prime * result + ((refreshToken == null) ? 0 : refreshToken.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OAuthTokenImpl other = (OAuthTokenImpl) obj;
		if (accessToken == null) {
			if (other.accessToken != null)
				return false;
		} else if (!accessToken.equals(other.accessToken))
			return false;
		if (expirationTime != other.expirationTime)
			return false;
		if (refreshToken == null) {
			if (other.refreshToken != null)
				return false;
		} else if (!refreshToken.equals(other.refreshToken))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "OAuthTokenImpl [userId=" + userId + ", expirationTime=" + expirationTime + ", accessToken="
				+ accessToken + ", refreshToken=" + refreshToken + "]";
	}
	

}
