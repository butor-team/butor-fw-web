/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.sso.oauth2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.oltu.oauth2.common.error.OAuthError;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthResponse;
import org.apache.oltu.oauth2.common.message.types.ParameterStyle;
import org.apache.oltu.oauth2.rs.request.OAuthAccessResourceRequest;
import org.apache.oltu.oauth2.rs.response.OAuthRSResponse;
import org.butor.web.servlet.PrincipalServletRequestWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.google.common.net.HttpHeaders;

public class OAuth2Filter implements Filter {
	
	protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	protected AccessTokenValidator accessTokenValidator;
	
	@Override
	public void init(FilterConfig config) throws ServletException {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());
		accessTokenValidator = ctx.getBean(AccessTokenValidator.class);
	}

	@Override
	public void destroy() {
	}		

	@Override
	public void doFilter(ServletRequest req_, ServletResponse resp_,
			FilterChain filterChain_)
		throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest)req_;
		HttpServletResponse response = (HttpServletResponse)resp_;
		OAuthToken oat=null;
		try {
			try {
				OAuthAccessResourceRequest oauthRequest = new OAuthAccessResourceRequest(request,
						ParameterStyle.HEADER);
				String accessToken = oauthRequest.getAccessToken();
				oat = accessTokenValidator.validateAccessToken(accessToken);
				if (oat == null) {
					OAuthResponse r = OAuthRSResponse.errorResponse(HttpServletResponse.SC_UNAUTHORIZED)
							.setError(OAuthError.ResourceResponse.INVALID_TOKEN).buildHeaderMessage();

					response.sendError(r.getResponseStatus());
					response.setHeader(HttpHeaders.WWW_AUTHENTICATE, r.getHeader(HttpHeaders.WWW_AUTHENTICATE));
					response.setStatus(r.getResponseStatus());
					writeResponse(response, r);
					return;

				}
			} catch (OAuthProblemException ex) {
				logger.warn("Exception while processing token request{}", ex);
				OAuthResponse r = OAuthResponse.errorResponse(HttpServletResponse.SC_UNAUTHORIZED).error(ex)
						.buildJSONMessage();
				response.sendError(r.getResponseStatus());
				writeResponse(response, r);
				return;
			}
		} catch (OAuthSystemException e) {
			logger.error("{}", e);
		}
		
		PrincipalServletRequestWrapper srw = new PrincipalServletRequestWrapper(request, oat.getClientId());
		filterChain_.doFilter(srw, resp_);
		
	}
	
	private void writeResponse(HttpServletResponse response, OAuthResponse r) throws IOException {
		PrintWriter pw = response.getWriter();
		pw.print(r.getBody());
		pw.flush();
		pw.close();
	}

	public AccessTokenValidator getApiKeyValidator() {
		return accessTokenValidator;
	}

	public void setAccessTokenValidator(AccessTokenValidator apiKeyValidator) {
		this.accessTokenValidator = apiKeyValidator;
	}

}
