/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.sso.oauth2;

import java.util.List;
import java.util.concurrent.ConcurrentMap;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;

import com.hazelcast.core.HazelcastInstance;

public class HazelcastOAuth2TokenManagerBuilder implements FactoryBean<MemoryOAuth2TokenManager> , InitializingBean, DisposableBean{

	
	private HazelcastInstance hz;
	
	private  ConcurrentMap<String, OAuthToken> accessTokens;
	private  ConcurrentMap<String,OAuthToken> refreshTokens;
	private  ConcurrentMap<String,List<String>> tokensByUser;
	
	private MemoryOAuth2TokenManager tokenManager;
	
	public void setHazelcastInstance(HazelcastInstance hzInstance) {
		this.hz = hzInstance;
	}
	@Override
	public MemoryOAuth2TokenManager getObject() throws Exception {
		return tokenManager;
	}

	@Override
	public Class<?> getObjectType() {
		return MemoryOAuth2TokenManager.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	@Override
	public void destroy() throws Exception {
		hz.shutdown();
		
	}

	@Override
	public void afterPropertiesSet() throws Exception {

		accessTokens = hz.getMap("accessTokens");
		refreshTokens = hz.getMap("refreshTokens");
		tokensByUser= hz.getMap("tokensByUser");
		tokenManager= new MemoryOAuth2TokenManager(accessTokens, refreshTokens, tokensByUser);
		
	}



}
