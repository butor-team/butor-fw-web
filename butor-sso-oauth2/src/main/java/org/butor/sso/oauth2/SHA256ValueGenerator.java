/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.sso.oauth2;

import java.util.UUID;

import org.apache.oltu.oauth2.as.issuer.ValueGenerator;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.butor.checksum.CommonChecksumFunction;

public class SHA256ValueGenerator implements ValueGenerator {

	@Override
	public String generateValue() throws OAuthSystemException {
	  return generateValue(UUID.randomUUID().toString());
	}

	@Override
	public String generateValue(String param) throws OAuthSystemException {
		return CommonChecksumFunction.SHA256.generateChecksum(param);
	}

}
