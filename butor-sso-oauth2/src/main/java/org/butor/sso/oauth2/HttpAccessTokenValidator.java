/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.sso.oauth2;

import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.methods.PostMethod;
import org.butor.json.JsonHelper;
import org.butor.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.reflect.TypeToken;

/*
 *
 * @author asawan
 * @date 30-July-2017
 */
public class HttpAccessTokenValidator implements AccessTokenValidator {
	private Logger logger = LoggerFactory.getLogger(getClass());

	private JsonHelper jsh = new JsonHelper();
	protected String checkAccessTokenUrl = null;

	@Override
	public OAuthToken validateAccessToken(String token) {
		logger.info("Validating oauth2 access token={} wirh URL={} ...", token, checkAccessTokenUrl);
		if (StringUtil.isEmpty(checkAccessTokenUrl)) {
			logger.warn("Got null/empty checkAccessTokenUrl");
			return null;
		}

		PostMethod method = null;
		try {
			method = buildPost(new NameValuePair[] { new NameValuePair("accessToken", token) });
			sendRequest(method);
			return parseReply(method);
		} catch (Exception e) {
			logger.error("{}", e);
		} finally {
			if (method != null) {
				method.releaseConnection();
			}
		}
		return null;
	}

	public PostMethod buildPost(NameValuePair[] args_) {
		PostMethod method = new PostMethod(checkAccessTokenUrl);
		method.getParams().setCookiePolicy(CookiePolicy.DEFAULT);
		method.setRequestBody(args_);
		return method;
	}

	/**
	 * Sends an import request to the wiki.
	 * 
	 * @param m
	 *            The post method to send.
	 * @throws Exception
	 *             In case the transmission fails.
	 */
	private void sendRequest(PostMethod m) throws IOException {
		HttpClient client = new HttpClient();
		client.getHttpConnectionManager().getParams().setConnectionTimeout(5000);

		int status = client.executeMethod(m);

		if (status != HttpStatus.SC_OK) {
			throw new IOException("Post failed with HTTP status : (" + status + ") " + HttpStatus.getStatusText(status));
		}
	}

	private OAuthToken parseReply(PostMethod m) throws IOException {
		String json = m.getResponseBodyAsString();
		OAuthToken token = jsh.deserialize(json, TypeToken.of(OAuthTokenImpl.class).getType()); 
		return token;
	}
	
	public void setCheckAccessTokenUrl(String checkAccessTokenUrl) {
		this.checkAccessTokenUrl = checkAccessTokenUrl;
	}
}
