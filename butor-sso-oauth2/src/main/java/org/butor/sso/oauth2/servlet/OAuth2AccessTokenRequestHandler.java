/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.sso.oauth2.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpStatus;
import org.butor.json.JsonHelper;
import org.butor.sso.oauth2.OAuth2TokenManager;
import org.butor.sso.oauth2.OAuthToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.HttpRequestHandler;

import com.google.common.net.HttpHeaders;

/**
 * OAuth 2.0 based authentication and bearer token generator 
 * Using Apache Oltu
 * 
 * on RFC 6750
 * 
 * @author asawan
 *
 */
public class OAuth2AccessTokenRequestHandler  implements HttpRequestHandler {
	protected final Logger logger = LoggerFactory.getLogger(getClass());
	private OAuth2TokenManager tokenManager;

	@Override
	public void handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			String accessToken = request.getParameter("accessToken");
			logger.info("Validation oauth2 access token={} ...", accessToken);
			OAuthToken oat = tokenManager.validateAccessToken(accessToken);
			response.setHeader(HttpHeaders.PRAGMA, "no-cache");
			response.setHeader(HttpHeaders.CACHE_CONTROL, "no-store");
			
			if (oat == null) {
				logger.info("Oauth2 access token={} in not valid!", accessToken);
				response.setStatus(HttpStatus.SC_UNAUTHORIZED);
			} else {
				logger.info("Oauth2 access token={} is valid", accessToken);
				String json = new JsonHelper().serialize(oat);
				response.setStatus(HttpStatus.SC_OK);
				PrintWriter pw = response.getWriter();
				pw.print(json);
				pw.flush();
				pw.close();
			}
			
		} catch (Exception e) {
			logger.warn("Failed to validate oauth2 access token!", e);
			response.setStatus(HttpStatus.SC_UNAUTHORIZED);
		}
	}
	
	public void setTokenManager(OAuth2TokenManager tokenManager) {
		this.tokenManager = tokenManager;
	}
}
