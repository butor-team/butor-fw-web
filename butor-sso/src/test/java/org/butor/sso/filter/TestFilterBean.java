/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.sso.filter;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import java.util.Map;

import javax.servlet.FilterConfig;

import org.apache.log4j.BasicConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.mock.web.MockServletContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.StaticWebApplicationContext;

import com.google.api.client.util.Maps;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;

public class TestFilterBean {

	@BeforeClass 
	public static void init() {
		BasicConfigurator.configure();
	}

	
	/**
	 * Test blocking default value & ignoredList default value 
	 * @throws Exception
	 */
	@Test
	public void testFilterConfig1() throws Exception {
		final Map<String,String> filterConfigParams = Maps.newHashMap();
		FilterConfig fc = getMockedFilterConfig(filterConfigParams);
		SSOFilterBean filter = new SSOFilterBean();
		filter.init(fc);
		assertTrue(filter.isEnabled());
		assertTrue(filter.ignoredList.isEmpty());
	}
	
	/**
	 * Test when blocking is set to true
	 * @throws Exception
	 */
	@Test
	public void testFilterConfig2() throws Exception {
		final Map<String,String> filterConfigParams = Maps.newHashMap();
		filterConfigParams.put("blocking","true");
		FilterConfig fc = getMockedFilterConfig(filterConfigParams);
		SSOFilterBean filter = new SSOFilterBean();
		filter.init(fc);
		assertTrue(filter.isEnabled());
	}

	/**
	 * Test when blocking has a value
	 * @throws Exception
	 */
	@Test
	public void testFilterConfig3() throws Exception {
		final Map<String,String> filterConfigParams = Maps.newHashMap();
		filterConfigParams.put("blocking","false");
		FilterConfig fc = getMockedFilterConfig(filterConfigParams);
		SSOFilterBean filter = new SSOFilterBean();
		filter.init(fc);
		assertFalse(filter.isEnabled());
	}
	
	/**
	 * Test when ignoreURI is set
	 * @throws Exception
	 */
	@Test
	public void testFilterConfig4() throws Exception {
		final Map<String,String> filterConfigParams = Maps.newHashMap();
		filterConfigParams.put("ignoreURIs","/wl?,/test");
		FilterConfig fc = getMockedFilterConfig(filterConfigParams);
		SSOFilterBean filter = new SSOFilterBean();
		filter.init(fc);
		assertFalse(filter.ignoredList.isEmpty());
		assertEquals(2,filter.ignoredList.size());
		assertEquals("/wl?",filter.ignoredList.get(0));
		assertEquals("/test",filter.ignoredList.get(1));
	}
	
	/**
	 * Test spring priority
	 * @throws Exception
	 */
	@Test
	public void testFilterConfig5() throws Exception {
		final Map<String,String> filterConfigParams = Maps.newHashMap();
		filterConfigParams.put("ignoreURIs","/wl?,/test");
		filterConfigParams.put("blocking","false");
		FilterConfig fc = getMockedFilterConfig(filterConfigParams);
		
		
		SSOFilterBean filter = new SSOFilterBean();
		filter.setEnabled(true);
		filter.setIgnoredList(Lists.newArrayList("/spring"));
		filter.init(fc);
		
		assertFalse(filter.ignoredList.isEmpty());
		assertEquals(1,filter.ignoredList.size());
		assertEquals("/spring",filter.ignoredList.get(0));
		assertTrue(filter.isEnabled());
	}
	
	
	
	protected FilterConfig getMockedFilterConfig(final Map<String, String> filterConfigParams) {
		final StaticWebApplicationContext wac = new StaticWebApplicationContext();
		wac.registerBeanDefinition("validator", new RootBeanDefinition(MockValidator.class));
		MockServletContext sc = new MockServletContext();
		sc.setAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE,wac);
		FilterConfig fc = mock(FilterConfig.class);
		when(fc.getInitParameterNames()).thenReturn(Iterators.asEnumeration(filterConfigParams.keySet().iterator()));
		when(fc.getFilterName()).thenReturn("mockFilter");
		when(fc.getInitParameter(any(String.class))).thenAnswer(new Answer<String>() {
			@Override
			public String answer(InvocationOnMock invocation) throws Throwable {
				return filterConfigParams.get(invocation.getArguments()[0]);
			}
			
		});
		when(fc.getServletContext()).thenReturn(sc);
		return fc;
	}
}
