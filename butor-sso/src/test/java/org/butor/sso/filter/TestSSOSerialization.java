/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.sso.filter;

import static org.junit.Assert.*;

import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;

import org.butor.sso.HazelcastSSOManager;
import org.butor.sso.SSOInfo;
import org.junit.Test;
import org.mockito.Mockito;

import com.google.api.client.util.Maps;
import com.hazelcast.config.Config;
import com.hazelcast.config.InMemoryFormat;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.config.ReplicatedMapConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

public class TestSSOSerialization {

	
	@Test
	public void testWithHzcast() throws Exception {
		Config config = new Config();
		config.setProperty("hazelcast.shutdownhook.enabled", "false");
		NetworkConfig network = new NetworkConfig();
		network.getJoin().getMulticastConfig().setEnabled(true);
		network.setPublicAddress("127.0.0.2");
		network.setPort(11111);
		config.setNetworkConfig(network);

		ReplicatedMapConfig rmc = new ReplicatedMapConfig();
		rmc.setAsyncFillup(true);
		rmc.setInMemoryFormat(InMemoryFormat.BINARY);
		Map<String, ReplicatedMapConfig> rmcm = Maps.newHashMap();
		rmcm.put("sessions",rmc);
		config.setReplicatedMapConfigs(rmcm);

		HazelcastInstance instanceOne = Hazelcast.newHazelcastInstance(config);
		network.setPort(11112);
		HazelcastInstance instanceTwo = Hazelcast.newHazelcastInstance(config);

		
		ScheduledExecutorService service = Mockito.mock(ScheduledExecutorService.class);
		HazelcastSSOManager managerOne = new HazelcastSSOManager(service);
		managerOne.setHazelcastInstance(instanceOne);
		managerOne.afterPropertiesSet();
		SSOInfo soi = managerOne.createSSOSession("userId", "Mr", "Test", "Mr Test", "email");
		
		
		HazelcastSSOManager managerTwo = new HazelcastSSOManager(service);
		managerTwo.setHazelcastInstance(instanceTwo);
		managerTwo.afterPropertiesSet();

		
		assertEquals(soi,managerTwo.getSSOSession(soi.getSsoId()));
	}
	

}
