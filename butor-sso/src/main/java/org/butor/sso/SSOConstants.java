/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.sso;

/*
 *TODO
 *
 * @author asawan
 * @date 23-Jan-09
 */
public final class SSOConstants {
	public static final String SSO_ARG_ACTION = "action";
	public static final String SSO_ARG_SERVICE = "service";
	public static final String SSO_ACTION_LOGIN = "login";
	public static final String SSO_ACTION_LOGOUT = "logout";
	public static final String SSO_ACTION_CHECK_SSO = "check_sso";

	public static final String SSO_ID = "id";
	public static final String SSO_PWD = "pwd";
	public static final String SSO_SSO_ID = "sso_id";
	public static final String SSO_TICKET = "ticket";

	public static final String SSO_RC = "rc";
	public static final String SSO_RC_DSC = "rc_dsc";

	public static final String SSO_RC_SUCCESS = "success";
	public static final String SSO_RC_FAILED = "failed";
	public static final String SSO_RC_MISSING_ARGS = "missing_args";
	public static final String SSO_RC_INVALID_ARGS = "invalid_args";
}
