/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.sso;

import java.util.UUID;

/**
 * @author asawan
 */
public class DefaultSSOSession extends DefaultTicketInfo implements SSOInfo {
	protected long creationTime;
	protected long lastAccessTime;
	protected String ssoId;
	
	/**
	 * Used for externalization only
	 */
	public DefaultSSOSession() {};
	
	protected DefaultSSOSession(String firstName, String lastName, String id, String displayName, String email, String ssoId, long creationTime,long lastAccessTime) {
		super(firstName, lastName, id, displayName, email);
		this.ssoId = ssoId;
		this.lastAccessTime = lastAccessTime;
		this.creationTime = creationTime;
	}
	
	@Override
	public long getLastAccessTime() {
		return lastAccessTime;
	}
	@Override
	public String getSsoId() {
		return ssoId;
	}
	public long getCreationTime() {
		return creationTime;
	}
	
	
	public static class DefaultSSOSessionBuilder extends AbstractDefaultSSOSessionBuilder<DefaultSSOSession,DefaultSSOSessionBuilder> {
		protected String prefix;
		
		public DefaultSSOSession keepAlive(DefaultSSOSession original) {
			setCreationTime(original.getCreationTime());
			setDisplayName(original.getDisplayName());
			setEmail(original.getEmail());
			setFirstName(original.getFirstName());
			setId(original.getId());
			setLastName(original.getLastName());
			setSsoId(original.getSsoId());
			setLastAccessTime(System.currentTimeMillis());
			return build();
		}
		@Override
		public DefaultSSOSession build() {
			if (creationTime == -1L) {
				creationTime = System.currentTimeMillis();
			}
			if (lastAccessTime == -1L) {
				lastAccessTime = this.creationTime;
			}
			if (ssoId == null) {
				String newSsoId = UUID.randomUUID().toString();
				if (prefix != null) {
					newSsoId = prefix +"." +newSsoId;
				}
				this.ssoId = newSsoId;
			}
			return new DefaultSSOSession(firstName,lastName,id,displayName,email,ssoId,creationTime,lastAccessTime);
		}

		public DefaultSSOSessionBuilder setPrefix(String prefix) {
			this.prefix = prefix;
			return this;
		}

		@Override
		protected DefaultSSOSessionBuilder thisBuilder() {
			return this;
		}
		
	}
	
	
	protected static abstract class AbstractDefaultSSOSessionBuilder<T extends DefaultSSOSession, B extends AbstractDefaultSSOSessionBuilder<T,B>>  extends AbstractDefaultTicketInfoBuilder<T,B> {
		protected long creationTime = -1L;
		protected long lastAccessTime = -1L;
		protected String ssoId;
		public B setCreationTime(long creationTime) {
			this.creationTime = creationTime;
			return thisBuilder();
		}
		public B setLastAccessTime(long lastAccessTime) {
			this.lastAccessTime = lastAccessTime;
			return thisBuilder();
		}
		public B setSsoId(String ssoId) {
			this.ssoId = ssoId;
			return thisBuilder();
		}
		
		
		
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (creationTime ^ (creationTime >>> 32));
		result = prime * result + (int) (lastAccessTime ^ (lastAccessTime >>> 32));
		result = prime * result + ((ssoId == null) ? 0 : ssoId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DefaultSSOSession other = (DefaultSSOSession) obj;
		if (creationTime != other.creationTime)
			return false;
		if (lastAccessTime != other.lastAccessTime)
			return false;
		if (ssoId == null) {
			if (other.ssoId != null)
				return false;
		} else if (!ssoId.equals(other.ssoId))
			return false;
		return true;
	}
}
