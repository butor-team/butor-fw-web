/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.sso;

import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.butor.json.util.MapJsonProxy;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import com.google.common.base.Strings;
import com.hazelcast.core.HazelcastInstance;

public class HazelcastSSOManager extends MemSSOManager  implements InitializingBean, DisposableBean{
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public static final String TICKETS_MAP_KEY = "tickets";
	public static final String SESSIONS_MAP_KEY = "sessions";
	
	private Map<String, DefaultSSOSession> distributedSessions;
	private Map<String, Ticket> distributedTickets;
	
	private HazelcastInstance hz;
	private String sessionsMapName = null;
	private String ticketsMapName = null;
	
	public HazelcastSSOManager(ScheduledExecutorService executor) {
		super(executor);
	}
	
	public void setHazelcastInstance(HazelcastInstance hzInstance) {
		this.hz = hzInstance;
	}
	@Override
	protected Map<String, DefaultSSOSession> getSessions() {
		return distributedSessions;
	}
	
	@Override
	protected Map<String, Ticket> getTickets() {
		return distributedTickets;
	}
	

	@Override
	public void afterPropertiesSet() throws Exception {
		if (Strings.isNullOrEmpty(sessionsMapName)) {
			logger.info("sessionsMapName was not set! Considering default name {}", SESSIONS_MAP_KEY);
			sessionsMapName = SESSIONS_MAP_KEY;
		}
		Map<String,String> sessions = hz.getMap(sessionsMapName);
		distributedSessions = new MapJsonProxy<DefaultSSOSession>(sessions,DefaultSSOSession.class);
		
		if (Strings.isNullOrEmpty(ticketsMapName)) {
			logger.info("ticketsMapName was not set! Considering default name {}", TICKETS_MAP_KEY);
			ticketsMapName = TICKETS_MAP_KEY;
		}
		Map<String,String> tickets = hz.getReplicatedMap(TICKETS_MAP_KEY);
		distributedTickets = new MapJsonProxy<Ticket>(tickets,Ticket.class);
	}

	@Override
	public void destroy() throws Exception {
		hz.shutdown();
	}

	public void setSessionsMapName(String sessionsMapName) {
		this.sessionsMapName = sessionsMapName;
	}

	public void setTicketsMapName(String ticketsMapName) {
		this.ticketsMapName = ticketsMapName;
	}


}
