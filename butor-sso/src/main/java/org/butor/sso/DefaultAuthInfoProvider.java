/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.sso;

import org.butor.auth.common.AuthServices;
import org.butor.json.service.Context;
import org.butor.json.service.ResponseHandlerHelper;
import org.butor.json.util.ContextBuilder;
import org.butor.json.util.JsonResponse;
import org.butor.utils.AccessMode;


public class DefaultAuthInfoProvider implements AuthInfoProvider {
	private AuthServices authServices = null;

	public DefaultAuthInfoProvider(){
		super();
	}
	
	@Override
	public Boolean hasAccess(String sys, String func, AccessMode mode, String userId, String sessionId,
			String reqId, String lang, String domain) {
		JsonResponse<Boolean> handler = ResponseHandlerHelper.createJsonResponse(Boolean.class);
		Context<Boolean> ctx = new ContextBuilder<Boolean>()
				.createCommonRequestArgs(userId, lang, sessionId, reqId, domain)
				.setResponseHandler(handler)
				.build();
		authServices.hasAccess(ctx, sys, func, mode);
		return handler.getRow();
	}

	public void setAuthServices(AuthServices authServices) {
		this.authServices = authServices;
	}


}
