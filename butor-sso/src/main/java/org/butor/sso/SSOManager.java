/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.sso;

import java.util.List;

public interface SSOManager {
	SSOInfo createSSOSession(String id);
	SSOInfo createSSOSession(String id, String firstName, String lastName,
			String displayName, String email);
	SSOInfo getSSOSession(String ssoId);
	SSOInfo destroySSOSession(String ssoId);
	String createTicket(String ssoId);
	TicketInfo validateTicket(String token);
	List<DefaultSSOSession> listSessions(String filter);
	void shutdown();
}
