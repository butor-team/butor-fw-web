/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.sso.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.butor.sso.SSOConstants;
import org.butor.sso.SSOException;
import org.butor.sso.SSOHelper;
import org.butor.sso.SSOInfo;
import org.butor.sso.validator.ISSOValidator;
import org.butor.web.servlet.PrincipalServletRequestWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.google.common.base.Strings;

/**
 * Filter usage:
 *
  &lt;filter&gt;
	&lt;filter-name&gt;sso&lt;/filter-name&gt;
	&lt;filter-class&gt;org.butor.sso.filter.SSOFilter&lt;/filter-class&gt;
	&lt;init-param&gt;
		&lt;param-name&gt;blocking&lt;/param-name&gt;
		&lt;param-value&gt;true&lt;/param-value&gt;
	&lt;/init-param&gt;
  &lt;/filter&gt;

  &lt;filter-mapping&gt;
	&lt;filter-name&gt;sso&lt;/filter-name&gt;
	&lt;!-- &lt;servlet-name&gt;action&lt;/servlet-name&gt; --&gt;
	&lt;url-pattern&gt;/*&lt;/url-pattern&gt;
  &lt;/filter-mapping&gt;

 * @deprecated superseded by SSOFilterBean
 * @author asawan
 * 
 */
@Deprecated
public class SSOFilter implements Filter {
	private Logger logger = LoggerFactory.getLogger(getClass());
	protected boolean blocking = true;
	private List<String> ignoredList = null;
	private ISSOValidator validator = null;
	
	private String sessionTimeoutUrl = null;
	private String sessionTimeoutStreamingUrl = null;

	/**
	 * @see javax.servlet.Filter#init(FilterConfig)
	 */
	@Override
	public void init(FilterConfig config) throws ServletException {
		String blocking = config.getInitParameter("blocking");
		this.blocking = (blocking != null && (blocking.equalsIgnoreCase("true") || blocking.equalsIgnoreCase("yes") || blocking.equalsIgnoreCase("n") || blocking.equals("1")));
		this.ignoredList = new ArrayList<String>();
		String ignoreURIs = config.getInitParameter("ignoreURIs");
		if (ignoreURIs != null) {
			ignoredList = Arrays.asList(ignoreURIs.split(","));
		}

		WebApplicationContext ctxt = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());
		validator = ctxt.getBean(ISSOValidator.class);
	
		sessionTimeoutStreamingUrl = validator.getSessionTimeoutStreamingUrl();
		if (Strings.isNullOrEmpty(sessionTimeoutStreamingUrl)) {
			throw new ServletException("Got null sessionTimeoutStreamingUrl from validator!");
		}
		sessionTimeoutUrl = validator.getSessionTimeoutUrl();
		if (Strings.isNullOrEmpty(sessionTimeoutUrl)) {
			throw new ServletException("Got null sessionTimeoutUrl from validator!");
		}
	}

	/**
	 * @see Filter#destroy()
	 */
	@Override
	public void destroy() {		
	}		
	/**
	 * - If no session exists, creates one and initializes the user profile.
	 * -  Does a special check about the validity of the password.  If the 
	 * 	password is expired or has a bad format, it places a flag in the session
	 *  which indicates to any action, that the use must change his password 
	 *  before going further.
	 */
	@Override
	public void doFilter(ServletRequest req_, ServletResponse resp_,
			FilterChain filterChain_)
		throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest)req_;
		HttpServletResponse response = (HttpServletResponse)resp_;

		if (logger.isDebugEnabled()) {
			logger.debug("Filtering request {}?{}", request.getRequestURI(), request.getQueryString());
		}
		// let pass some requests if required
		String reqURI = request.getRequestURI();
		for (String uri : ignoredList){
			if (reqURI.indexOf(uri) > -1){
				logger.info("Let pass request {}?{}", request.getRequestURI(), request.getQueryString());
				filterChain_.doFilter(req_, resp_);
				return;
			}
		}
		
		HttpSession session = request.getSession();
		String ssoId = SSOHelper.getCookie(request, SSOConstants.SSO_SSO_ID);
		SSOInfo ssoInfo = null;
		if (ssoId != null) {
			if (logger.isDebugEnabled())
				logger.debug("Got ssoId={}", ssoId);

			try {
				ssoInfo = validator.validate(ssoId);
			} catch (SSOException e) {
				logger.warn("Failed", e);
			}
		}

		if (ssoInfo != null) {
			if (logger.isDebugEnabled())
				logger.info("Valid ssoId={}, id={}", ssoId, ssoInfo.getId());

			if (session == null || session.getAttribute(SSOConstants.SSO_ID) == null || session.getAttribute(SSOConstants.SSO_SSO_ID) == null) {
				session = request.getSession(true);
				session.setAttribute(SSOConstants.SSO_SSO_ID, ssoId);
				session.setAttribute(SSOConstants.SSO_ID, ssoInfo.getId());

				// set cookie
				SSOHelper.setCookie(response, SSOConstants.SSO_SSO_ID, ssoId, -1, "/");
			}

		} else {
			if (ssoId != null) {
				//remove cookie
				SSOHelper.removeCookie(response, SSOConstants.SSO_SSO_ID, "/");
			}
			if (session != null && session.getAttribute(SSOConstants.SSO_SSO_ID) != null) {
				session.removeAttribute(SSOConstants.SSO_ID);
				session.removeAttribute(SSOConstants.SSO_SSO_ID);
				session.invalidate();
			}
			if (this.blocking) {
				String sp = request.getParameter("streaming");
				boolean ajaxCall = (sp != null && 
						request.getParameter("service") != null &&
								request.getParameter("reqId") != null);
				boolean streaming = sp != null && sp.equals("true");
				logger.info("Invalid ssoId={}. redirecting to login (blocking filter config) ...", ssoId);
				if (ajaxCall) {
					if (!streaming) {
						String reqId = request.getParameter("reqId");
						byte[] bb = ("{\"reqId\":\"" +reqId +"\",\"data\":[],\"messages\":[{\"id\":\"SESSION_TIMEDOUT\",\"sysId\":\"common\",\"type\":\"ERROR\"}]}").getBytes();
						resp_.getOutputStream().write(bb);
					} else {
						String url = sessionTimeoutStreamingUrl +"?ts=" +System.currentTimeMillis();
						logger.info("redirecting to login, URL={}", url);
						response.sendRedirect(url);
					}
				} else {
					String url = sessionTimeoutUrl +"?ts=" +System.currentTimeMillis();
					logger.info("redirecting to login, URL={}", url);
					response.sendRedirect(url);
				}
				return;

			} else if (ssoId != null) {
				logger.info("Invalid ssoId={}. Invalidated session and let pass. (non blocking filter config)", ssoId);
			}
		}

		PrincipalServletRequestWrapper srw = new PrincipalServletRequestWrapper(request, ssoInfo.getId());
		filterChain_.doFilter(srw, resp_);
	}

}
