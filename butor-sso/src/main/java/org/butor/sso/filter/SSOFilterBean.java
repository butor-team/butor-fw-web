/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.sso.filter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.butor.sso.SSOConstants;
import org.butor.sso.SSOException;
import org.butor.sso.SSOHelper;
import org.butor.sso.SSOInfo;
import org.butor.sso.validator.ISSOValidator;
import org.butor.web.servlet.PrincipalServletRequestWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.filter.GenericFilterBean;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

public class SSOFilterBean extends GenericFilterBean{
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	protected Boolean enabled;
	protected List<String> ignoredList;

	
	
	private ISSOValidator validator;

	
	private String sessionTimeoutUrl;
	private String sessionTimeoutStreamingUrl;

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest)req;
		HttpServletResponse response = (HttpServletResponse)resp;

		if (logger.isDebugEnabled()) {
			logger.debug("Filtering request {}?{}", request.getRequestURI(), request.getQueryString());
		}
		// let pass some requests if required
		String reqURI = request.getRequestURI();
		for (String uri : ignoredList){
			if (reqURI.indexOf(uri) > -1){
				logger.info("Let pass request {}?{}", request.getRequestURI(), request.getQueryString());
				chain.doFilter(req, resp);
				return;
			}
		}
		
		HttpSession session = request.getSession();
		String ssoId = SSOHelper.getCookie(request, SSOConstants.SSO_SSO_ID);
		SSOInfo ssoInfo = null;
		if (ssoId != null) {
			if (logger.isDebugEnabled())
				logger.debug("Got ssoId={}", ssoId);

			try {
				ssoInfo = validator.validate(ssoId);
			} catch (SSOException e) {
				logger.warn("Failed", e);
			}
		}

		if (ssoInfo != null) {
			if (logger.isDebugEnabled()) {
				logger.debug("Valid ssoId={}, id={}", ssoId, ssoInfo.getId());
			}
			if (session == null || session.getAttribute(SSOConstants.SSO_ID) == null || session.getAttribute(SSOConstants.SSO_SSO_ID) == null) {
				session = request.getSession(true);
				session.setAttribute(SSOConstants.SSO_SSO_ID, ssoId);
				session.setAttribute(SSOConstants.SSO_ID, ssoInfo.getId());
				// set cookie
				SSOHelper.setCookie(response, SSOConstants.SSO_SSO_ID, ssoId, -1, "/");
			}
		} else {
			if (ssoId != null) {
				//remove cookie
				SSOHelper.removeCookie(response, SSOConstants.SSO_SSO_ID, "/");
			}
			if (session != null && session.getAttribute(SSOConstants.SSO_SSO_ID) != null) {
				session.removeAttribute(SSOConstants.SSO_ID);
				session.removeAttribute(SSOConstants.SSO_SSO_ID);
				session.invalidate();
			}
			if (this.enabled) {
				String sp = request.getParameter("streaming");
				boolean ajaxCall = (sp != null && 
						request.getParameter("service") != null &&
								request.getParameter("reqId") != null);
				boolean streaming = sp != null && sp.equals("true");
				logger.info("Invalid ssoId={}. redirecting to login (blocking filter config) ...", ssoId);
				if (ajaxCall) {
					if (!streaming) {
						String reqId = request.getParameter("reqId");
						byte[] bb = ("{\"reqId\":\"" +reqId +"\",\"data\":[],\"messages\":[{\"id\":\"SESSION_TIMEDOUT\",\"sysId\":\"common\",\"type\":\"ERROR\"}]}").getBytes();
						resp.getOutputStream().write(bb);
					} else {
						String url = sessionTimeoutStreamingUrl +"?ts=" +System.currentTimeMillis();
						logger.info("Redirecting to session timeout streaming url : , URL={}", url);
						response.sendRedirect(url);
					}
				} else {
					String url = sessionTimeoutUrl +"?ts=" +System.currentTimeMillis();
					logger.info("redirecting to session timeout streaming url , URL={}", url);
					response.sendRedirect(url);
				}
				return;

			} else if (ssoId != null) {
				logger.info("Invalid ssoId={}. Invalidated session and let pass. (not enabled filter config)", ssoId);
			}
		}

		PrincipalServletRequestWrapper srw = new PrincipalServletRequestWrapper(request, ssoInfo.getId());
		chain.doFilter(srw, resp);
	}
	
	@Override
	protected void initFilterBean() throws ServletException {
		legacySupport();
		setDefaultValues();
		Preconditions.checkNotNull(validator,"Validator is mandatory!");
		sessionTimeoutUrl = validator.getSessionTimeoutUrl();
		sessionTimeoutStreamingUrl = validator.getSessionTimeoutStreamingUrl();		
		Preconditions.checkArgument(!Strings.isNullOrEmpty(sessionTimeoutUrl), "Got null sessionTimeoutUrl from validator!");
		Preconditions.checkArgument(!Strings.isNullOrEmpty(sessionTimeoutStreamingUrl),"Got null sessionTimeoutStreamingUrl from validator!");
	}
	/**
	 * Support the web.xml fashion as used in butor-sso original SSOFilter
	 * Spring configuration should have priority
	 */
	private void legacySupport() {
		FilterConfig filterConfig = getFilterConfig();
		
		if (filterConfig == null) {
			return;
		}
		if (validator == null) {
			//
			WebApplicationContext ctxt = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
			Preconditions.checkNotNull(ctxt,"Unable to find Spring context or validator not set in spring config");
			validator = ctxt.getBean(ISSOValidator.class);
		}
		
		String blockingLegacyCfg = filterConfig.getInitParameter("blocking");
		if (enabled == null && blockingLegacyCfg != null)  { 
			enabled =  !blockingLegacyCfg.equalsIgnoreCase("false");
		}
		if (ignoredList == null)  { 
			String ignoreURIsLegacyCfg = filterConfig.getInitParameter("ignoreURIs");
			if (!Strings.isNullOrEmpty(ignoreURIsLegacyCfg)) {
				ignoredList = Arrays.asList(ignoreURIsLegacyCfg.split(","));
			}				
		} 
	}

	protected void setDefaultValues() {
		/* Set default values after check for legacy */
		if (ignoredList == null) {
			ignoredList = Lists.newArrayList();
		}
		if (enabled == null) {
			enabled = true;
		}
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void setIgnoredList(List<String> ignoredList) {
		this.ignoredList = ignoredList;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public List<String> getIgnoredList() {
		return ignoredList;
	}

	public void setValidator(ISSOValidator validator) {
		this.validator = validator;
	}

}