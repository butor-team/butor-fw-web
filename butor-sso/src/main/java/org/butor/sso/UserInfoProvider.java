/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.sso;

import org.butor.auth.common.firm.Firm;
import org.butor.auth.common.password.PasswordPolicies;
import org.butor.auth.common.user.User;
import org.butor.auth.common.user.UserKey;
import org.butor.auth.common.user.UserQuestions;

public interface UserInfoProvider {
	User readUser(String id, String sessionId, String reqId, String lang, String domain);
	UserQuestions readUserQuestions(String id, String sessionId, String reqId, String lang, String domain);
	UserKey updateState(User user, String sessionId, String reqId, String lang, String domain);
	UserKey updateUser(User user, String sessionId, String reqId, String lang, String domain);
	UserKey insertUser(User user, String sessionId, String reqId, String lang, String domain);
	UserKey updateQuestions(UserQuestions qrs, String sessionId, String reqId, String lang, String domain);
	Firm readFirm(long firmId, String sessionId, String reqId, String lang, String domain);
	String resetLogin(String id, String url, boolean resetAndSendLink, String sessionId, String reqId, String lang, String domain);
	PasswordPolicies getPwdPolicies(String sessionId, String reqId, String lang, String domain);
}
