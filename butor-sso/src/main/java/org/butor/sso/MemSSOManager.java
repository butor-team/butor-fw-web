/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.sso;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import org.butor.checksum.CommonChecksumFunction;
import org.butor.sso.DefaultTicketInfo.AbstractDefaultTicketInfoBuilder;
import org.butor.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.client.util.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/*
 * @author asawan
 * @date 23-Jan-09/12
 */
public class MemSSOManager implements SSOManager, Runnable {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private ScheduledExecutorService executor = null;
	private final static int CHECK_SESSION_TIMEOUT_INTERVAL = 60000;
	private long ticketTimeoutMS = 10000;
	private int ticketMaxUses = 1;
	private final Map<String, DefaultSSOSession> sessions = Maps.newConcurrentMap();
	private final Map<String, Ticket> tickets = Maps.newConcurrentMap();

	private ScheduledFuture<?> sf = null;
	public final static long DEFAULT_SSO_TIMEOUT_MS = 1000 * 60 * 10;
	private long ssoTimeoutMS = DEFAULT_SSO_TIMEOUT_MS;
	private String ssoIdPrefix;

	protected static class Ticket extends DefaultTicketInfo {
		private String ssoId;
		private int usageCount;
		private long creationTime;
		/**
		 * Used for externalization only
		 */
		public Ticket() {}
		protected Ticket(String firstName, String lastName, String id, String displayName, String email, String ssoId, long creationTime, int usageCount) {
			super(firstName, lastName, id, displayName, email);
			this.creationTime=creationTime;
			this.usageCount=usageCount;
			this.ssoId=ssoId;
		}
	

		public String getSsoId() {
			return ssoId;
		}
		public int getUsageCount() {
			return usageCount;
		}
		public long getCreationTime() {
			return creationTime;
		}
	
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + (int) (creationTime ^ (creationTime >>> 32));
			result = prime * result + ((ssoId == null) ? 0 : ssoId.hashCode());
			result = prime * result + usageCount;
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			Ticket other = (Ticket) obj;
			if (creationTime != other.creationTime)
				return false;
			if (ssoId == null) {
				if (other.ssoId != null)
					return false;
			} else if (!ssoId.equals(other.ssoId))
				return false;
			if (usageCount != other.usageCount)
				return false;
			return true;
		}
	}
	
	protected static class TicketBuilder extends AbstractDefaultTicketInfoBuilder<Ticket, TicketBuilder> {
		private String ssoId;
		private int usageCount=0;
		private long creationTime = -1L;
		
		public Ticket useTicket(Ticket oldTicket) {
			setFirstName(oldTicket.getFirstName());
			setLastName(oldTicket.getLastName());
			setId(oldTicket.getId());
			setDisplayName(oldTicket.getDisplayName());
			setEmail(oldTicket.getEmail());
			setSsoId(oldTicket.getSsoId());
			setCreationTime(oldTicket.getCreationTime());
			setUsageCount(oldTicket.getUsageCount()+1);
			return build();
		}
		
		@Override
		public Ticket build() {
			if (creationTime == -1L) {
				creationTime = System.currentTimeMillis();
			}
			return new Ticket(firstName,lastName,id,displayName,email,ssoId,creationTime,usageCount);
		}

		@Override
		protected TicketBuilder thisBuilder() {
			return this;
		}

		public TicketBuilder setSsoId(String ssoId) {
			this.ssoId = ssoId;
			return this;
		}

		public TicketBuilder setUsageCount(int usedCount) {
			this.usageCount = usedCount;
			return this;
		}

		public TicketBuilder setCreationTime(long creationTime) {
			this.creationTime = creationTime;
			return this;
		}
		
	}
	
	

	public MemSSOManager(ScheduledExecutorService executor_) {
		executor = executor_;
		sf = executor.scheduleWithFixedDelay(this, CHECK_SESSION_TIMEOUT_INTERVAL, CHECK_SESSION_TIMEOUT_INTERVAL,
				TimeUnit.MILLISECONDS);
	}

	@Override
	public void run() {
		logger.info(String.format("There are  %d sessions. Timeout is %dms", getSessions().size(), ssoTimeoutMS));
		try {
			Set<String> expiredSessions = Sets.newHashSet();
			for (Entry<String,DefaultSSOSession> item : getSessions().entrySet()) {
				SSOInfo ss = (SSOInfo) item.getValue();
				long delta = System.currentTimeMillis() - ss.getLastAccessTime();
				if (logger.isDebugEnabled()) {
					logger.debug(String.format("Session %s (%s) last activity %dms ago. Session timeout is %d",
							ss.getSsoId(), ss.getId(),delta, ssoTimeoutMS)); 
				}
				if (delta > ssoTimeoutMS) {
					logger.info(String.format("Session %s (%s) has timed out after %dms. Session timeout is %d",
							ss.getSsoId(), ss.getId(), delta, ssoTimeoutMS));
					expiredSessions.add(item.getKey());
				}
			}
			for (String s : expiredSessions) {
				logger.info("Invalidating session {}", s);
				getSessions().remove(s);
			}

			Set<String> expiredTickets = Sets.newHashSet();
			for (Entry<String, Ticket> item : getTickets().entrySet()) {
				Ticket t = item.getValue();
				long delta = System.currentTimeMillis() - t.creationTime;
				if (delta > ticketTimeoutMS) {
					logger.info(String.format("Pass ticket %s expired after %dms for user %s", item.getKey(),
							ticketTimeoutMS, t.getId()));
					expiredTickets.add(item.getKey());
				}
			}
			for (String k : expiredTickets) {
				logger.info("Invalidating ticket {}", k);
				getTickets().remove(k);
			}
		} catch (Exception e) {
			logger.error("Problem in the code that chekcs for session timeouts! Please FIX! : {}", e);
		}

	}

	@Override
	public SSOInfo createSSOSession(String id) throws IllegalArgumentException {
		return this.createSSOSession(id, null, null, null, null);
	}

	@Override
	public SSOInfo createSSOSession(String id, String firstName, String lastName, String displayName, String email)
			throws IllegalArgumentException {
		if (id == null)
			throw new IllegalArgumentException("Got null id");
		DefaultSSOSession session = new DefaultSSOSession.DefaultSSOSessionBuilder().setId(id).setPrefix(ssoIdPrefix)
				.setFirstName(firstName).setLastName(lastName).setDisplayName(displayName).build();
		getSessions().put(session.getSsoId(), session);
		logger.info("Created sso session {} for user {}.", session.getSsoId(), id);
		return session;
	}

	protected Map<String,DefaultSSOSession> getSessions() {
		return sessions;
	}

	protected Map<String, Ticket> getTickets() {
		return tickets;
	}

	@Override
	public SSOInfo getSSOSession(String ssoId) {
		if (ssoId == null)
			return null;
		SSOInfo ss = (SSOInfo) getSessions().get(ssoId);
		if (ss != null) {
			DefaultSSOSession session = new DefaultSSOSession.DefaultSSOSessionBuilder().keepAlive((DefaultSSOSession) ss);
			getSessions().put(ssoId, session);
		}
		return ss;
	}

	@Override
	public SSOInfo destroySSOSession(String ssoId) {
		if (ssoId == null)
			return null;
		logger.info(String.format("Destroying sso session %s. ...", ssoId));
		SSOInfo ss = (SSOInfo) getSessions().remove(ssoId);
		return ss;
	}

	@Override
	public List<DefaultSSOSession> listSessions(String filter) {
		List<DefaultSSOSession> sl = Lists.newArrayList();
		getSessions().values().forEach(new Consumer<DefaultSSOSession>() {
			@Override
			public void accept(DefaultSSOSession t) {
				if (t.getId().matches(filter) || 
						t.getDisplayName().matches(filter) || 
						t.getEmail().matches(filter)) {
					sl.add(t);
				}
			}
		});
		return sl;
	}

	public long getSsoTimeoutMS() {
		return ssoTimeoutMS;
	}

	public void setSsoTimeoutMS(long ssoTimeoutMS) {
		this.ssoTimeoutMS = ssoTimeoutMS;
	}

	@Override
	public void shutdown() {
		logger.info("Shutting down");
		executor = null;
		sf.cancel(true);
		getSessions().clear();
	}

	@Override
	public String createTicket(String ssoId) {
		SSOInfo ss = getSSOSession(ssoId);
		if (ss == null) {
			logger.info(String.format("Cannot create pass ticket. Got invalid ssoId=%s", ssoId));
			return null;
		}
		String ticket = CommonChecksumFunction.SHA256.generateChecksum(UUID.randomUUID().toString());
		Ticket t = new TicketBuilder().setSsoId(ssoId).setDisplayName(ss.getDisplayName()).setFirstName(ss.getFirstName()).setLastName(ss.getLastName()).setId(ss.getId()).build();
		getTickets().put(ticket, t);
		logger.info("Created pass ticket {} for user {}", ticket, ss.getId());
		return ticket;
	}

	@Override
	public TicketInfo validateTicket(String ticket) {
		if (logger.isDebugEnabled()) {
			logger.debug(String.format("Validating pass ticket %s ...", ticket));
		}
		if (StringUtil.isEmpty(ticket)) {
			return null;
		}
		Ticket t = getTickets().get(ticket);
		if (t == null) {
			return null;
		}
		t = new TicketBuilder().useTicket(t);
		int usedCount = t.getUsageCount();
		getTickets().put(ticket, t);
		if (usedCount > ticketMaxUses) {
			logger.info(String.format("Pass ticket %s reached max uses %d for user %s", ticket, ticketMaxUses,
					t.getId()));
			getTickets().remove(ticket);
			return null;
		}
		long delta = System.currentTimeMillis() - t.creationTime;
		if (delta > ticketTimeoutMS) {
			logger.info(String.format("Pass ticket %s expired after %dms for user %s", ticket, ticketTimeoutMS,
					t.getId()));
			getTickets().remove(ticket);
			return null;
		}

		DefaultTicketInfo li = new DefaultTicketInfo.DefaultTicketInfoBuilder().setDisplayName(t.getDisplayName())
				.setLastName(t.getLastName()).setFirstName(t.getFirstName()).setId(t.getId()).build();
		return li;
	}

	public void setTicketTimeoutMS(long ticketTimeoutMS) {
		this.ticketTimeoutMS = ticketTimeoutMS;
	}

	public void setTicketMaxUses(int ticketMaxUses) {
		this.ticketMaxUses = ticketMaxUses;
	}

	public long getTicketTimeoutMS() {
		return ticketTimeoutMS;
	}

	public int getTicketMaxUses() {
		return ticketMaxUses;
	}

	public void setSsoIdPrefix(String ssoIdPrefix) {
		this.ssoIdPrefix = ssoIdPrefix;
	}
}
