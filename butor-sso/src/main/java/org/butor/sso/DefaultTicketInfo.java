/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.sso;

/*
 * @author asawan
 */
public class DefaultTicketInfo implements TicketInfo {

	private String firstName;
	private String lastName;
	private String id;
	private String displayName;
	private String email;

	/**
	 * Used for externalization only
	 */
	public DefaultTicketInfo() {};
	
	protected DefaultTicketInfo(String firstName, String lastName, String id, String displayName, String email) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.id = id;
		this.displayName = displayName;
		this.email = email;
	}

	

	
	@Override
	public String getId() {
		return id;
	}
	@Override
	public String getDisplayName() {
		return displayName;
	}

	@Override
	public String getFirstName() {
		return firstName;
	}

	@Override
	public String getLastName() {
		return lastName;
	}

	@Override
	public String getEmail() {
		return email;
	}
	

	public static class DefaultTicketInfoBuilder extends AbstractDefaultTicketInfoBuilder<DefaultTicketInfo,DefaultTicketInfoBuilder> {
		@Override
		public DefaultTicketInfo build() {
			return new DefaultTicketInfo(firstName,lastName,id,displayName,email) ;
		}

		@Override
		protected DefaultTicketInfoBuilder thisBuilder() {
			return this;
		}
		
	}
	
	protected static abstract class AbstractDefaultTicketInfoBuilder<T extends DefaultTicketInfo, B extends AbstractDefaultTicketInfoBuilder<T,B>> {
		public abstract T build();
		protected abstract B thisBuilder();
		
		protected String firstName;
		protected String lastName;
		protected String id;
		protected String displayName;
		protected String email;
		
		public B setFirstName(String firstName) {
			this.firstName = firstName;
			return thisBuilder();
		}
		public B setLastName(String lastName) {
			this.lastName = lastName;
			return thisBuilder();
		}
		public B setId(String id) {
			this.id = id;
			return thisBuilder();
		}
		public B setDisplayName(String displayName) {
			this.displayName = displayName;
			return thisBuilder();
		}
		public B setEmail(String email) {
			this.email = email;
			return thisBuilder();
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((displayName == null) ? 0 : displayName.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DefaultTicketInfo other = (DefaultTicketInfo) obj;
		if (displayName == null) {
			if (other.displayName != null)
				return false;
		} else if (!displayName.equals(other.displayName))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		return true;
	}
}
