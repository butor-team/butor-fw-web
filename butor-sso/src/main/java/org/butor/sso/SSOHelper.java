/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.sso;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 *
 * @author asawan
 * @date 30-Jan-09
 */
public class SSOHelper {
	private static final Logger logger = LoggerFactory.getLogger(SSOHelper.class);
	public static String getCookie(HttpServletRequest req_, String cookieName_) {
		Cookie cookie = null;
		Cookie[] cookies = req_.getCookies();
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				cookie = cookies[i];
				if (cookie != null) {
					if (cookie.getName().equalsIgnoreCase(cookieName_)) {
						return cookie.getValue();
					}
				}
			}
		}
		return null;
	}
	public static void setCookie(HttpServletResponse resp_, String name_, String value_, int age_, String path_) {
		// set cookie
		logger.info(String.format("Set cookie %s=%s with path=%s", name_, value_, path_));
		Cookie ssoCookie = new Cookie(name_, value_);
		ssoCookie.setMaxAge(age_);
		ssoCookie.setPath(path_);
		ssoCookie.setHttpOnly(true);
		resp_.addCookie(ssoCookie);
	}
	public static void removeCookie(HttpServletResponse resp_, String name_, String path_) {
		// set cookie
		logger.info("Destroyed cookie={} with path={}", name_, path_);
		Cookie ssoCookie = new Cookie(name_, "");
		ssoCookie.setMaxAge(0);
		ssoCookie.setPath(path_);
		resp_.addCookie(ssoCookie);
	}
}
