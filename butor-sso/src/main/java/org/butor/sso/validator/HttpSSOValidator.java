/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.sso.validator;

import java.io.IOException;

import com.google.common.reflect.TypeToken;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.methods.PostMethod;
import org.butor.json.JsonHelper;
import org.butor.sso.DefaultSSOSession;
import org.butor.sso.SSOConstants;
import org.butor.sso.SSOException;
import org.butor.sso.SSOInfo;
import org.butor.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 *TODO should be separated, so that the wars that import this class are smallers and require less dependencies.
 *
 * @author asawan
 * @date 24-Jan-09
 */
public class HttpSSOValidator  extends AbstractSSOValidator {
	private JsonHelper jsh = new JsonHelper();
	protected String ssoCheckUrl = null;

	/**
	 * validate sso session
	 * 
	 * return null is sso is invalid
	 */
	@Override
	public SSOInfo validate(String ssoId) throws SSOException {
		logger.info("Validating ssoId={} wirh URL={} ...", ssoId, ssoCheckUrl);
		if (StringUtil.isEmpty(ssoCheckUrl)) {
			throw new SSOException("Got null/empty ssoUrl");
		}

		PostMethod method = null;
		try {
			method = buildPost(new NameValuePair[] { new NameValuePair(SSOConstants.SSO_SSO_ID, ssoId) });
			method.addRequestHeader("Cookie", SSOConstants.SSO_SSO_ID +"=" +ssoId);
			sendRequest(method);
			return parseReply(method);
		} catch (IOException e) {
			throw new SSOException(e);
		} finally {
			if (method != null) {
				method.releaseConnection();
			}
		}
	}

	public PostMethod buildPost(NameValuePair[] args_) {
		PostMethod method = new PostMethod(ssoCheckUrl);
		method.getParams().setCookiePolicy(CookiePolicy.DEFAULT);
		method.setRequestBody(args_);
		return method;
	}

	/**
	 * Sends an import request to the wiki.
	 * 
	 * @param m
	 *            The post method to send.
	 * @throws Exception
	 *             In case the transmission fails.
	 */
	private void sendRequest(PostMethod m) throws IOException {
		HttpClient client = new HttpClient();
		client.getHttpConnectionManager().getParams().setConnectionTimeout(5000);

		int status = client.executeMethod(m);

		if (status != HttpStatus.SC_OK) {
			throw new IOException("Post failed with HTTP status : (" + status + ") " + HttpStatus.getStatusText(status));
		}
	}

	private SSOInfo parseReply(PostMethod m) throws IOException {
		String pl = m.getResponseBodyAsString();
		SSOInfo sos = jsh.deserialize(pl, TypeToken.of(DefaultSSOSession.class).getType()); 
		return sos;
	}
	
	public String getSsoCheckUrl() {
		return ssoCheckUrl;
	}
	

	public void setSsoCheckUrl(String ssoCheckUrl) {
		this.ssoCheckUrl = ssoCheckUrl;
	}


}
