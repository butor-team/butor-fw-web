/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.sso.validator;

import org.butor.sso.SSOException;
import org.butor.sso.SSOInfo;
import org.butor.sso.SSOManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 *TODO
 *
 * @author asawan
 * @date 24-Jan-09
 */
public class VMSSOValidator implements ISSOValidator {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private SSOManager ssoManager = null;
	
	private String sessionTimeoutUrl = null;
	private String sessionTimeoutStreamingUrl = null;

	public SSOManager getSsoManager() {
		return ssoManager;
	}

	public void setSsoManager(SSOManager ssoManager) {
		this.ssoManager = ssoManager;
	}

	/**
	 * validate sso session
	 * 
	 * return null is sso is invalid
	 */
	@Override
	public SSOInfo validate(String ssoId) throws SSOException {
		logger.info("Validating ssoId={} ...", ssoId);
		return ssoManager.getSSOSession(ssoId);
	}

	@Override
	public String getSessionTimeoutUrl() {
		return sessionTimeoutUrl;
	}

	@Override
	public String getSessionTimeoutStreamingUrl() {
		return sessionTimeoutStreamingUrl;
	}

	public void setSessionTimeoutUrl(String sessionTimeoutUrl) {
		this.sessionTimeoutUrl = sessionTimeoutUrl;
	}

	public void setSessionTimeoutStreamingUrl(String sessionTimeoutStreamingUrl) {
		this.sessionTimeoutStreamingUrl = sessionTimeoutStreamingUrl;
	}
}
