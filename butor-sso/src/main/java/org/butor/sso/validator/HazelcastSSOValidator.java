/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.sso.validator;

import java.util.Map;

import org.butor.sso.HazelcastSSOManager;
import org.butor.sso.SSOException;
import org.butor.sso.SSOInfo;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.hazelcast.core.HazelcastInstance;
/**
 * 
 * @author tbussier
 *
 */
public class HazelcastSSOValidator extends AbstractSSOValidator implements InitializingBean, DisposableBean{

	private Map<String, SSOInfo> distributedSessions;
	private String sessionsMapName = null;

	HazelcastInstance hz;
	@Override
	public SSOInfo validate(String ssoId) throws SSOException {
		return distributedSessions.get(ssoId);
	}
	
	public void setHazelcastInstance(HazelcastInstance hzInstance) {
		this.hz = hzInstance;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if (Strings.isNullOrEmpty(sessionsMapName)) {
			logger.info("sessionsMapName was not set! Considering default name {}", HazelcastSSOManager.SESSIONS_MAP_KEY);
			sessionsMapName = HazelcastSSOManager.SESSIONS_MAP_KEY;
		}
		Preconditions.checkNotNull(hz, "HazelcastInstance is mandatory!");
		distributedSessions = hz.getReplicatedMap(sessionsMapName);
	}

	@Override
	public void destroy() throws Exception {
		hz.shutdown();
	}
	
	public void setSessionsMapName(String sessionsMapName) {
		this.sessionsMapName = sessionsMapName;
	}
}
