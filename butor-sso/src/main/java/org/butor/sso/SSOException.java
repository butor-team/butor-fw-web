/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.sso;

/*
 *
 * @author asawan
 * @date 24-Jan-09
 */
public class SSOException extends Exception {
	private static final long serialVersionUID = -8702374911197008629L;

	public SSOException() {
		super();
	}

	public SSOException(String message_) {
		super(message_);
	}

	public SSOException(Throwable cause_) {
		super(cause_);
	}

	public SSOException(String message_, Throwable cause_) {
		super(message_, cause_);
	}

}
